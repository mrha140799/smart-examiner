export default class UserActionCommons {
  public static readonly SAVE_TOKEN: string = "SAVE_TOKEN";
  public static readonly LOG_OUT: string = "LOG_OUT";
  public static readonly SAVE_USER_LOGIN: string = "SAVE_USER_LOGIN";
  public static readonly SAVE_USER_PROFILE: string = "SAVE_USER_PROFILE";
  public static readonly GET_TOKEN: string = "GET_TOKEN";
}
