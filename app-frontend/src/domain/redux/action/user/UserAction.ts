import UserActionCommons from "./UserActionCommons";
import {Action, Dispatch} from "redux";

export const saveToken = (token: string) => (dispatch: Dispatch<Action>) => {
  dispatch({type: UserActionCommons.SAVE_TOKEN, payload: token});
};
export const logOut = () => (dispatch: Dispatch<Action>) => {
  dispatch({type: UserActionCommons.LOG_OUT});
};
export const saveUserLogin = (data: any) => (dispatch: Dispatch<Action>) => {
  dispatch({type: UserActionCommons.SAVE_USER_LOGIN, payload: data});
};
export const saveUserProfile = (data: any) => (dispatch: Dispatch<Action>) => {
  dispatch({type: UserActionCommons.SAVE_USER_PROFILE, payload: data});
};
