import {combineReducers} from "redux";
import UserReducers from "./user/UserReducer";

const allReducers = combineReducers({
  UserReducers,
});
export default allReducers;
