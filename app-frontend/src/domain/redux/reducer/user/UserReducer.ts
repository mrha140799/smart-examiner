import UserActionCommons from "../../action/user/UserActionCommons";
import {AnyAction} from "redux";

const initialState = {
  USER_TOKEN: null,
  USER_INFO: null,
};
const UserReducers = (state: any = initialState, action: AnyAction) => {
  switch (action.type) {
    case UserActionCommons.SAVE_TOKEN:
      return {
        ...state,
        USER_TOKEN: action.payload,
      };
    case UserActionCommons.LOG_OUT:
      return {
        ...state,
        USER_TOKEN: null,
        USER_INFO: null,
      };
    case UserActionCommons.SAVE_USER_LOGIN:
      return {
        ...state,
        USER_INFO: JSON.stringify(action.payload.userInfo),
        USER_TOKEN: action.payload.token,
      };
    case UserActionCommons.SAVE_USER_PROFILE:
      return {
        ...state,
        USER_INFO: JSON.stringify(action.payload),
      };
    default:
      return state;
  }
};
export default UserReducers;
