import {applyMiddleware, createStore} from "redux";
import allReducers from "../redux/reducer";
import thunk from "redux-thunk";

const Store = createStore(allReducers, applyMiddleware(thunk));

export default Store;
