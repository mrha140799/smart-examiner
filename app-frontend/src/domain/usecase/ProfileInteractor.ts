import User from "../../data/model/User";
import IUserRepository from "../../data/repository/IUserRepository";
import UserApi from "../../data/api/UserApi";

export default class ProfileInteractor {
  private output: IProfileOutputPort;
  private userApi: IUserRepository;

  private constructor(output: IProfileOutputPort) {
    this.output = output;
    this.userApi = new UserApi();
  }

  public static created(output: IProfileOutputPort): ProfileInteractor {
    return new ProfileInteractor(output);
  }

  public changeAvatar(fileContent: string) {
    this.userApi
      .changeAvatar(fileContent)
      .then((resp: any) => {
        this.output.changeAvatarSuccessfully(resp.data.data);
      })
      .catch(() => {
        this.output.changeAvatarError();
      });
  }
}

export abstract class IProfileOutputPort {
  abstract changeAvatarSuccessfully(avatarName: string): void;

  abstract changeAvatarError(): void;
}
