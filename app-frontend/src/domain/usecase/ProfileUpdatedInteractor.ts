import IUserRepository from "../../data/repository/IUserRepository";
import UserApi from "../../data/api/UserApi";
import User from "../../data/model/User";

export default class ProfileUpdatedInteractor {
  private output: IProfileUpdatedOutputPort;
  private userApi: IUserRepository;

  private constructor(output: IProfileUpdatedOutputPort) {
    this.output = output;
    this.userApi = new UserApi();
  }

  public static created(
    output: IProfileUpdatedOutputPort,
  ): ProfileUpdatedInteractor {
    return new ProfileUpdatedInteractor(output);
  }

  public getMyProfile() {
    this.userApi
      .me()
      .then((resp: any) => {
        this.output.getMyProfileSuccessfully(resp.data.data);
      })
      .catch(() => {
        this.output.getMyProfileError();
      });
  }

  public updateProfile(user: User) {
    this.userApi
      .updateProfile(user)
      .then((resp: any) => {
        this.output.updateProfileSuccessfully(resp.data.data);
      })
      .catch(() => {
        this.output.updateProfileError();
      });
  }
}

export abstract class IProfileUpdatedOutputPort {
  abstract getMyProfileSuccessfully(user: any): void;

  abstract getMyProfileError(): void;

  abstract updateProfileSuccessfully(user: User): void;

  abstract updateProfileError(): void;
}
