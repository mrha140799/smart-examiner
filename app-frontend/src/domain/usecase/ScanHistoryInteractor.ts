import IScanRepository from "../../data/repository/IScanRepository";
import ScanApi from "../../data/api/ScanApi";
import ScanResult from "../../data/model/ScanResult";

export default class ScanHistoryInteractor {
  public outputPort: IScanHistoryOutputPort;
  private scanRep: IScanRepository;

  private constructor(outputPort: IScanHistoryOutputPort) {
    this.outputPort = outputPort;
    this.scanRep = new ScanApi();
  }

  public myScanHistories() {
    this.scanRep
      .myScanHistories()
      .then((resp) => {
        const body = resp.data;
        this.outputPort.myScanHistoriesSuccessfully(body.data);
      })
      .catch(() => {
        this.outputPort.myScanHistoriesError();
      });
  }

  public static create(
    outputPort: IScanHistoryOutputPort,
  ): ScanHistoryInteractor {
    return new ScanHistoryInteractor(outputPort);
  }
}
export abstract class IScanHistoryOutputPort {
  abstract myScanHistoriesSuccessfully(scans: Array<ScanResult>): any;
  abstract myScanHistoriesError(): any;
}
