import ITestRepository from "../../data/repository/ITestRepository";
import TestApi from "../../data/api/TestApi";
import Test from "../../data/model/Test";
import IStudentCodeRepository from "../../data/repository/IStudentCodeRepository";
import StudentCodeApi from "../../data/api/StudentCodeApi";
import StudentCodeRequest from "../../data/model/StudentCodeRequest";
import IClassroomRepository from "../../data/repository/IClassroomRepository";
import ClassroomApi from "../../data/api/ClassroomApi";
import Classroom from "../../data/model/Classroom";
import IStudentRepository from "../../data/repository/IStudentRepository";
import StudentApi from "../../data/api/StudentApi";
import Student from "../../data/model/Student";

export default class StudentCodeInteractor {
  public outputPort: IStudentCodeOutputPort;
  private testRep: ITestRepository;
  private studentCodeRep: IStudentCodeRepository;
  private studentRep: IStudentRepository;
  private classroomRep: IClassroomRepository;

  private constructor(outputPort: IStudentCodeOutputPort) {
    this.outputPort = outputPort;
    this.testRep = new TestApi();
    this.studentCodeRep = new StudentCodeApi();
    this.studentRep = new StudentApi();
    this.classroomRep = new ClassroomApi();
  }

  public getDataInit(studentCodeId?: number) {
    const promises: Array<Promise<any>> = [];
    promises.push(this.classroomRep.getAllMyClassroom());
    promises.push(this.testRep.myTests());
    if (studentCodeId) {
      promises.push(this.studentCodeRep.getOneById(studentCodeId));
    }
    Promise.all(promises)
      .then((responses: any[]) => {
        const classrooms = responses[0].data.data;
        if (classrooms.length) {
          this.studentRep
            .getAllByClassId(classrooms[0].id)
            .then((resp: any) => {
              this.outputPort.getDataInitSuccessfully(
                classrooms,
                responses[1].data.data,
                resp.data.data,
                responses[2] ? responses[2].data.data : null,
              );
            });
        } else {
          this.outputPort.getDataInitSuccessfully(
            classrooms,
            responses[1].data.data,
            [],
            responses[2] ? responses[2].data.data : null,
          );
        }
      })
      .catch(() => {
        this.outputPort.getDataInitError();
      });
  }

  public getStudentByClassId(classId: number) {
    this.studentRep
      .getAllByClassId(classId)
      .then((resp: any) => {
        this.outputPort.getStudentByClassIdSuccessfully(resp.data.data);
      })
      .catch(() => {
        this.outputPort.getStudentByClassIdError();
      });
  }

  public createOne(studentCode: StudentCodeRequest) {
    this.studentCodeRep
      .createOne(studentCode)
      .then((resp: any) => {
        if (resp.data.statusCode === -200) {
          this.outputPort.invalidCode();
        } else if (resp.data.statusCode === -201) {
          this.outputPort.invalidStudent();
        } else {
          this.outputPort.createOneSuccessfully();
        }
      })
      .catch(() => {
        this.outputPort.createOneError();
      });
  }

  public updateOne(studentCode: StudentCodeRequest) {
    this.studentCodeRep
      .updateOne(studentCode)
      .then((resp: any) => {
        if (resp.data.statusCode === -200) {
          this.outputPort.invalidCode();
        } else {
          this.outputPort.updateOneSuccessfully();
        }
      })
      .catch(() => {
        this.outputPort.updateOneError();
      });
  }

  public static create(
    outputPort: IStudentCodeOutputPort,
  ): StudentCodeInteractor {
    return new StudentCodeInteractor(outputPort);
  }
}
export abstract class IStudentCodeOutputPort {
  abstract getDataInitSuccessfully(
    classrooms: Array<Classroom>,
    tests: Array<Test>,
    students: Array<Student>,
    studentCode: StudentCodeRequest | null,
  ): any;
  abstract getDataInitError(): any;
  abstract callApiFinally(): any;
  abstract createOneSuccessfully(): any;
  abstract updateOneSuccessfully(): any;
  abstract updateOneError(): any;
  abstract createOneError(): any;
  abstract invalidCode(): any;
  abstract invalidStudent(): any;
  abstract getStudentByClassIdSuccessfully(students: Array<Student>): any;
  abstract getStudentByClassIdError(): any;
}
