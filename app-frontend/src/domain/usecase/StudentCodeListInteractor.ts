import ITestRepository from "../../data/repository/ITestRepository";
import TestApi from "../../data/api/TestApi";
import Test from "../../data/model/Test";
import IStudentCodeRepository from "../../data/repository/IStudentCodeRepository";
import StudentCodeApi from "../../data/api/StudentCodeApi";
import StudentCode from "../../data/model/StudentCode";

export default class StudentCodeListInteractor {
  public outputPort: IStudentCodeListOutputPort;
  private testRep: ITestRepository;
  private studentCodeRep: IStudentCodeRepository;

  private constructor(outputPort: IStudentCodeListOutputPort) {
    this.outputPort = outputPort;
    this.testRep = new TestApi();
    this.studentCodeRep = new StudentCodeApi();
  }

  public deleteById(id: number | null) {
    this.studentCodeRep
      .deleteById(id)
      .then(() => {
        this.outputPort.deleteByIdSuccessfully();
      })
      .catch(() => {
        this.outputPort.deleteByIdError();
      });
  }

  public getMyTests() {
    this.testRep
      .myTests()
      .then((resp) => {
        const body = resp.data;
        this.outputPort.getMyTestsSuccessfully(body.data);
      })
      .catch(() => {
        this.outputPort.getMyTestsError();
      });
  }

  public getAllByTestId(id: number) {
    this.studentCodeRep
      .getAllByTestId(id)
      .then((resp: any) => {
        this.outputPort.getAllByTestIdSuccessfully(resp.data.data);
      })
      .catch(() => {
        this.outputPort.getAllByTestIdError();
      });
  }

  public static create(
    outputPort: IStudentCodeListOutputPort,
  ): StudentCodeListInteractor {
    return new StudentCodeListInteractor(outputPort);
  }
}
export abstract class IStudentCodeListOutputPort {
  abstract getMyTestsSuccessfully(tests: Array<Test>): any;
  abstract getMyTestsError(): any;
  abstract deleteByIdSuccessfully(): any;
  abstract deleteByIdError(): any;
  abstract callApiFinally(): any;
  abstract getAllByTestIdSuccessfully(studentCodes: Array<StudentCode>): any;
  abstract getAllByTestIdError(): any;
}
