import IAuthRepository from "../../data/repository/IAuthRepository";
import Account from "../../data/model/Account";
import AuthApi from "../../data/api/AuthApi";
import ResponseMessage from "../../data/model/ResponseMessage";

export default class LoginInteractor {
  private outputPort: LoginOutputPort;
  private authRepo: IAuthRepository;
  private constructor(outputPort: LoginOutputPort) {
    this.authRepo = new AuthApi();
    this.outputPort = outputPort;
  }

  public login(account: Account) {
    this.authRepo
      .login(account)
      .then((response: any) => {
        const body: ResponseMessage<any> = response.data;
        this.outputPort.loginSuccess(body.data);
      })
      .catch((error: any) => {
        this.outputPort.loginError(error);
      })
      .finally(() => {
        this.outputPort.loginFinally();
      });
  }

  public static create(outputPort: LoginOutputPort): LoginInteractor {
    return new LoginInteractor(outputPort);
  }
}
export abstract class LoginOutputPort {
  abstract loginSuccess(data: any): void;
  abstract loginError(error: any): void;
  abstract loginFinally(): void;
}
