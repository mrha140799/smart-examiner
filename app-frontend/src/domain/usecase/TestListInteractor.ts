import ITestRepository from "../../data/repository/ITestRepository";
import TestApi from "../../data/api/TestApi";
import Test from "../../data/model/Test";

export default class TestListInteractor {
  public outputPort: ITestListOutputPort;
  private testRep: ITestRepository;

  private constructor(outputPort: ITestListOutputPort) {
    this.outputPort = outputPort;
    this.testRep = new TestApi();
  }

  public deleteById(id: number | null) {
    this.testRep
      .deleteById(id)
      .then(() => {
        this.outputPort.deleteByIdSuccessfully();
      })
      .catch(() => {
        this.outputPort.deleteByIdError();
      })
      .finally(() => {
        this.outputPort.callApiFinally();
      });
  }

  public getMyTests() {
    this.testRep
      .myTests()
      .then((resp) => {
        const body = resp.data;
        this.outputPort.getMyTestsSuccessfully(body.data);
      })
      .catch(() => {
        this.outputPort.getMyTestsError();
      });
  }

  public static create(outputPort: ITestListOutputPort): TestListInteractor {
    return new TestListInteractor(outputPort);
  }
}
export abstract class ITestListOutputPort {
  abstract getMyTestsSuccessfully(tests: Array<Test>): any;
  abstract getMyTestsError(): any;
  abstract deleteByIdSuccessfully(): any;
  abstract deleteByIdError(): any;
  abstract callApiFinally(): any;
}
