import IScanRepository from "../../data/repository/IScanRepository";
import ScanApi from "../../data/api/ScanApi";

export default class ScanInteractor {
  private outputPort: ScanOutputPort;
  private scanRepo: IScanRepository;
  private constructor(outputPort: ScanOutputPort) {
    this.scanRepo = new ScanApi();
    this.outputPort = outputPort;
  }

  public saveFile(fileContent: string) {
    this.scanRepo
      .saveFile(fileContent)
      .then((response: any) => {
        this.outputPort.saveFileSuccess(response.data.data);
      })
      .catch((error: any) => {
        this.outputPort.saveFileError(error.response.data.message);
      });
  }

  public static create(outputPort: ScanOutputPort): ScanInteractor {
    return new ScanInteractor(outputPort);
  }
}
export abstract class ScanOutputPort {
  abstract saveFileSuccess(result: any): void;
  abstract saveFileError(error: any): void;
}
