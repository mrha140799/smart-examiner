import ITestRepository from "../../data/repository/ITestRepository";
import TestApi from "../../data/api/TestApi";
import Test from "../../data/model/Test";

export default class TestInteractor {
  public outputPort: ITestOutputPort;
  private testRep: ITestRepository;

  private constructor(outputPort: ITestOutputPort) {
    this.outputPort = outputPort;
    this.testRep = new TestApi();
  }

  public isValidTestCode(code: string) {
    this.testRep
      .isValidTestCode(code)
      .then(() => {
        this.outputPort.isValidTestCode();
      })
      .catch(() => {
        this.outputPort.invalidTestCode();
      });
  }

  public getById(id: number) {
    this.testRep
      .getById(id)
      .then((resp: any) => {
        this.outputPort.getByIdSuccessfully(resp.data.data);
      })
      .catch(() => {
        this.outputPort.getByIdError();
      });
  }

  public updateTest(test: Test) {
    this.testRep
      .updateTest(test)
      .then(() => {
        this.outputPort.updateTestSuccessfully();
      })
      .catch(() => {
        this.outputPort.updateTestError();
      });
  }

  public createOne(test: Test) {
    this.testRep
      .createOne(test)
      .then(() => {
        this.outputPort.createOneSuccessfully();
      })
      .catch((error: any) => {
        console.log(error);
        this.outputPort.createOneError();
      });
  }

  public static create(outputPort: ITestOutputPort): TestInteractor {
    return new TestInteractor(outputPort);
  }
}
export abstract class ITestOutputPort {
  abstract createOneSuccessfully(): any;
  abstract createOneError(): any;
  abstract isValidTestCode(): any;
  abstract invalidTestCode(): any;
  abstract getByIdSuccessfully(test: Test): any;
  abstract getByIdError(): any;
  abstract updateTestSuccessfully(): any;
  abstract updateTestError(): any;
}
