import IAuthRepository from "../../data/repository/IAuthRepository";
import AuthApi from "../../data/api/AuthApi";
import User from "../../data/model/User";

export default class RegistrationInteractor {
  public outputPort: IRegistrationOutputPort;
  private authRepo: IAuthRepository;

  private constructor(outputPort: IRegistrationOutputPort) {
    this.outputPort = outputPort;
    this.authRepo = new AuthApi();
  }

  public isValidEmail(email: string) {
    this.authRepo
      .isValidEmail(email)
      .then(() => {
        this.outputPort.isValidEmail();
      })
      .catch(() => {
        this.outputPort.invalidEmail();
      });
  }

  public register(user: User) {
    this.authRepo
      .register(user)
      .then(() => {
        this.outputPort.registerSuccessfully();
      })
      .catch(() => {
        this.outputPort.registerError();
      });
  }

  public static create(
    outputPort: IRegistrationOutputPort,
  ): RegistrationInteractor {
    return new RegistrationInteractor(outputPort);
  }
}
export abstract class IRegistrationOutputPort {
  abstract isValidEmail(): any;
  abstract invalidEmail(): any;
  abstract registerSuccessfully(): any;
  abstract registerError(): any;
}
