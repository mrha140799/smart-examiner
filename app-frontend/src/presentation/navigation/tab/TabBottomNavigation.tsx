import React from "react";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import HomeScreen from "../../container/home-screen/HomeScreen";
import AboutScreen from "../../container/about-screen/AboutScreen";
import {Icon} from "native-base";
import {TimingKeyboardAnimationConfig} from "@react-navigation/bottom-tabs/src/types";
import {TransitionSpec} from "@react-navigation/stack/lib/typescript/src/types";
import {Easing} from "react-native";

const Tab = createBottomTabNavigator();
export default class TabBottomNavigation extends React.Component<any, any> {
  public config: TransitionSpec = {
    animation: "timing",
    config: {
      duration: 300,
      easing: Easing.linear,
    },
  };
  render() {
    return (
      <Tab.Navigator
        tabBarOptions={{
          inactiveTintColor: "#30475e",
          activeTintColor: "#ed6663",
        }}>
        <Tab.Screen
          name="Home"
          component={HomeScreen}
          options={{
            title: "",
            tabBarIcon: (config) => (
              <Icon
                name="home"
                style={{fontSize: config.size, color: config.color}}
              />
            ),
          }}
        />
        <Tab.Screen
          name="About"
          component={AboutScreen}
          options={{
            title: "",
            tabBarIcon: (config) => (
              <Icon
                name="information"
                style={{fontSize: config.size, color: config.color}}
              />
            ),
          }}
        />
      </Tab.Navigator>
    );
  }
}
