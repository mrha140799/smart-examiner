import {StyleSheet} from "react-native";

export default class DrawerNavigationStyles {
  public static styles = StyleSheet.create({
    icon: {
      width: 25,
      height: 25,
    },
  });
}
