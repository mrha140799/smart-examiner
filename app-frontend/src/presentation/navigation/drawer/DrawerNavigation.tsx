import React from "react";
import {createDrawerNavigator} from "@react-navigation/drawer";
import DrawerNavigationStyles from "./DrawerNavigationStyles";
import Sidebar from "../sidebar/Sidebar";
import {Icon} from "native-base";
import ProfileScreen from "../../container/profile-screen/ProfileScreen";
import TabBottomNavigation from "../tab/TabBottomNavigation";
import {connect} from "react-redux";

const Drawer = createDrawerNavigator<any>();
export class DrawerNavigation extends React.Component<any, any> {
  private styles = DrawerNavigationStyles.styles;
  render() {
    return (
      <Drawer.Navigator
        drawerContent={(props) => <Sidebar {...props} />}
        drawerContentOptions={{
          inactiveTintColor: "#30475e",
          activeTintColor: "#f05454",
        }}>
        <Drawer.Screen
          name="Trang Chủ"
          component={TabBottomNavigation}
          options={{
            drawerIcon: (config) => (
              <Icon
                name="home"
                style={{fontSize: config.size, color: config.color}}
              />
            ),
          }}
        />
        <Drawer.Screen
          name="Thông tin cá nhân"
          component={ProfileScreen}
          options={{
            drawerIcon: (config) => (
              <Icon
                name="people"
                style={{fontSize: config.size, color: config.color}}
              />
            ),
          }}
        />
      </Drawer.Navigator>
    );
  }
}

export default DrawerNavigation;
