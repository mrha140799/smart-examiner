import React from "react";
import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";
import LoginScreen from "../container/login-screen/LoginScreen";
import DrawerNavigation from "./drawer/DrawerNavigation";
import {TransitionSpec} from "@react-navigation/stack/lib/typescript/src/types";
import {Easing} from "react-native";
import ScanScreen from "../container/scan-screen/ScanScreen";
import ScanResultScreen from "../container/scan-result-screen/ScanResultScreen";
import ScanInfoScreen from "../container/scan-info-screen/ScanInfoScreen";
import RegisterScreen from "../container/register-screen/RegisterScreen";
import ListTestScreen from "../container/list-test-screen/ListTestScreen";
import TestScreen from "../container/test-screen/TestScreen";
import ListStudentCodeScreen from "../container/list-student-code-screen/ListStudentCodeScreen";
import StudentCodeScreen from "../container/student-code-screen/StudentCodeScreen";
import ScanHistoryScreen from "../container/scan-history-screen/ScanHistoryScreen";
import ProfileUpdatedScreen from "../container/profile-updated-screen/ProfileUpdatedScreen";

const Stack = createStackNavigator();
export default class AppNavigation extends React.Component {
  public config: TransitionSpec = {
    animation: "timing",
    config: {
      duration: 300,
      easing: Easing.linear,
    },
  };
  render() {
    return (
      <>
        <NavigationContainer>
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
              gestureEnabled: false,
              transitionSpec: {
                close: this.config,
                open: this.config,
              },
            }}>
            <Stack.Screen name="Login" component={LoginScreen} />
            <Stack.Screen
              name="ListStudentCode"
              component={ListStudentCodeScreen}
            />
            <Stack.Screen
              name="ProfileUpdated"
              component={ProfileUpdatedScreen}
            />
            <Stack.Screen name="Index" component={DrawerNavigation} />
            <Stack.Screen name="Test" component={TestScreen} />
            <Stack.Screen name="StudentCode" component={StudentCodeScreen} />
            <Stack.Screen name="ScanHistory" component={ScanHistoryScreen} />
            <Stack.Screen name="ListTest" component={ListTestScreen} />
            <Stack.Screen name="Registration" component={RegisterScreen} />
            <Stack.Screen name="Scan" component={ScanScreen} />
            <Stack.Screen name="ScanResult" component={ScanResultScreen} />
            <Stack.Screen name="TestInfoScreen" component={ScanInfoScreen} />
          </Stack.Navigator>
        </NavigationContainer>
      </>
    );
  }
}
