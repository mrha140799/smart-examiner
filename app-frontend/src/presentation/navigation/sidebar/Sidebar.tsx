import React from "react";
import {
  DrawerContentScrollView,
  DrawerItem,
  DrawerItemList,
} from "@react-navigation/drawer";
import {
  Body,
  Container,
  Content,
  Footer,
  H3,
  Icon,
  Left,
  ListItem,
  Text,
  Thumbnail,
  View,
} from "native-base";
import {connect} from "react-redux";
import {logOut} from "../../../domain/redux/action/user/UserAction";
import MasterData from "../../../assets/json/Commons.json";
import {Image} from "react-native";

class Sidebar extends React.Component<any, any> {
  private readonly IMAGE_URI: string = MasterData["image-uri"];
  constructor(props: any) {
    super(props);
    this.state = {
      isLoadImageFull: false,
    };
  }
  public getCurrentUserRoleName(): string {
    if (this.props.userInfo && this.props.userInfo.roles.length) {
      if (this.props.userInfo.roles.includes("Admin system")) {
        return "Quản trị hệ thống";
      } else if (this.props.userInfo.roles.includes("Admin")) {
        return "Quản trị viên";
      } else {
        return "Người dùng";
      }
    }
    return "Người dùng";
  }
  render() {
    return (
      <>
        <Container>
          <Content>
            <ListItem thumbnail>
              <Left>
                <Thumbnail
                  source={
                    this.props.userInfo && this.props.userInfo.imageAvatarName
                      ? {
                          uri:
                            this.IMAGE_URI +
                            this.props.userInfo.imageAvatarName,
                        }
                      : require("../../../assets/image/default_user.png")
                  }
                  onLoadStart={() => {
                    this.setState((preState: any) => ({
                      ...preState,
                      isLoadImageFull: true,
                    }));
                  }}
                  onLoadEnd={() => {
                    this.setState((preState: any) => ({
                      ...preState,
                      isLoadImageFull: false,
                    }));
                  }}
                />
              </Left>
              <Body>
                <H3>
                  {this.props.userInfo && this.props.userInfo.firstName
                    ? this.props.userInfo.firstName
                    : "Ẩn danh"}
                </H3>
                <Text note>{this.getCurrentUserRoleName()}</Text>
              </Body>
            </ListItem>
            <DrawerContentScrollView {...this.props}>
              <DrawerItemList
                state={this.props.state}
                navigation={this.props.navigation}
                descriptors={this.props.descriptors}
              />
            </DrawerContentScrollView>
          </Content>
          <Footer>
            <View style={{flex: 1}}>
              <DrawerItem
                label="Đăng xuất"
                icon={(config) => (
                  <Icon
                    name="exit"
                    style={{fontSize: config.size, color: config.color}}
                  />
                )}
                onPress={() => {
                  this.props.logOut();
                  this.props.navigation.navigate("Login");
                }}
              />
            </View>
          </Footer>
        </Container>
      </>
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    userInfo: JSON.parse(state.UserReducers.USER_INFO),
  };
};
export default connect(mapStateToProps, {logOut})(Sidebar);
