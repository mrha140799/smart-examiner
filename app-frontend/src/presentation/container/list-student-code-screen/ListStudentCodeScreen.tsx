import React from "react";
import AwesomeLoading from "react-native-awesome-loading/index";
import {
  Body,
  Button,
  Container,
  Content,
  Header,
  Left,
  ListItem,
  Right,
  Title,
  View,
} from "native-base";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import {Alert, SafeAreaView, Text, TouchableOpacity} from "react-native";
import DropDownPicker from "react-native-dropdown-picker";
import {SwipeListView} from "react-native-swipe-list-view";
import Test from "../../../data/model/Test";
import StudentCodeListInteractor, {
  IStudentCodeListOutputPort,
} from "../../../domain/usecase/StudentCodeListInteractor";
import StudentCode from "../../../data/model/StudentCode";

class ListStudentCodeScreen
  extends React.Component<any, any>
  implements IStudentCodeListOutputPort {
  private readonly DETAIL_TYPE: string = "detail";
  private readonly UPDATE_TYPE: string = "update";
  private readonly DELETE_TYPE: string = "delete";
  private listStudentCodeUC: StudentCodeListInteractor = StudentCodeListInteractor.create(
    this,
  );
  constructor(props: any) {
    super(props);
    this.state = {
      isCallingApi: false,
      isEmpty: true,
      testIdSelected: null,
      tests: [],
      studentCodes: [new StudentCode()],
    };
  }
  componentDidMount() {
    this.setForState("isCallingApi", true);
    this.listStudentCodeUC.getMyTests();
  }

  private setForState(field: string, data: any) {
    this.setState((preState: any) => ({
      ...preState,
      [field]: data,
    }));
  }

  getMyTestsSuccessfully(tests: Test[]) {
    this.setState((preState: any) => ({
      ...preState,
      tests: this.formatInToDropDownItems(tests),
      isCallingApi: false,
    }));
  }
  getMyTestsError() {
    throw new Error("Method not implemented.");
  }
  deleteByIdSuccessfully() {
    Alert.alert("Xóa thông tin thành công!");
    if (this.state.testIdSelected) {
      this.listStudentCodeUC.getAllByTestId(this.state.testIdSelected);
    } else {
      this.listStudentCodeUC.getMyTests();
    }
  }
  deleteByIdError() {
    this.setForState("isCallingApi", false);
    Alert.alert("Xóa thông tin thất bại!");
  }
  callApiFinally() {
    throw new Error("Method not implemented.");
  }

  getAllByTestIdError(): any {
    this.setForState("isEmpty", true);
    this.setForState("isCallingApi", false);
  }

  getAllByTestIdSuccessfully(studentCodes: Array<StudentCode>): any {
    if (studentCodes.length === 0) {
      this.setForState("isEmpty", true);
      this.setForState("isCallingApi", false);
    } else {
      this.setState((preState: any) => ({
        ...preState,
        isCallingApi: false,
        isEmpty: false,
        studentCodes: studentCodes,
      }));
    }
  }
  public onHandlePressed(type: string, id: number | null) {
    if (type === this.DELETE_TYPE) {
      Alert.alert("Xóa thí sinh", " Bạn có chắc muốn xóa thí sinh này?", [
        {
          text: "Hủy",
          onPress: () => {},
          style: "cancel",
        },
        {
          text: "Đồng ý",
          onPress: () => {
            this.setForState("isCallingApi", true);
            this.listStudentCodeUC.deleteById(id);
          },
        },
      ]);
    } else if (type === this.UPDATE_TYPE) {
      this.props.navigation.navigate("StudentCode", {
        actionType: this.UPDATE_TYPE,
        studentCodeId: id,
      });
    } else if (type === this.DETAIL_TYPE) {
      this.props.navigation.navigate("StudentCode", {
        actionType: this.DETAIL_TYPE,
        studentCodeId: id,
      });
    }
  }
  private formatInToDropDownItems(tests: Array<Test>) {
    return tests.map((e) => {
      return {
        label: e.code,
        value: e.id,
      };
    });
  }
  private onSelectedChange(id: number) {
    this.setState((preState: any) => ({
      ...preState,
      testIdSelected: id,
      isCallingApi: true,
    }));
    this.listStudentCodeUC.getAllByTestId(id);
  }
  render() {
    return (
      <>
        <AwesomeLoading
          indicatorId={8}
          size={50}
          isActive={this.state.isCallingApi}
          text="loading"
        />
        <Container>
          <Header style={{borderBottomColor: "red", borderBottomWidth: 1}}>
            <Left>
              <Button
                transparent
                onPress={() => {
                  this.props.navigation.navigate("Index");
                }}>
                <FontAwesome5Icon
                  style={{fontSize: 25, paddingLeft: 5, color: "red"}}
                  name="angle-left"
                />
              </Button>
            </Left>
            <Body>
              <Title>D.S THI</Title>
            </Body>
            <Right>
              <Button
                transparent
                onPress={() => {
                  this.props.navigation.navigate("StudentCode");
                }}>
                <FontAwesome5Icon
                  style={{fontSize: 25, paddingLeft: 5, color: "red"}}
                  name="plus"
                />
              </Button>
            </Right>
          </Header>
          <View style={{flex: 1}}>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                marginTop: 20,
                height: "10%",
                zIndex: 20,
              }}>
              <View style={{width: "90%"}}>
                <DropDownPicker
                  items={this.state.tests}
                  placeholder={"Chọn mã đề thi"}
                  defaultValue={this.state.country}
                  containerStyle={{height: 40, width: "100%", zIndex: 2}}
                  style={{backgroundColor: "#fafafa"}}
                  itemStyle={{
                    justifyContent: "flex-start",
                  }}
                  dropDownStyle={{backgroundColor: "#fafafa", zIndex: 2}}
                  onChangeItem={(item) => this.onSelectedChange(item.value)}
                />
              </View>
            </View>
            <View style={{height: "80%", zIndex: 1}}>
              <View>
                <ListItem
                  thumbnail
                  style={{
                    borderBottomWidth: 1,
                    width: "90%",
                    zIndex: 1,
                    backgroundColor: "white",
                  }}>
                  <Left style={{width: "10%"}}>
                    <Text>STT</Text>
                  </Left>
                  <Body
                    style={{
                      flexDirection: "row",
                      width: "75%",
                      paddingRight: 5,
                      borderBottomWidth: 0,
                    }}>
                    <Text
                      style={{
                        width: "50%",
                        textAlign: "center",
                        marginLeft: 5,
                      }}>
                      Họ Tên
                    </Text>
                    <Text
                      style={{
                        width: "20%",
                        textAlign: "center",
                        marginLeft: -10,
                      }}>
                      Lớp
                    </Text>
                    <Text
                      style={{
                        width: "25%",
                        textAlign: "center",
                        marginLeft: 5,
                      }}>
                      Số B.D
                    </Text>
                  </Body>
                </ListItem>
              </View>
              {this.state.isEmpty && (
                <Content>
                  <View
                    style={{
                      paddingTop: "50%",
                      flexDirection: "row",
                      justifyContent: "center",
                      width: "100%",
                    }}>
                    <Text
                      style={{
                        fontStyle: "italic",
                        alignContent: "center",
                      }}>
                      Không Có Dữ Liệu
                    </Text>
                  </View>
                </Content>
              )}
              {!this.state.isEmpty && this.state.studentCodes.length && (
                <SafeAreaView style={{flex: 1}}>
                  <SwipeListView
                    data={this.state.studentCodes}
                    renderItem={(data, rowMap) => (
                      <ListItem
                        thumbnail
                        style={{
                          borderBottomWidth: 0,
                          width: "90%",
                          backgroundColor: data.index % 2 ? "#fafafa" : "white",
                        }}>
                        <Left style={{width: "10%"}}>
                          <Text style={{textAlign: "center", paddingLeft: 5}}>
                            {data.index + 1}
                          </Text>
                        </Left>
                        <Body
                          style={{
                            flexDirection: "row",
                            width: "65%",
                            paddingRight: 5,
                            borderBottomWidth: 0,
                          }}>
                          <Text
                            style={{
                              width: "50%",
                              textAlign: "center",
                              marginLeft: 5,
                            }}>
                            {(data.item as StudentCode).fullName}
                          </Text>
                          <Text
                            style={{
                              width: "20%",
                              textAlign: "center",
                              marginLeft: -10,
                            }}>
                            {(data.item as StudentCode).className}
                          </Text>
                          <Text
                            style={{
                              width: "35%",
                              textAlign: "center",
                              marginLeft: -10,
                            }}>
                            {(data.item as StudentCode).code}
                          </Text>
                        </Body>
                      </ListItem>
                    )}
                    renderHiddenItem={(data, rowMap) => (
                      <View
                        style={{
                          alignItems: "center",
                          flexDirection: "row",
                          justifyContent: "space-between",
                          paddingLeft: 15,
                          paddingRight: 15,
                          minHeight: "90%",
                        }}>
                        <View />
                        <View style={{flexDirection: "row", paddingRight: 20}}>
                          <TouchableOpacity
                            onPress={() =>
                              this.onHandlePressed(
                                this.DETAIL_TYPE,
                                (data.item as StudentCode).id,
                              )
                            }>
                            <FontAwesome5Icon
                              style={{fontSize: 25, paddingRight: 15}}
                              name={"eye"}
                            />
                          </TouchableOpacity>
                          <TouchableOpacity
                            onPress={() =>
                              this.onHandlePressed(
                                this.UPDATE_TYPE,
                                (data.item as StudentCode).id,
                              )
                            }>
                            <FontAwesome5Icon
                              style={{fontSize: 25, paddingRight: 15}}
                              name={"pencil-alt"}
                            />
                          </TouchableOpacity>
                          <TouchableOpacity
                            onPress={() =>
                              this.onHandlePressed(
                                this.DELETE_TYPE,
                                (data.item as StudentCode).id,
                              )
                            }>
                            <FontAwesome5Icon
                              style={{fontSize: 25}}
                              name={"trash"}
                            />
                          </TouchableOpacity>
                        </View>
                      </View>
                    )}
                    rightOpenValue={-130}
                    disableRightSwipe
                    keyExtractor={(item, index) => index.toString()}
                  />
                </SafeAreaView>
              )}
            </View>
          </View>
        </Container>
      </>
    );
  }
}
export default ListStudentCodeScreen;
