import React from "react";
import AwesomeLoading from "react-native-awesome-loading/index";
import {
  Body,
  Button,
  Container,
  Content,
  Header,
  Left,
  ListItem,
  Right,
  Title,
} from "native-base";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import {
  KeyboardAvoidingView,
  Platform,
  Text,
  TextInput,
  View,
} from "react-native";
import ProfileUpdatedInteractor, {
  IProfileUpdatedOutputPort,
} from "../../../domain/usecase/ProfileUpdatedInteractor";
import {RadioButton} from "react-native-paper";
import User from "../../../data/model/User";
import {connect} from "react-redux";
import {saveUserProfile} from "../../../domain/redux/action/user/UserAction";

class ProfileUpdatedScreen
  extends React.Component<any, any>
  implements IProfileUpdatedOutputPort {
  private profileUpdatedUC: ProfileUpdatedInteractor = ProfileUpdatedInteractor.created(
    this,
  );

  constructor(props: any) {
    super(props);
    this.state = {
      isCallingApi: false,
      isValidFirstName: true,
      isValidLastName: true,
      isValidPhoneNumber: true,
      currentUser: new User(),
    };
  }

  componentDidMount() {
    this.setForState("isCallingApi", true);
    this.profileUpdatedUC.getMyProfile();
  }

  public onInputChange = (field: string, value: string) => {
    this.setState((preState: any) => ({
      ...preState,
      currentUser: {
        ...this.state.currentUser,
        [field]: value,
      },
    }));
  };

  public onHandleChangeInput = (field: string, text: string) => {
    if (field === "firstName") {
      this.setState((preState: any) => ({
        ...preState,
        isValidFirstName: text.length >= 2,
      }));
    } else if (field === "lastName") {
      this.setState((preState: any) => ({
        ...preState,
        isValidLastName: text.length >= 2,
      }));
    } else if (field === "phoneNumber") {
      const regex = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g;
      this.setState((preState: any) => ({
        ...preState,
        isValidPhoneNumber: regex.test(text),
      }));
    } else if (field === "email") {
      const regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      this.setState((preState: any) => ({
        ...preState,
        isValidEmail: regExp.test(text),
      }));
    }
  };

  public onSubmitPressed() {
    if (
      this.state.isValidFirstName &&
      this.state.isValidLastName &&
      this.state.isValidPhoneNumber
    ) {
      this.setForState("isCallingApi", true);
      this.profileUpdatedUC.updateProfile(this.state.currentUser);
    }
  }

  private setForState(field: string, data: any) {
    this.setState((preState: any) => ({
      ...preState,
      [field]: data,
    }));
  }
  public setGender = (value: boolean | null) => {
    this.setState((preState: any) => ({
      ...preState,
      currentUser: {
        ...this.state.currentUser,
        gender: value,
      },
    }));
  };

  render() {
    return (
      <>
        <KeyboardAvoidingView
          behavior={Platform.OS ? "padding" : "height"}
          style={{
            flex: 1,
            backgroundColor: "#dddddd",
          }}>
          <AwesomeLoading
            indicatorId={8}
            size={50}
            isActive={this.state.isCallingApi}
            text="loading"
          />
          <Container>
            <Header style={{borderBottomColor: "red", borderBottomWidth: 1}}>
              <Left>
                <Button
                  transparent
                  onPress={() => {
                    this.props.navigation.goBack();
                  }}>
                  <FontAwesome5Icon
                    style={{fontSize: 25, paddingLeft: 5, color: "red"}}
                    name="angle-left"
                  />
                </Button>
              </Left>
              <Body>
                <Title>Cập nhật T.T</Title>
              </Body>
              <Right>
                <Button
                  transparent
                  onPress={() => {
                    this.onSubmitPressed();
                  }}>
                  <FontAwesome5Icon
                    style={{fontSize: 20, paddingLeft: 5, color: "red"}}
                    name="check"
                  />
                </Button>
              </Right>
            </Header>
            <Content>
              <Content>
                <ListItem>
                  <Body>
                    <Text
                      style={{
                        fontSize: 16,
                        fontWeight: "bold",
                        paddingVertical: 15,
                      }}>
                      Họ:
                    </Text>
                    <View>
                      <TextInput
                        style={{
                          height: 50,
                          width: "100%",
                          borderColor: "rgba(0,0,0,0.2)",
                          paddingLeft: 10,
                          marginVertical: 5,
                        }}
                        placeholder="Họ"
                        placeholderTextColor="gray"
                        onChangeText={(e) => this.onInputChange("lastName", e)}
                        onEndEditing={(e) =>
                          this.onHandleChangeInput(
                            "lastName",
                            e.nativeEvent.text,
                          )
                        }
                        value={this.state.currentUser.lastName}
                      />
                    </View>
                    {!this.state.isValidLastName && (
                      <View
                        style={{
                          flexDirection: "row",
                          marginTop: 10,
                        }}>
                        <Text style={{fontSize: 14, color: "red"}}>
                          *Tên có độ dài lớn hơn 2 kí tự
                        </Text>
                      </View>
                    )}
                  </Body>
                </ListItem>
                <ListItem>
                  <Body>
                    <Text
                      style={{
                        fontSize: 16,
                        fontWeight: "bold",
                        paddingVertical: 15,
                      }}>
                      Tên:
                    </Text>
                    <View>
                      <TextInput
                        style={{
                          height: 50,
                          width: "100%",
                          borderColor: "rgba(0,0,0,0.2)",
                          paddingLeft: 10,
                          marginVertical: 5,
                        }}
                        placeholder="Tên"
                        placeholderTextColor="gray"
                        onChangeText={(e) => this.onInputChange("firstName", e)}
                        onEndEditing={(e) =>
                          this.onHandleChangeInput(
                            "firstName",
                            e.nativeEvent.text,
                          )
                        }
                        value={this.state.currentUser.firstName}
                      />
                    </View>
                    {!this.state.isValidFirstName && (
                      <View
                        style={{
                          flexDirection: "row",
                          marginTop: 10,
                        }}>
                        <Text style={{fontSize: 14, color: "red"}}>
                          *Tên có độ dài lớn hơn 2 kí tự
                        </Text>
                      </View>
                    )}
                  </Body>
                </ListItem>
                <ListItem>
                  <Body>
                    <Text
                      style={{
                        fontSize: 16,
                        fontWeight: "bold",
                        paddingVertical: 15,
                      }}>
                      Giới tính:
                    </Text>
                    <View style={{width: "90%"}}>
                      <ListItem
                        style={{borderBottomWidth: 0}}
                        onPress={() => this.setGender(true)}>
                        <Body
                          style={{
                            flexDirection: "row",
                            justifyContent: "space-between",
                          }}>
                          <Text style={{fontSize: 16, color: "#1877f2"}}>
                            Nam
                          </Text>
                          <RadioButton
                            value="Second"
                            status={
                              this.state.currentUser.gender
                                ? "checked"
                                : "unchecked"
                            }
                            onPress={() => this.setGender(true)}
                          />
                        </Body>
                      </ListItem>
                      <ListItem
                        style={{borderBottomWidth: 0}}
                        onPress={() => this.setGender(false)}>
                        <Body
                          style={{
                            flexDirection: "row",
                            justifyContent: "space-between",
                          }}>
                          <Text style={{fontSize: 16, color: "#1877f2"}}>
                            Nữ
                          </Text>
                          <RadioButton
                            value="Second"
                            status={
                              this.state.currentUser.gender === false
                                ? "checked"
                                : "unchecked"
                            }
                            onPress={() => this.setGender(false)}
                          />
                        </Body>
                      </ListItem>
                    </View>
                  </Body>
                </ListItem>
                <ListItem>
                  <Body>
                    <Text
                      style={{
                        fontSize: 16,
                        fontWeight: "bold",
                        paddingVertical: 15,
                      }}>
                      Số điện thoại:
                    </Text>
                    <View>
                      <TextInput
                        style={{
                          height: 50,
                          width: "100%",
                          borderColor: "rgba(0,0,0,0.2)",
                          paddingLeft: 10,
                          marginVertical: 5,
                        }}
                        placeholder="Số điện thoại"
                        placeholderTextColor="gray"
                        onChangeText={(e) =>
                          this.onInputChange("phoneNumber", e)
                        }
                        onEndEditing={(e) =>
                          this.onHandleChangeInput(
                            "phoneNumber",
                            e.nativeEvent.text,
                          )
                        }
                        value={this.state.currentUser.phoneNumber}
                      />
                    </View>
                    {!this.state.isValidPhoneNumber && (
                      <View
                        style={{
                          flexDirection: "row",
                          marginTop: 10,
                        }}>
                        <Text style={{fontSize: 14, color: "red"}}>
                          *Số điện thoại không hợp lệ
                        </Text>
                      </View>
                    )}
                  </Body>
                </ListItem>
              </Content>
            </Content>
          </Container>
        </KeyboardAvoidingView>
      </>
    );
  }

  getMyProfileError(): void {
    this.setForState("isCallingApi", false);
  }

  getMyProfileSuccessfully(user: any): void {
    this.setState((preState: any) => ({
      ...preState,
      isCallingApi: false,
      currentUser: user,
    }));
  }
  updateProfileSuccessfully(user: User): void {
    this.props.saveUserProfile(user);
    this.setForState("isCallingApi", false);
    this.props.navigation.navigate("Index");
  }
  updateProfileError(): void {
    throw new Error("Method not implemented.");
  }
}
const mapStateToProps = (state: any) => {
  return {
    token: state.UserReducers.USER_TOKEN,
    userInfo: state.UserReducers.USER_INFO,
  };
};
export default connect(mapStateToProps, {saveUserProfile})(
  ProfileUpdatedScreen,
);
