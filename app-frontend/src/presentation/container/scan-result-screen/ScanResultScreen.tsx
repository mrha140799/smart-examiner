import React from "react";
import ScanResultScreenStyles from "./ScanResultScreenStyles";
import {
  Body,
  Button,
  Container,
  Content,
  Header,
  Left,
  ListItem,
  Right,
  Title,
  Text,
} from "native-base";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import {TouchableOpacity} from "react-native";
import Moment from "moment";

export default class ScanResultScreen extends React.Component<any, any> {
  private styles = ScanResultScreenStyles.styles;

  constructor(props: any) {
    super(props);
  }

  render() {
    return (
      <Container>
        <Header style={{borderBottomColor: "red", borderBottomWidth: 1}}>
          <Left>
            <Button
              transparent
              onPress={() => {
                this.props.navigation.goBack();
              }}>
              <FontAwesome5Icon
                style={{fontSize: 25, paddingLeft: 5, color: "red"}}
                name="angle-left"
              />
            </Button>
          </Left>
          <Body>
            <Title>
              {this.props.route.params.screenType === "history"
                ? "THÔNG TIN"
                : "CHẤM ĐIỂM"}
            </Title>
          </Body>
          <Right>
            {!this.props.route.params.screenType && (
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("Index");
                }}>
                <Text style={{color: "blue"}}>Xong</Text>
              </TouchableOpacity>
            )}
          </Right>
        </Header>
        <Content>
          <ListItem>
            <Body>
              <Text style={{fontWeight: "bold"}}>Họ Tên</Text>
              <Text style={{marginTop: 10}}>
                {this.props.route.params.scanData.studentFullName}
              </Text>
            </Body>
          </ListItem>
          <ListItem>
            <Body>
              <Text style={{fontWeight: "bold"}}>Số Báo Danh</Text>
              <Text style={{marginTop: 10}}>
                {this.props.route.params.scanData.studentCode}
              </Text>
            </Body>
          </ListItem>
          <ListItem>
            <Body>
              <Text style={{fontWeight: "bold"}}>Mã Đề Thi</Text>
              <Text style={{marginTop: 10}}>
                {this.props.route.params.scanData.testCode}
              </Text>
            </Body>
          </ListItem>
          <ListItem>
            <Body>
              <Text style={{fontWeight: "bold"}}>Điểm Thi</Text>
              <Text style={{marginTop: 10}}>
                {this.props.route.params.scanData.score}
              </Text>
            </Body>
          </ListItem>
          <ListItem>
            <Body>
              <Text style={{fontWeight: "bold"}}>Số Câu Đúng</Text>
              <Text style={{marginTop: 10}}>
                {this.props.route.params.scanData.numberQuestionCorrect}
              </Text>
            </Body>
          </ListItem>
          <ListItem>
            <Body>
              <Text style={{fontWeight: "bold"}}>Môn Thi</Text>
              <Text style={{marginTop: 10}}>
                {this.props.route.params.scanData.subjectName}
              </Text>
            </Body>
          </ListItem>
          <ListItem>
            <Body>
              <Text style={{fontWeight: "bold"}}>Ngày Thi</Text>
              <Text style={{marginTop: 10}}>
                {this.props.route.params.scanData.testDay
                  ? Moment(this.props.route.params.scanData.testDay).format(
                      "DD/MM/YYYY",
                    )
                  : "Không có thông tin"}
              </Text>
            </Body>
          </ListItem>
          <ListItem>
            <Body>
              <Text style={{fontWeight: "bold"}}>Thông Tin Bài Thi</Text>
              <TouchableOpacity
                onPress={() => {
                  const scanData = this.props.route.params.scanData;
                  this.props.navigation.navigate("TestInfoScreen", {
                    scanData: scanData,
                  });
                }}>
                <Text
                  style={{marginTop: 10, fontStyle: "italic", color: "blue"}}>
                  Ấn vào đây để xem ảnh chi tiết
                </Text>
              </TouchableOpacity>
            </Body>
          </ListItem>
        </Content>
      </Container>
    );
  }
}
