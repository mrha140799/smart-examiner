import {StyleSheet} from "react-native";

export default class ScanResultScreenStyles {
  public static styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
    },
  });
}
