import React from "react";
import AwesomeLoading from "react-native-awesome-loading/index";
import {
  Body,
  Button,
  Container,
  Content,
  Header,
  Left,
  Right,
  Text,
  Title,
  View,
} from "native-base";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import {Alert, TextInput} from "react-native";
import * as Animatable from "react-native-animatable";
import Moment from "moment";
import DropDownPicker from "react-native-dropdown-picker";
import StudentCodeInteractor, {
  IStudentCodeOutputPort,
} from "../../../domain/usecase/StudentCodeInteractor";
import Classroom from "../../../data/model/Classroom";
import Test from "../../../data/model/Test";
import StudentCodeRequest from "../../../data/model/StudentCodeRequest";
import Student from "../../../data/model/Student";

export default class StudentCodeScreen
  extends React.Component<any, any>
  implements IStudentCodeOutputPort {
  private readonly DETAIL_TYPE: string = "detail";
  private readonly UPDATE_TYPE: string = "update";
  private studentCodeUC: StudentCodeInteractor = StudentCodeInteractor.create(
    this,
  );
  private studentController: any;

  constructor(props: any) {
    super(props);
    this.state = {
      screenType: "create",
      isLoading: false,
      dateOfBirth: new Date(),
      studentCodeId: null,
      isShowDatePicker: false,
      screenName: "TẠO S.B.D",
      studentId: null,
      code: "",
      classroomId: "",
      testId: null,
      validCode: true,
      validTestId: true,
      validStudent: true,
      isNotExistsCode: true,
      isNotExistsStudent: true,
      studentController: null,
      studentIdTemp: null,
      students: [],
      classrooms: [],
      tests: [],
    };
  }

  public onSubmitPressed() {
    if (!this.state.studentId) {
      this.setForState("isNotExistsStudent", false);
    }
    if (!this.state.testId) {
      this.setForState("validTestId", false);
    }
    if (this.state.code.trim().length < 7) {
      this.setForState("validCode", false);
    }
    if (
      this.state.validTestId &&
      this.state.validCode &&
      this.state.isNotExistsStudent &&
      this.state.isNotExistsCode
    ) {
      this.setForState("isCallingApi", true);
      const studentCode = new StudentCodeRequest();
      studentCode.testId = this.state.testId;
      studentCode.code = this.state.code;
      studentCode.id = this.state.studentCodeId;
      studentCode.studentId = this.state.studentId;
      if (this.state.screenType === this.UPDATE_TYPE) {
        this.studentCodeUC.updateOne(studentCode);
      } else {
        this.studentCodeUC.createOne(studentCode);
      }
    }
  }
  public onInputChange = (field: string, value: any) => {
    if (field === "classroomId") {
      this.setForState("isCallingApi", true);
      this.studentCodeUC.getStudentByClassId(value);
    }
    if (field === "studentId") {
      const studentChoice = this.state.students.find(
        (e: any) => e.id === value,
      );
      this.setState((preState: any) => ({
        ...preState,
        dateOfBirth: studentChoice ? studentChoice.dateOfBirth : new Date(),
        validStudent: true,
        isNotExistsStudent: true,
      }));
    } else if (field === "testId") {
      this.setState((preState: any) => ({
        ...preState,
        validStudent: true,
      }));
    }
    this.setState((preState: any) => ({
      ...preState,
      [field]: value,
    }));
  };
  public onHandleChangeInput = (field: string, text: string) => {
    if (field === "code") {
      this.setForState("validCode", text.trim().length === 7);
      this.setForState("isNotExistsCode", true);
    }
  };
  public onVisibleDate = () => {
    if (this.state.screenType !== this.DETAIL_TYPE) {
      this.setState((preState: any) => ({
        ...preState,
        isShowDatePicker: true,
      }));
    }
  };
  public onDateChange = (date?: Date) => {
    this.setState((preState: any) => ({
      ...preState,
      dateOfBirth: date,
      isShowDatePicker: false,
    }));
  };
  public onHandleHideDatePicker = () => {
    this.setState((preState: any) => ({
      ...preState,
      isShowDatePicker: false,
    }));
  };
  private setForState(field: string, data: any) {
    this.setState((preState: any) => ({
      ...preState,
      [field]: data,
    }));
  }
  componentDidMount() {
    this.setForState("isCallingApi", true);
    if (this.props.route.params) {
      if (this.props.route.params.actionType === this.DETAIL_TYPE) {
        this.setState((preState: any) => ({
          ...preState,
          screenType: this.DETAIL_TYPE,
          screenName: "XEM S.B.D",
          isCallingApi: true,
        }));
        this.studentCodeUC.getDataInit(this.props.route.params.studentCodeId);
      } else if (this.props.route.params.actionType === this.UPDATE_TYPE) {
        this.setState((preState: any) => ({
          ...preState,
          screenType: this.UPDATE_TYPE,
          screenName: "SỬA S.B.D",
          isCallingApi: true,
        }));
        this.studentCodeUC.getDataInit(this.props.route.params.studentCodeId);
      }
    }
    this.studentCodeUC.getDataInit();
  }
  private formatInToDropDownItems(tests: Array<any>, label: string) {
    return tests.map((e) => {
      return {
        label: e[label],
        value: e.id,
        ...e,
      };
    });
  }
  render() {
    return (
      <>
        <AwesomeLoading
          indicatorId={8}
          size={50}
          isActive={this.state.isCallingApi}
          text="loading"
        />
        <Container>
          <Header style={{borderBottomColor: "red", borderBottomWidth: 1}}>
            <Left>
              <Button
                transparent
                onPress={() => {
                  this.props.navigation.goBack();
                }}>
                <FontAwesome5Icon
                  style={{fontSize: 25, paddingLeft: 5, color: "red"}}
                  name="angle-left"
                />
              </Button>
            </Left>
            <Body>
              <Title>{this.state.screenName}</Title>
            </Body>
            <Right>
              {this.state.screenType !== this.DETAIL_TYPE && (
                <Button
                  transparent
                  onPress={() => {
                    this.onSubmitPressed();
                  }}>
                  <FontAwesome5Icon
                    style={{fontSize: 20, paddingLeft: 5, color: "red"}}
                    name="check"
                  />
                </Button>
              )}
            </Right>
          </Header>
          <Content style={{paddingTop: 20}}>
            <View style={{marginVertical: 10, zIndex: 4}}>
              <View style={{marginHorizontal: 10, marginBottom: 5}}>
                <Text>Lớp:</Text>
              </View>
              <View>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "center",
                  }}>
                  <View style={{width: "90%"}}>
                    <DropDownPicker
                      disabled={
                        this.state.screenType === this.DETAIL_TYPE ||
                        this.state.screenType === this.UPDATE_TYPE
                      }
                      items={this.state.classrooms}
                      defaultValue={this.state.classroomId}
                      placeholder={"Chọn lớp"}
                      containerStyle={{height: 40, width: "100%", zIndex: 2}}
                      style={{backgroundColor: "white"}}
                      itemStyle={{
                        justifyContent: "flex-start",
                      }}
                      dropDownStyle={{backgroundColor: "#fafafa", zIndex: 2}}
                      onChangeItem={(item) => {
                        this.onInputChange("classroomId", item.value);
                      }}
                    />
                  </View>
                </View>
              </View>
            </View>
            <View style={{marginVertical: 10, zIndex: 3}}>
              <View style={{marginHorizontal: 10, marginBottom: 5}}>
                <Text>Thí sinh:</Text>
              </View>
              <View>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "center",
                  }}>
                  <View style={{width: "90%"}}>
                    <DropDownPicker
                      disabled={
                        this.state.screenType === this.DETAIL_TYPE ||
                        this.state.screenType === this.UPDATE_TYPE
                      }
                      items={this.state.students}
                      defaultValue={this.state.studentId}
                      placeholder={"Chọn thí sinh"}
                      containerStyle={{height: 40, width: "100%", zIndex: 2}}
                      style={{backgroundColor: "white"}}
                      itemStyle={{
                        justifyContent: "flex-start",
                      }}
                      controller={(instance) =>
                        (this.studentController = instance)
                      }
                      dropDownStyle={{backgroundColor: "#fafafa", zIndex: 2}}
                      onChangeItem={(item) => {
                        this.onInputChange("studentId", item.value);
                      }}
                    />
                  </View>
                </View>
              </View>
            </View>
            {!this.state.validStudent && (
              <Animatable.View animation="fadeInLeft" duration={500}>
                <Text
                  style={{
                    fontSize: 14,
                    alignContent: "center",
                    marginHorizontal: 20,
                    fontStyle: "italic",
                    color: "red",
                  }}>
                  * Thí sinh đã tồn tại trong bài kiểm tra này
                </Text>
              </Animatable.View>
            )}
            {!this.state.isNotExistsStudent && (
              <Animatable.View animation="fadeInLeft" duration={500}>
                <Text
                  style={{
                    fontSize: 14,
                    alignContent: "center",
                    marginHorizontal: 20,
                    fontStyle: "italic",
                    color: "red",
                  }}>
                  * Bắt buộc
                </Text>
              </Animatable.View>
            )}
            <View style={{marginVertical: 10}}>
              <View>
                <View style={{marginHorizontal: 10, marginBottom: 5}}>
                  <Text>Ngày sinh:</Text>
                </View>
                <View
                  style={{
                    height: 40,
                    width: "90%",
                    borderColor: "rgba(0,0,0,0.2)",
                    borderRadius: 10,
                    borderWidth: 0.5,
                    marginHorizontal: 20,
                    paddingLeft: 10,
                    marginVertical: 5,
                    flexDirection: "row",
                    alignItems: "center",
                  }}>
                  <Text
                    style={{
                      alignContent: "center",
                      color: "#9d9d9d",
                      fontSize: 14,
                    }}>
                    {Moment(this.state.dateOfBirth).format("DD/MM/YYYY")}
                  </Text>
                </View>
              </View>
            </View>
            <View style={{marginVertical: 10, zIndex: 2}}>
              <View style={{marginHorizontal: 10, marginBottom: 5}}>
                <Text>Mã đề thi:</Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                }}>
                <View style={{width: "90%"}}>
                  <DropDownPicker
                    disabled={
                      this.state.screenType === this.DETAIL_TYPE ||
                      this.state.screenType === this.UPDATE_TYPE
                    }
                    items={this.state.tests}
                    placeholder={"Chọn mã đề thi"}
                    defaultValue={this.state.testId ?? ""}
                    containerStyle={{height: 40, width: "100%", zIndex: 2}}
                    style={{backgroundColor: "white"}}
                    itemStyle={{
                      justifyContent: "flex-start",
                    }}
                    dropDownStyle={{backgroundColor: "#fafafa", zIndex: 2}}
                    onChangeItem={(item) => {
                      this.onInputChange("testId", item.value);
                    }}
                  />
                </View>
              </View>
              {!this.state.validTestId && (
                <Animatable.View animation="fadeInLeft" duration={500}>
                  <Text
                    style={{
                      fontSize: 14,
                      alignContent: "center",
                      marginHorizontal: 20,
                      fontStyle: "italic",
                      color: "red",
                    }}>
                    * Bắt buộc
                  </Text>
                </Animatable.View>
              )}
            </View>
            <View style={{marginVertical: 10}}>
              <View style={{marginHorizontal: 10, marginBottom: 5}}>
                <Text>Số báo danh:</Text>
              </View>
              <View>
                <TextInput
                  style={
                    this.state.screenType === this.DETAIL_TYPE
                      ? {
                          height: 40,
                          width: "90%",
                          borderColor: "rgba(0,0,0,0.2)",
                          borderRadius: 10,
                          borderWidth: 0.5,
                          marginHorizontal: 20,
                          paddingLeft: 10,
                          marginVertical: 5,
                          color: "#9d9d9d",
                        }
                      : {
                          height: 40,
                          width: "90%",
                          borderColor: "rgba(0,0,0,0.2)",
                          borderRadius: 10,
                          borderWidth: 0.5,
                          marginHorizontal: 20,
                          paddingLeft: 10,
                          marginVertical: 5,
                        }
                  }
                  keyboardType={"numeric"}
                  editable={this.state.screenType !== this.DETAIL_TYPE}
                  placeholder="Số báo danh"
                  placeholderTextColor="gray"
                  onChangeText={(e) => this.onInputChange("code", e)}
                  onEndEditing={(e) =>
                    this.onHandleChangeInput("code", e.nativeEvent.text)
                  }
                  value={this.state.code}
                />
                {!this.state.validCode && (
                  <Animatable.View animation="fadeInLeft" duration={500}>
                    <Text
                      style={{
                        fontSize: 14,
                        alignContent: "center",
                        marginHorizontal: 20,
                        fontStyle: "italic",
                        color: "red",
                      }}>
                      * Số báo danh có 7 chữ số
                    </Text>
                  </Animatable.View>
                )}
                {!this.state.isNotExistsCode && (
                  <Animatable.View animation="fadeInLeft" duration={500}>
                    <Text
                      style={{
                        fontSize: 14,
                        alignContent: "center",
                        marginHorizontal: 20,
                        fontStyle: "italic",
                        color: "red",
                      }}>
                      * Đã tồn tại số báo danh tương ứng với đề thi
                    </Text>
                  </Animatable.View>
                )}
              </View>
            </View>
          </Content>
        </Container>
      </>
    );
  }

  callApiFinally(): any {}

  getDataInitError(): any {
    this.setForState("isCallingApi", false);
    Alert.alert("Lỗi, không thế lấy thông tin liên quan");
  }

  getDataInitSuccessfully(
    classrooms: Array<Classroom>,
    tests: Array<Test>,
    students: Array<Student>,
    studentCode: StudentCodeRequest | null,
  ): any {
    if (
      this.state.screenType === this.UPDATE_TYPE ||
      this.state.screenType === this.DETAIL_TYPE
    ) {
      if (studentCode) {
        const studentChoice: Student | undefined = students.find(
          (e: any) => e.id === studentCode.studentId,
        );
        this.setState((preState: any) => ({
          ...preState,
          tests: students.length
            ? this.formatInToDropDownItems(tests, "code")
            : [],
          classrooms: classrooms.length
            ? this.formatInToDropDownItems(classrooms, "name")
            : [],
          testId: studentCode.testId,
          code: studentCode.code,
          studentIdTemp: studentCode.studentId,
          classroomId: studentChoice?.classroom.id,
          studentCodeId: studentCode.id,
        }));
        if (
          studentChoice &&
          studentChoice.classroom &&
          studentChoice.classroom.id
        ) {
          this.studentCodeUC.getStudentByClassId(studentChoice.classroom.id);
        } else {
          this.setForState("isCallingApi", false);
        }
      }
    } else {
      this.setState((preState: any) => ({
        ...preState,
        testId: tests.length ? tests[0].id : "",
        tests: tests.length ? this.formatInToDropDownItems(tests, "code") : [],
        students: students.length
          ? this.formatInToDropDownItems(students, "fullName")
          : [],
        classrooms: classrooms.length
          ? this.formatInToDropDownItems(classrooms, "name")
          : [],
        classroomId: classrooms.length ? classrooms[0].id : "",
        isCallingApi: false,
      }));
    }
  }

  createOneError(): any {
    this.setForState("isCallingApi", false);
    Alert.alert("Lỗi, Thêm mới thất bại!");
  }

  invalidCode(): any {
    this.setState((preState: any) => ({
      ...preState,
      isCallingApi: false,
      isNotExistsCode: false,
    }));
  }

  createOneSuccessfully(): any {
    this.setForState("isCallingApi", false);
    Alert.alert("Thêm mới thành công!");
    this.props.navigation.navigate("ListStudentCode");
  }

  updateOneSuccessfully(): any {
    this.setForState("isCallingApi", false);
    Alert.alert("Sửa thí sinh thành công!");
    this.props.navigation.navigate("ListStudentCode");
  }

  updateOneError(): any {
    this.setForState("isCallingApi", false);
    Alert.alert("Lỗi, sửa thí sinh thất bại!");
  }

  invalidStudent(): any {
    this.setState((preState: any) => ({
      ...preState,
      isCallingApi: false,
      validStudent: false,
    }));
  }

  getStudentByClassIdError(): any {
    this.setForState("isCallingApi", false);
  }

  getStudentByClassIdSuccessfully(students: Array<Student>): any {
    this.studentController.reset();
    this.setState((preState: any) => ({
      ...preState,
      isCallingApi: false,
      students: students.length
        ? this.formatInToDropDownItems(students, "fullName")
        : [],
      studentId: this.state.studentIdTemp ?? null,
    }));
  }
}
