import React from "react";
import {
  Body,
  Button,
  Container,
  Content,
  Header,
  Left,
  ListItem,
  Right,
  Text,
  Title,
  View,
} from "native-base";
import AwesomeLoading from "react-native-awesome-loading/index";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import Test from "../../../data/model/Test";
import Moment from "moment";
import {Alert, SafeAreaView, TouchableOpacity} from "react-native";
import {SwipeListView} from "react-native-swipe-list-view";
import TestListInteractor, {
  ITestListOutputPort,
} from "../../../domain/usecase/TestListInteractor";

class ListTestScreen
  extends React.Component<any, any>
  implements ITestListOutputPort {
  private testListUC: TestListInteractor = TestListInteractor.create(this);
  private readonly DETAIL_TYPE: string = "detail";
  private readonly UPDATE_TYPE: string = "update";
  private readonly DELETE_TYPE: string = "delete";
  constructor(props: any) {
    super(props);
    this.state = {
      isCallingApi: false,
      isEmpty: true,
      tests: [new Test()],
    };
  }

  private setForState(field: string, data: any) {
    this.setState((preState: any) => ({
      ...preState,
      [field]: data,
    }));
  }

  public onHandlePressed(type: string, id: number | null) {
    if (type === this.DELETE_TYPE) {
      Alert.alert("Xóa bài thi", " Bạn có chắc muốn xóa bài thi này?", [
        {
          text: "Hủy",
          onPress: () => {},
          style: "cancel",
        },
        {
          text: "Đồng ý",
          onPress: () => {
            this.setForState("isCallingApi", true);
            this.testListUC.deleteById(id);
          },
        },
      ]);
    } else if (type === this.UPDATE_TYPE) {
      this.props.navigation.navigate("Test", {
        actionType: this.UPDATE_TYPE,
        testId: id,
      });
    } else if (type === this.DETAIL_TYPE) {
      this.props.navigation.navigate("Test", {
        actionType: this.DETAIL_TYPE,
        testId: id,
      });
    }
  }

  callApiFinally(): any {}

  deleteByIdError(): any {
    this.setForState("isCallingApi", false);
    Alert.alert("Lỗi, không thể xóa bài thi!");
  }

  deleteByIdSuccessfully(): any {
    Alert.alert("Xóa bài thi thành công!");
    this.testListUC.getMyTests();
  }
  componentDidMount() {
    this.setForState("isCallingApi", true);
    this.testListUC.getMyTests();
  }

  getMyTestsError(): any {
    this.setForState("isEmpty", true);
    this.setForState("isCallingApi", false);
  }

  getMyTestsSuccessfully(tests: Array<Test>): any {
    if (tests.length === 0) {
      this.setForState("isEmpty", true);
      this.setForState("isCallingApi", false);
    } else {
      this.setState((preState: any) => ({
        ...preState,
        isCallingApi: false,
        isEmpty: false,
        tests: tests,
      }));
    }
  }
  render() {
    return (
      <>
        <AwesomeLoading
          indicatorId={8}
          size={50}
          isActive={this.state.isCallingApi}
          text="loading"
        />
        <Container>
          <Header style={{borderBottomColor: "red", borderBottomWidth: 1}}>
            <Left>
              <Button
                transparent
                onPress={() => {
                  this.props.navigation.navigate("Index");
                }}>
                <FontAwesome5Icon
                  style={{fontSize: 25, paddingLeft: 5, color: "red"}}
                  name="angle-left"
                />
              </Button>
            </Left>
            <Body>
              <Title>D.S BÀI TEST</Title>
            </Body>
            <Right>
              <Button
                transparent
                onPress={() => {
                  this.props.navigation.navigate("Test");
                }}>
                <FontAwesome5Icon
                  style={{fontSize: 25, paddingLeft: 5, color: "red"}}
                  name="plus"
                />
              </Button>
            </Right>
          </Header>
          {this.state.isEmpty && (
            <Content>
              <View
                style={{
                  paddingTop: "50%",
                  flexDirection: "row",
                  justifyContent: "center",
                  width: "100%",
                }}>
                <Text
                  style={{
                    fontStyle: "italic",
                    alignContent: "center",
                  }}>
                  Không Có Dữ Liệu
                </Text>
              </View>
            </Content>
          )}
          {!this.state.isEmpty && this.state.tests.length && (
            <SafeAreaView style={{flex: 1}}>
              <SwipeListView
                data={this.state.tests}
                renderItem={(data, rowMap) => (
                  <ListItem thumbnail style={{backgroundColor: "white"}}>
                    <Left style={{width: "5%"}}>
                      <Text>{data.index + 1}</Text>
                    </Left>
                    <Body>
                      <View style={{flexDirection: "row"}}>
                        <Text>Mã đề thi: </Text>
                        <Text style={{fontStyle: "italic", fontWeight: "bold"}}>
                          {(data.item as Test).code}{" "}
                        </Text>
                        <Text>- Ngày thi: </Text>
                        <Text style={{fontStyle: "italic", fontWeight: "bold"}}>
                          {" "}
                          {Moment((data.item as Test).testDay).format(
                            "DD/MM/YYYY",
                          )}
                        </Text>
                      </View>
                      <View style={{flexDirection: "row", paddingTop: 5}}>
                        <Text>Tên môn thi: </Text>
                        <Text style={{fontStyle: "italic", fontWeight: "bold"}}>
                          {" "}
                          {(data.item as Test).subject}
                        </Text>
                      </View>
                    </Body>
                  </ListItem>
                )}
                renderHiddenItem={(data, rowMap) => (
                  <View
                    style={{
                      alignItems: "center",
                      flexDirection: "row",
                      justifyContent: "space-between",
                      paddingLeft: 15,
                      paddingRight: 15,
                      minHeight: "100%",
                    }}>
                    <View />
                    <View style={{flexDirection: "row"}}>
                      <TouchableOpacity
                        onPress={() =>
                          this.onHandlePressed(
                            this.DETAIL_TYPE,
                            (data.item as Test).id,
                          )
                        }>
                        <FontAwesome5Icon
                          style={{fontSize: 25, paddingRight: 15}}
                          name={"eye"}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() =>
                          this.onHandlePressed(
                            this.UPDATE_TYPE,
                            (data.item as Test).id,
                          )
                        }>
                        <FontAwesome5Icon
                          style={{fontSize: 25, paddingRight: 15}}
                          name={"pencil-alt"}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() =>
                          this.onHandlePressed(
                            this.DELETE_TYPE,
                            (data.item as Test).id,
                          )
                        }>
                        <FontAwesome5Icon
                          style={{fontSize: 25}}
                          name={"trash"}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                )}
                rightOpenValue={-130}
                disableRightSwipe
                keyExtractor={(item, index) => index.toString()}
              />
            </SafeAreaView>
          )}
        </Container>
      </>
    );
  }
}
export default ListTestScreen;
