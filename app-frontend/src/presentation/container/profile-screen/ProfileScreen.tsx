import React from "react";
import ProfileScreenStyles from "./ProfileScreenStyles";
import AwesomeLoading from "react-native-awesome-loading/index";
import {
  Body,
  Container,
  Content,
  Header,
  Left,
  ListItem,
  Right,
  Thumbnail,
  Title,
} from "native-base";
import ProfileInteractor, {
  IProfileOutputPort,
} from "../../../domain/usecase/ProfileInteractor";
import MasterData from "../../../assets/json/Commons.json";
import {Text, TouchableOpacity} from "react-native";
import {connect} from "react-redux";
import ImagePicker from "react-native-image-crop-picker";
import {saveUserProfile} from "../../../domain/redux/action/user/UserAction";

class ProfileScreen
  extends React.Component<any, any>
  implements IProfileOutputPort {
  private styles = ProfileScreenStyles.styles;
  private readonly IMAGE_URI: string = MasterData["image-uri"];
  private profileUC: ProfileInteractor = ProfileInteractor.created(this);
  constructor(props: any) {
    super(props);
    this.state = {
      isCallingApi: false,
      currentUser: null,
    };
  }
  changeAvatarError(): void {
    this.setForState("isCallingApi", false);
  }

  changeAvatarSuccessfully(avatarName: string): void {
    this.setForState("isCallingApi", false);
    this.props.saveUserProfile({
      ...this.props.userInfo,
      imageAvatarName: avatarName,
    });
  }
  componentDidMount() {}

  private setForState(field: string, data: any) {
    this.setState((preState: any) => ({
      ...preState,
      [field]: data,
    }));
  }

  render() {
    return (
      <>
        <AwesomeLoading
          indicatorId={8}
          size={50}
          isActive={this.state.isCallingApi}
          text="loading"
        />
        <Container>
          <Header style={{borderBottomColor: "red", borderBottomWidth: 1}}>
            <Left />
            <Body>
              <Title>Thông tin</Title>
            </Body>
            <Right />
          </Header>
          <Content>
            <ListItem thumbnail style={{paddingTop: 5}}>
              <Left>
                <Thumbnail
                  source={
                    this.props.userInfo && this.props.userInfo.imageAvatarName
                      ? {
                          uri:
                            this.IMAGE_URI +
                            this.props.userInfo.imageAvatarName,
                        }
                      : require("../../../assets/image/default_user.png")
                  }
                  onLoadStart={() => {
                    this.setState((preState: any) => ({
                      ...preState,
                      isLoadImageFull: true,
                    }));
                  }}
                  onLoadEnd={() => {
                    this.setState((preState: any) => ({
                      ...preState,
                      isLoadImageFull: false,
                    }));
                  }}
                />
              </Left>
              <Body>
                <TouchableOpacity
                  onPress={() => {
                    ImagePicker.openPicker({
                      width: 300,
                      height: 400,
                      cropping: true,
                      includeBase64: true,
                    })
                      .then((image: any) => {
                        this.setForState("isCallingApi", true);
                        this.profileUC.changeAvatar(image.data);
                      })
                      .catch(() => {
                        console.log("Cancel");
                      });
                  }}>
                  <Text style={{fontSize: 16}}>Thêm/sửa ảnh đại diện</Text>
                </TouchableOpacity>
              </Body>
            </ListItem>
            <ListItem>
              <Body>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: "bold",
                    paddingVertical: 15,
                  }}>
                  Họ:
                </Text>
                <Text style={{fontSize: 16}}>
                  {this.props.userInfo
                    ? this.props.userInfo.lastName
                    : "Ẩn danh"}
                </Text>
              </Body>
            </ListItem>
            <ListItem>
              <Body>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: "bold",
                    paddingVertical: 15,
                  }}>
                  Tên:
                </Text>
                <Text style={{fontSize: 16}}>
                  {this.props.userInfo
                    ? this.props.userInfo.firstName
                    : "Ẩn danh"}
                </Text>
              </Body>
            </ListItem>
            <ListItem>
              <Body>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: "bold",
                    paddingVertical: 15,
                  }}>
                  Giới tính:
                </Text>
                <Text style={{fontSize: 16}}>
                  {this.props.userInfo
                    ? this.props.userInfo.gender
                      ? "Nam"
                      : "Nữ"
                    : "Không xác định"}
                </Text>
              </Body>
            </ListItem>
            <ListItem>
              <Body>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: "bold",
                    paddingVertical: 15,
                  }}>
                  Địa chỉ Email:
                </Text>
                <Text style={{fontSize: 16, fontStyle: "italic"}}>
                  {this.props.userInfo
                    ? this.props.userInfo.email ?? "Chưa cập nhật"
                    : "Chưa cập nhật"}
                </Text>
              </Body>
            </ListItem>
            <ListItem>
              <Body>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: "bold",
                    paddingVertical: 15,
                  }}>
                  Số điện thoại:
                </Text>
                <Text style={{fontSize: 16}}>
                  {this.props.userInfo
                    ? this.props.userInfo.phoneNumber ?? "Chưa cập nhật"
                    : "Chưa cập nhật"}
                </Text>
              </Body>
            </ListItem>
            <ListItem>
              <Body>
                <TouchableOpacity>
                  <Text
                    style={{
                      fontSize: 16,
                      fontWeight: "bold",
                      paddingVertical: 15,
                    }}>
                    Đổi mật khẩu đăng nhập
                  </Text>
                </TouchableOpacity>
              </Body>
            </ListItem>
            <ListItem>
              <Body>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("ProfileUpdated");
                  }}>
                  <Text
                    style={{
                      fontSize: 16,
                      fontWeight: "bold",
                      paddingVertical: 15,
                    }}>
                    Đổi thông tin cá nhân
                  </Text>
                </TouchableOpacity>
              </Body>
            </ListItem>
          </Content>
        </Container>
      </>
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    token: state.UserReducers.USER_TOKEN,
    userInfo: JSON.parse(state.UserReducers.USER_INFO),
  };
};

export default connect(mapStateToProps, {saveUserProfile})(ProfileScreen);
