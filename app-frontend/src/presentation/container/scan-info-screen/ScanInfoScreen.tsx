import React from "react";
import {
  Container,
  Header,
  Text,
  Left,
  Title,
  Button,
  Body,
  Right,
  Content,
  ListItem,
  View,
} from "native-base";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import Commons from "../../../assets/json/Commons.json";
import {Image} from "react-native";
import AwesomeLoading from "react-native-awesome-loading/index";

export default class ScanInfoScreen extends React.Component<any, any> {
  protected readonly PREFIX_URL: string = Commons.api.dev_uri;

  constructor(props: any) {
    super(props);
    this.state = {
      isLoadImageFull: false,
      isLoadImageAnswer: false,
    };
  }
  componentDidMount() {
    console.log(this.props.route.params.scanData);
  }

  render() {
    return (
      <>
        <AwesomeLoading
          indicatorId={8}
          size={50}
          isActive={this.state.isLoadImageFull || this.state.isLoadImageAnswer}
          text="loading"
        />
        <Container>
          <Header style={{borderBottomColor: "red", borderBottomWidth: 1}}>
            <Left>
              <Button
                transparent
                onPress={() => {
                  this.props.navigation.navigate("ScanResult");
                }}>
                <FontAwesome5Icon
                  style={{fontSize: 25, paddingLeft: 5, color: "red"}}
                  name="angle-left"
                />
              </Button>
            </Left>
            <Body>
              <Title>CHI TIẾT</Title>
            </Body>
            <Right />
          </Header>
          <Content>
            <ListItem>
              <Body>
                <Text style={{fontWeight: "bold"}}>Tổng Quan Phiếu Thi</Text>
                <Text style={{marginTop: 10}}>
                  {"Số Báo Danh: " +
                    this.props.route.params.scanData.studentCode}
                </Text>
                <Text style={{marginTop: 10}}>
                  {"Mã Đề Thi: " + this.props.route.params.scanData.testCode}
                </Text>
                <View
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignContent: "center",
                  }}>
                  <Image
                    style={{
                      marginTop: 20,
                      height: 500,
                      width: "auto",
                    }}
                    resizeMode={"contain"}
                    source={{
                      uri:
                        this.PREFIX_URL +
                        "/scan-result/" +
                        this.props.route.params.scanData.imageFullFileName,
                    }}
                    onLoadStart={() => {
                      this.setState((preState: any) => ({
                        ...preState,
                        isLoadImageFull: true,
                      }));
                    }}
                    onLoadEnd={() => {
                      this.setState((preState: any) => ({
                        ...preState,
                        isLoadImageFull: false,
                      }));
                    }}
                  />
                </View>
              </Body>
            </ListItem>
            <ListItem>
              <Body>
                <Text style={{fontWeight: "bold"}}>Điểm Thi</Text>
                <Text style={{marginTop: 10}}>
                  {this.props.route.params.scanData.score +
                    " - " +
                    this.props.route.params.scanData.numberQuestionCorrect +
                    " câu đúng"}
                </Text>
                <View
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignContent: "center",
                  }}>
                  <Image
                    style={{
                      height: 250,
                      width: "auto",
                    }}
                    resizeMode={"contain"}
                    source={{
                      uri:
                        this.PREFIX_URL +
                        "/scan-result/" +
                        this.props.route.params.scanData.imageAnswerFileName,
                    }}
                    onLoadStart={() => {
                      this.setState((preState: any) => ({
                        ...preState,
                        isLoadImageAnswer: true,
                      }));
                    }}
                    onLoadEnd={() => {
                      this.setState((preState: any) => ({
                        ...preState,
                        isLoadImageAnswer: false,
                      }));
                    }}
                  />
                </View>
              </Body>
            </ListItem>
          </Content>
        </Container>
      </>
    );
  }
}
