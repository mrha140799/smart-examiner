import {StyleSheet} from "react-native";

export default class ScanScreenStyles {
  public static styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#4e89ae",
    },
    capture: {
      flex: 0,
      backgroundColor: "#4e89ae",
      borderRadius: 6,
      padding: 15,
      paddingHorizontal: 10,
      alignSelf: "center",
      margin: 10,
    },
  });
}
