import React from "react";
import {Alert, TouchableOpacity, View} from "react-native";
import ScanScreenStyles from "./ScanScreenStyles";
import {RNCamera, TakePictureResponse} from "react-native-camera";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import ScanInteractor, {
  ScanOutputPort,
} from "../../../domain/usecase/ScanInteractor";
import AwesomeLoading from "react-native-awesome-loading/index";

class ScanScreen extends React.Component<any, any> implements ScanOutputPort {
  private camera: any;
  constructor(props: any) {
    super(props);
    this.camera = null;
    this.state = {
      captureImage: null,
      iscCallingApi: false,
    };
  }
  private styles = ScanScreenStyles.styles;
  private scanUC: ScanInteractor = ScanInteractor.create(this);
  saveFileError(error: any): void {
    this.setState((preState: any) => ({
      ...preState,
      isCallingApi: false,
    }));
    Alert.alert(error);
  }

  saveFileSuccess(result: any): void {
    this.setState((preState: any) => ({
      ...preState,
      isCallingApi: false,
    }));
    this.props.navigation.navigate("ScanResult", {scanData: result});
  }
  onSaveFilePressed = (fileContent: string) => {
    this.scanUC.saveFile(fileContent);
  };
  render() {
    return (
      <View style={this.styles.container}>
        <AwesomeLoading
          indicatorId={8}
          size={50}
          isActive={this.state.isCallingApi}
          text="loading"
        />
        <View
          style={{
            paddingTop: 35,
            flexDirection: "row",
            justifyContent: "flex-start",
          }}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <FontAwesome5Icon
              name="chevron-left"
              style={{
                fontSize: 30,
                paddingLeft: 10,
                paddingBottom: 10,
                color: "#fefefe",
              }}
            />
          </TouchableOpacity>
        </View>
        <FontAwesome5Icon
          name="square"
          style={{
            position: "absolute",
            fontSize: 35,
            top: 75,
            left: 1,
            color: "#61b15a",
            zIndex: 40,
          }}
        />
        <FontAwesome5Icon
          name="square"
          style={{
            position: "absolute",
            fontSize: 35,
            top: 75,
            right: 1,
            color: "#61b15a",
            zIndex: 40,
          }}
        />
        <FontAwesome5Icon
          name="square"
          style={{
            position: "absolute",
            fontSize: 35,
            bottom: 85,
            right: 1,
            color: "#61b15a",
            zIndex: 40,
          }}
        />

        <FontAwesome5Icon
          name="square"
          style={{
            position: "absolute",
            fontSize: 35,
            bottom: 85,
            left: 1,
            color: "#61b15a",
            zIndex: 40,
          }}
        />
        <RNCamera
          style={{flex: 1}}
          ref={(camera) => {
            this.camera = camera;
          }}
        />
        <View
          style={{
            flex: 0,
            flexDirection: "row",
            justifyContent: "center",
          }}>
          <TouchableOpacity onPress={this.takePicture.bind(this)}>
            <FontAwesome5Icon
              name="camera"
              style={{fontSize: 40, padding: 10, color: "#fefefe"}}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  takePicture = async () => {
    if (this.camera) {
      const options = {
        quality: 0.5,
        base64: true,
        fixOrientation: true,
        pictureOrientation: "portrait",
        deviceOrientation: "portrait",
      };
      this.camera
        .takePictureAsync(options)
        .then((data: TakePictureResponse) => {
          this.setState((preState: any) => ({
            ...preState,
            captureImage: data.base64,
            isCallingApi: true,
          }));
          this.onSaveFilePressed(data.base64 as string);
        });
    }
  };
}

export default ScanScreen;
