import {StyleSheet} from "react-native";

export default class AboutScreenStyles {
  public static styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "#dddddd",
    },
  });
}
