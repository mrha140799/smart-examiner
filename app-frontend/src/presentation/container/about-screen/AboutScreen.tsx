import React from "react";
import {Image, Text, View} from "react-native";
import AboutScreenStyles from "./AboutScreenStyles";
import AwesomeLoading from "react-native-awesome-loading/index";
import {Body, Container, Content, Header, Title} from "native-base";

export default class AboutScreen extends React.Component {
  private styles = AboutScreenStyles.styles;
  private readonly WEBSITE_URL: string = "http://localhost:8082.";

  constructor(props: any) {
    super(props);
    this.state = {
      isCallingApi: false,
      isLoadImageFull: false,
    };
  }

  render() {
    return (
      <>
        <AwesomeLoading
          indicatorId={8}
          size={50}
          isActive={this.state.isLoadImageFull}
          text="loading"
        />
        <Container>
          <Header style={{borderBottomColor: "red", borderBottomWidth: 1}}>
            <Body>
              <Title>HƯỚNG DẪN SỬ DỤNG</Title>
            </Body>
          </Header>
          <Content>
            <View style={{paddingHorizontal: 15, paddingTop: 10}}>
              <Text
                style={{
                  fontSize: 16,
                  textAlign: "center",
                  fontWeight: "bold",
                  fontStyle: "italic",
                }}>
                SMART EXAMINER
              </Text>
              <Text style={{fontSize: 16, paddingTop: 20}}>
                {"   "}Khi sử dụng hệ thống của chúng tôi, người dùng có thể sử
                dụng tất cả các tiện ích mà phần mềm hỗ trợ như: Chấm điểm thi,
                quản lý học viên, xem kết quả điểm thi ...
              </Text>
              <View style={{paddingTop: 20}}>
                <Text style={{fontSize: 16, fontWeight: "bold"}}>
                  I. TÔNG QUAN VỀ HỆ THỐNG
                </Text>
              </View>
              <View style={{flexDirection: "row", paddingTop: 20}}>
                <Text style={{fontSize: 16, fontWeight: "bold"}}>
                  1. Website quản trị:{" "}
                </Text>
                <Text style={{fontSize: 16, fontStyle: "italic"}}>
                  {this.WEBSITE_URL}
                </Text>
              </View>
              <View style={{paddingTop: 10}}>
                <Text style={{fontSize: 16}}>
                  {"  "} Website quản trị có thể cho phép giáo viên: Thêm, sửa,
                  xóa một vài thông tin mà hệ thống yêu cầu, cho phép giáo viên
                  có thể tạo đề thi, nhiều sinh viên bằng excel ...
                </Text>
              </View>
              <Image
                style={{
                  height: 250,
                  width: "auto",
                }}
                resizeMode={"contain"}
                source={require("../../../assets/image/website_overview.png")}
                onLoadStart={() => {
                  this.setState((preState: any) => ({
                    ...preState,
                    isLoadImageFull: true,
                  }));
                }}
                onLoadEnd={() => {
                  this.setState((preState: any) => ({
                    ...preState,
                    isLoadImageFull: false,
                  }));
                }}
              />
              <View style={{flexDirection: "row", paddingTop: 20}}>
                <Text style={{fontSize: 16, fontWeight: "bold"}}>
                  2. Ứng dụng:{" "}
                </Text>
                <Text style={{fontSize: 16, fontStyle: "italic"}}>
                  Smart Examiner
                </Text>
              </View>
              <View style={{paddingTop: 10}}>
                <Text style={{fontSize: 16}}>
                  {"  "} Ứng dụng cho phép giáo viên: Thêm, sửa, xóa một thông
                  tin bài kiểm tra, đăng kí số báo danh cho thí sinh, chấm điểm
                  trắc nghiệm, xem lịch sử chấm. Một số tính năng đăng phát
                  triển như: Thống kê số câu sai của từng bài kiểm tra, tổng kết
                  điểm cho thí sinh.
                </Text>
                <View style={{flexDirection: "row"}}>
                  <Text
                    style={{fontStyle: "italic", paddingTop: 10, fontSize: 16}}>
                    LƯU Ý RẰNG: Lịch chấm điểm và ảnh chi tiết phiếu trả lời sau
                    khi chấm, hệ thống sẽ tự động xóa sau 7 ngày
                  </Text>
                </View>
              </View>
              <Image
                style={{
                  marginTop: 10,
                  height: 500,
                  width: "auto",
                }}
                resizeMode={"contain"}
                source={require("../../../assets/image/mobile_overview.jpeg")}
                onLoadStart={() => {
                  this.setState((preState: any) => ({
                    ...preState,
                    isLoadImageFull: true,
                  }));
                }}
                onLoadEnd={() => {
                  this.setState((preState: any) => ({
                    ...preState,
                    isLoadImageFull: false,
                  }));
                }}
              />
              <View style={{paddingTop: 20}}>
                <Text style={{fontSize: 16, fontWeight: "bold"}}>
                  II. HƯỚNG DẪN SỬ DỤNG
                </Text>
              </View>
              <View style={{paddingTop: 10}}>
                <Text style={{fontSize: 16}}>
                  {"  "} Để có thể chấm điểm cho thí sinh: hệ thống cần phải
                  được cung cấp thông tin về thí sinh nhằm mục địch quản lý.
                </Text>
              </View>
              <View style={{flexDirection: "row", paddingTop: 20}}>
                <Text style={{fontSize: 16, fontWeight: "bold"}}>
                  - Bước 1:{" "}
                </Text>
                <Text style={{fontSize: 16, fontStyle: "italic"}}>
                  Thêm trường học.
                </Text>
              </View>
              <View style={{paddingTop: 10}}>
                <Text style={{fontSize: 16}}>
                  {"  "} Người dùng muốn thông tin trường học, người dùng sẽ
                  phải truy cập vào website quản trị: "{this.WEBSITE_URL}"
                </Text>
              </View>
              <View style={{flexDirection: "row"}}>
                <Text
                  style={{fontStyle: "italic", paddingTop: 10, fontSize: 16}}>
                  LƯU Ý RẰNG: Để dễ dàng cho việc quản trị, mỗi người dùng chỉ
                  có thể tạo một trường học
                </Text>
              </View>
              <Image
                style={{
                  height: 150,
                  width: "auto",
                }}
                resizeMode={"contain"}
                source={require("../../../assets/image/create_school.png")}
                onLoadStart={() => {
                  this.setState((preState: any) => ({
                    ...preState,
                    isLoadImageFull: true,
                  }));
                }}
                onLoadEnd={() => {
                  this.setState((preState: any) => ({
                    ...preState,
                    isLoadImageFull: false,
                  }));
                }}
              />
              <View style={{flexDirection: "row", paddingTop: 20}}>
                <Text style={{fontSize: 16, fontWeight: "bold"}}>
                  - Bước 2:{" "}
                </Text>
                <Text style={{fontSize: 16, fontStyle: "italic"}}>
                  Thêm lớp học.
                </Text>
              </View>
              <Image
                style={{
                  height: 150,
                  width: "auto",
                }}
                resizeMode={"contain"}
                source={require("../../../assets/image/create_classroom.png")}
                onLoadStart={() => {
                  this.setState((preState: any) => ({
                    ...preState,
                    isLoadImageFull: true,
                  }));
                }}
                onLoadEnd={() => {
                  this.setState((preState: any) => ({
                    ...preState,
                    isLoadImageFull: false,
                  }));
                }}
              />
            </View>
            <View style={{flexDirection: "row", paddingTop: 20}}>
              <Text style={{fontSize: 16, fontWeight: "bold"}}>- Bước 3: </Text>
              <Text style={{fontSize: 16, fontStyle: "italic"}}>
                Thêm học sinh.
              </Text>
            </View>
          </Content>
        </Container>
      </>
    );
  }
}
