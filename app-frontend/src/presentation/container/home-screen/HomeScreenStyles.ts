import {StyleSheet} from "react-native";

export default class HomeScreenStyles {
  public static styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#dddddd",
    },
  });
}
