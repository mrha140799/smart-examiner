import React from "react";
import {Image, Text, TouchableOpacity, View} from "react-native";
import HomeScreenStyles from "./HomeScreenStyles";
import {Icon} from "native-base";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import {DrawerActions} from "@react-navigation/native";
import {connect} from "react-redux";
import MasterData from "../../../assets/json/Commons.json";
import AwesomeLoading from "react-native-awesome-loading/index";

class HomeScreen extends React.Component<any, any> {
  private styles = HomeScreenStyles.styles;
  private readonly IMAGE_URI: string = MasterData["image-uri"];
  constructor(props: any) {
    super(props);
    this.state = {
      isLoadImageFull: false,
    };
  }
  render() {
    return (
      <>
        <View style={this.styles.container}>
          <AwesomeLoading
            indicatorId={8}
            size={50}
            isActive={this.state.isLoadImageFull}
            text="loading"
          />
          <View
            style={{
              backgroundColor: "#4e89ae",
              height: "28%",
              borderBottomLeftRadius: 20,
              borderBottomRightRadius: 20,
              paddingHorizontal: 20,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.dispatch(DrawerActions.openDrawer());
              }}>
              <Icon
                name="menu"
                style={{fontSize: 20, color: "white", marginTop: 40}}
              />
            </TouchableOpacity>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginTop: 25,
                width: "100%",
              }}>
              <View style={{width: "70%"}}>
                <Text
                  style={{color: "white", fontSize: 25, fontWeight: "bold"}}>
                  Xin chào{" "}
                  {this.props.userInfo && this.props.userInfo.firstName
                    ? this.props.userInfo.firstName
                    : "Ẩn danh"}
                  !
                </Text>
              </View>
              <View style={{width: "30%", alignItems: "flex-end"}}>
                <Image
                  source={
                    this.props.userInfo && this.props.userInfo.imageAvatarName
                      ? {
                          uri:
                            this.IMAGE_URI +
                            this.props.userInfo.imageAvatarName,
                        }
                      : require("../../../assets/image/default_user.png")
                  }
                  style={{width: 60, height: 60, borderRadius: 60}}
                  onLoadStart={() => {
                    this.setState((preState: any) => ({
                      ...preState,
                      isLoadImageFull: true,
                    }));
                  }}
                  onLoadEnd={() => {
                    this.setState((preState: any) => ({
                      ...preState,
                      isLoadImageFull: false,
                    }));
                  }}
                />
              </View>
            </View>
          </View>
          <View
            style={{
              height: "71%",
              justifyContent: "center",
              alignItems: "center",
            }}>
            <View
              style={{
                width: "95%",
                height: "90%",
              }}>
              <View
                style={{
                  width: "100%",
                  height: "50%",
                  flexDirection: "row",
                }}>
                <View
                  style={{
                    width: "47%",
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: "#FEFEFE",
                    borderRadius: 15,
                    marginHorizontal: 10,
                    marginVertical: 5,
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate("Scan");
                    }}>
                    <FontAwesome5Icon
                      name="qrcode"
                      style={{fontSize: 120, color: "#ec4646"}}
                    />
                    <Text
                      style={{
                        marginTop: 25,
                        fontSize: 15,
                        fontWeight: "bold",
                        textAlign: "center",
                      }}>
                      Chấm điểm
                    </Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    width: "47%",
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: "#FEFEFE",
                    borderRadius: 15,
                    marginVertical: 5,
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate("ListTest");
                    }}>
                    <FontAwesome5Icon
                      name="poll-h"
                      style={{fontSize: 120, color: "#ec4646"}}
                    />
                    <Text
                      style={{
                        marginTop: 25,
                        fontSize: 15,
                        fontWeight: "bold",
                        textAlign: "center",
                      }}>
                      Đề Thi
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View
                style={{width: "100%", height: "50%", flexDirection: "row"}}>
                <View
                  style={{
                    width: "47%",
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: "#FEFEFE",
                    borderRadius: 15,
                    marginHorizontal: 10,
                    marginVertical: 5,
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate("ListStudentCode");
                    }}>
                    <FontAwesome5Icon
                      name="user-graduate"
                      style={{fontSize: 120, color: "#ec4646"}}
                    />
                    <Text
                      style={{
                        marginTop: 25,
                        fontSize: 15,
                        fontWeight: "bold",
                        textAlign: "center",
                      }}>
                      Số báo danh
                    </Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    width: "47%",
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: "#FEFEFE",
                    borderRadius: 15,
                    marginVertical: 5,
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate("ScanHistory");
                    }}>
                    <FontAwesome5Icon
                      name="history"
                      style={{fontSize: 120, color: "#ec4646"}}
                    />
                    <View>
                      <Text
                        style={{
                          marginTop: 25,
                          fontSize: 15,
                          fontWeight: "bold",
                        }}>
                        Lịch sử chấm thi
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </View>
      </>
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    userInfo: JSON.parse(state.UserReducers.USER_INFO),
  };
};
export default connect(mapStateToProps)(HomeScreen);
