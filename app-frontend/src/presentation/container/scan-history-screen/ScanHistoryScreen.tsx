import React from "react";
import {
  Body,
  Button,
  Container,
  Content,
  Header,
  Left,
  ListItem,
  Right,
  Text,
  Title,
  View,
} from "native-base";
import AwesomeLoading from "react-native-awesome-loading/index";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import Moment from "moment";
import {Alert, SafeAreaView, TouchableOpacity} from "react-native";
import {SwipeListView} from "react-native-swipe-list-view";
import ScanHistoryInteractor, {
  IScanHistoryOutputPort,
} from "../../../domain/usecase/ScanHistoryInteractor";
import ScanResult from "../../../data/model/ScanResult";

class ScanHistoryScreen
  extends React.Component<any, any>
  implements IScanHistoryOutputPort {
  private scanUC: ScanHistoryInteractor = ScanHistoryInteractor.create(this);
  private readonly DETAIL_TYPE: string = "detail";
  private readonly UPDATE_TYPE: string = "update";
  private readonly DELETE_TYPE: string = "delete";
  constructor(props: any) {
    super(props);
    this.state = {
      isCallingApi: false,
      isEmpty: true,
      scanResults: [new ScanResult()],
    };
  }

  myScanHistoriesSuccessfully(scans: ScanResult[]) {
    if (!scans.length) {
      this.setState((preState: any) => ({
        ...preState,
        isCallingApi: false,
        isEmpty: true,
      }));
    } else {
      this.setState((preState: any) => ({
        ...preState,
        scanResults: scans,
        isEmpty: false,
        isCallingApi: false,
      }));
    }
  }
  myScanHistoriesError() {
    this.setForState("isCallingApi", false);
    throw new Error("Method not implemented.");
  }

  private setForState(field: string, data: any) {
    this.setState((preState: any) => ({
      ...preState,
      [field]: data,
    }));
  }

  public onHandlePressed(type: string, scanResult: ScanResult) {
    if (type === this.DELETE_TYPE) {
      Alert.alert("Xóa bài thi", " Bạn có chắc muốn xóa bài thi này?", [
        {
          text: "Hủy",
          onPress: () => {},
          style: "cancel",
        },
        {
          text: "Đồng ý",
          onPress: () => {},
        },
      ]);
    } else if (type === this.UPDATE_TYPE) {
    } else if (type === this.DETAIL_TYPE) {
      this.props.navigation.navigate("ScanResult", {
        scanData: scanResult,
        screenType: "history",
      });
    }
  }

  callApiFinally(): any {}
  componentDidMount() {
    this.setForState("isCallingApi", true);
    this.scanUC.myScanHistories();
  }

  render() {
    return (
      <>
        <AwesomeLoading
          indicatorId={8}
          size={50}
          isActive={this.state.isCallingApi}
          text="loading"
        />
        <Container>
          <Header style={{borderBottomColor: "red", borderBottomWidth: 1}}>
            <Left>
              <Button
                transparent
                onPress={() => {
                  this.props.navigation.navigate("Index");
                }}>
                <FontAwesome5Icon
                  style={{fontSize: 25, paddingLeft: 5, color: "red"}}
                  name="angle-left"
                />
              </Button>
            </Left>
            <Body>
              <Title>LỊCH SỬ CHẤM</Title>
            </Body>
            <Right />
          </Header>
          {this.state.isEmpty && (
            <Content>
              <View
                style={{
                  paddingTop: "50%",
                  flexDirection: "row",
                  justifyContent: "center",
                  width: "100%",
                }}>
                <Text
                  style={{
                    fontStyle: "italic",
                    alignContent: "center",
                  }}>
                  Không Có Dữ Liệu
                </Text>
              </View>
            </Content>
          )}
          {!this.state.isEmpty && this.state.scanResults.length && (
            <SafeAreaView style={{flex: 1}}>
              <SwipeListView
                data={this.state.scanResults}
                renderItem={(data, rowMap) => (
                  <ListItem thumbnail style={{backgroundColor: "white"}}>
                    <Left style={{width: "5%"}}>
                      <Text>{data.index + 1}</Text>
                    </Left>
                    <Body>
                      <View style={{flexDirection: "row"}}>
                        <Text>Số báo danh: </Text>
                        <Text style={{fontStyle: "italic", fontWeight: "bold"}}>
                          {" "}
                          {(data.item as ScanResult).studentCode}
                        </Text>
                      </View>
                      <View style={{flexDirection: "row", paddingTop: 5}}>
                        <Text>Mã đề thi: </Text>
                        <Text style={{fontStyle: "italic", fontWeight: "bold"}}>
                          {(data.item as ScanResult).testCode}{" "}
                        </Text>
                        <Text> - Tên môn thi: </Text>
                        <Text style={{fontStyle: "italic", fontWeight: "bold"}}>
                          {" "}
                          {(data.item as ScanResult).subjectName}
                        </Text>
                      </View>
                      <View style={{flexDirection: "row", paddingTop: 5}}>
                        <Text>Thời Gian Chấm: </Text>
                        <Text style={{fontStyle: "italic", fontWeight: "bold"}}>
                          {" "}
                          {Moment((data.item as ScanResult).createdTime).format(
                            "DD/MM/YYYY HH:MM",
                          )}
                        </Text>
                      </View>
                    </Body>
                  </ListItem>
                )}
                renderHiddenItem={(data, rowMap) => (
                  <View
                    style={{
                      alignItems: "center",
                      flexDirection: "row",
                      justifyContent: "space-between",
                      paddingLeft: 15,
                      paddingRight: 15,
                      minHeight: "100%",
                    }}>
                    <View />
                    <View style={{flexDirection: "row"}}>
                      <TouchableOpacity
                        onPress={() =>
                          this.onHandlePressed(
                            this.DETAIL_TYPE,
                            data.item as ScanResult,
                          )
                        }>
                        <FontAwesome5Icon
                          style={{fontSize: 25, paddingRight: 15}}
                          name={"eye"}
                        />
                      </TouchableOpacity>
                      {/*<TouchableOpacity*/}
                      {/*  onPress={() =>*/}
                      {/*    this.onHandlePressed(*/}
                      {/*      this.UPDATE_TYPE,*/}
                      {/*      (data.item as Test).id,*/}
                      {/*    )*/}
                      {/*  }>*/}
                      {/*  <FontAwesome5Icon*/}
                      {/*    style={{fontSize: 25, paddingRight: 15}}*/}
                      {/*    name={"pencil-alt"}*/}
                      {/*  />*/}
                      {/*</TouchableOpacity>*/}
                      {/*<TouchableOpacity*/}
                      {/*  onPress={() =>*/}
                      {/*    this.onHandlePressed(*/}
                      {/*      this.DELETE_TYPE,*/}
                      {/*      (data.item as Test).id,*/}
                      {/*    )*/}
                      {/*  }>*/}
                      {/*  <FontAwesome5Icon*/}
                      {/*    style={{fontSize: 25}}*/}
                      {/*    name={"trash"}*/}
                      {/*  />*/}
                      {/*</TouchableOpacity>*/}
                    </View>
                  </View>
                )}
                rightOpenValue={-130}
                disableRightSwipe
                keyExtractor={(item, index) => index.toString()}
              />
            </SafeAreaView>
          )}
        </Container>
      </>
    );
  }
}
export default ScanHistoryScreen;
