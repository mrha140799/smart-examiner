import React from "react";
import RegisterScreenStyle from "./RegisterScreenStyle";
import {
  Alert,
  KeyboardAvoidingView,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import {
  Body,
  Container,
  Content,
  Footer,
  Header,
  Left,
  List,
  ListItem,
  Right,
  Text,
  Title,
} from "native-base";
import {RadioButton} from "react-native-paper";
import * as Animatable from "react-native-animatable";
import RegistrationInteractor, {
  IRegistrationOutputPort,
} from "../../../domain/usecase/RegistrationInteractor";
import AwesomeLoading from "react-native-awesome-loading/index";
import GestureRecognizer from "react-native-swipe-gestures";
import User from "../../../data/model/User";

class RegisterScreen
  extends React.Component<any, any>
  implements IRegistrationOutputPort {
  private registrationUC: RegistrationInteractor = RegistrationInteractor.create(
    this,
  );
  private styles = RegisterScreenStyle.style;
  constructor(props: any) {
    super(props);
    this.state = {
      step: 1,
      firstName: "",
      lastName: "",
      phoneNumber: "",
      email: "",
      gender: true,
      password: "",
      confirmPassword: "",
      isCallingApi: false,
      isValidLastName: false,
      isValidFirstName: false,
      isValidPhoneNumber: true,
      isValidEmail: true,
      isExistsEmail: false,
      isValidConfirmPassword: true,
      isValidPassword: true,
      isEditingStep3: false,
      isEditingStep4: false,
    };
  }

  isValidEmail() {
    this.setState((preState: any) => ({
      ...preState,
      isExistsEmail: false,
    }));
    this.setState((preState: any) => ({
      ...preState,
      isCallingApi: false,
    }));
  }
  invalidEmail() {
    this.setState((preState: any) => ({
      ...preState,
      isCallingApi: false,
    }));
    this.setState((preState: any) => ({
      ...preState,
      isExistsEmail: true,
    }));
  }
  public onInputChange = (field: string, value: string) => {
    this.setState((preState: any) => ({
      ...preState,
      [field]: value,
    }));
  };
  public setGender = (value: boolean | null) => {
    this.setState((preState: any) => ({
      ...preState,
      gender: value,
    }));
  };

  public onHandleChangeInput = (field: string, text: string) => {
    if (field === "phoneNumber" || field === "email") {
      this.setState((preState: any) => ({
        ...preState,
        isEditingStep3: true,
      }));
      if (field === "email" && this.state.isValidEmail) {
        this.setState((preState: any) => ({
          ...preState,
          isCallingApi: true,
        }));
        this.registrationUC.isValidEmail(text);
      }
    } else if (field === "confirmPassword" || field === "passed") {
      this.setState((preState: any) => ({
        ...preState,
        isEditingStep4: true,
      }));
    }
    if (field === "firstName") {
      this.setState((preState: any) => ({
        ...preState,
        isValidFirstName: text.length >= 2,
      }));
    } else if (field === "lastName") {
      this.setState((preState: any) => ({
        ...preState,
        isValidLastName: text.length >= 2,
      }));
    } else if (field === "password") {
      this.setState((preState: any) => ({
        ...preState,
        isValidPassword: text.length >= 6,
      }));
    } else if (field === "confirmPassword") {
      const isValid = this.state.confirmPassword === this.state.password;
      this.setState((preState: any) => ({
        ...preState,
        isValidConfirmPassword: isValid,
      }));
    } else if (field === "phoneNumber") {
      const regex = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g;
      this.setState((preState: any) => ({
        ...preState,
        isValidPhoneNumber: regex.test(text),
      }));
    } else if (field === "email") {
      const regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      this.setState((preState: any) => ({
        ...preState,
        isValidEmail: regExp.test(text),
      }));
    }
  };

  public backStep = () => {
    const currentStep = this.state.step;
    this.setState((preSate: any) => ({
      ...preSate,
      step: currentStep - 1,
    }));
  };

  public registerUser() {
    this.setState((preState: any) => ({
      ...preState,
      isCallingApi: true,
    }));
    const user: User = new User();
    user.firstName = this.state.firstName;
    user.lastName = this.state.lastName;
    user.gender = this.state.gender;
    user.phoneNumber = this.state.phoneNumber;
    user.email = this.state.email;
    user.password = this.state.password;
    this.registrationUC.register(user);
  }
  render() {
    return (
      <KeyboardAvoidingView behavior={"padding"} style={this.styles.container}>
        <AwesomeLoading
          indicatorId={8}
          size={50}
          isActive={this.state.isCallingApi}
          text="loading"
        />
        <Container>
          <Header style={{borderBottomColor: "red", borderBottomWidth: 1}}>
            <Left />
            <Body>
              <Title>ĐĂNG KÝ</Title>
            </Body>
            <Right />
          </Header>
          <Content>
            {this.state.step === 1 && (
              <Animatable.View
                style={{paddingTop: "30%"}}
                animation={"fadeInLeft"}
                duration={500}>
                <View style={{flexDirection: "row", justifyContent: "center"}}>
                  <Text
                    style={{
                      fontSize: 25,
                      color: "#1877f2",
                      alignContent: "center",
                    }}>
                    Bạn tên gì?
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    width: "100%",
                    marginTop: 20,
                  }}>
                  <TextInput
                    style={{
                      height: 50,
                      width: "40%",
                      borderColor: "rgba(0,0,0,0.2)",
                      borderRadius: 10,
                      borderWidth: 0.5,
                      marginHorizontal: 20,
                      paddingLeft: 10,
                      marginVertical: 5,
                    }}
                    placeholder="Họ"
                    placeholderTextColor="gray"
                    onChangeText={(e) => this.onInputChange("lastName", e)}
                    onEndEditing={(e) =>
                      this.onHandleChangeInput("lastName", e.nativeEvent.text)
                    }
                    value={this.state.lastName}
                  />
                  <TextInput
                    style={{
                      height: 50,
                      width: "40%",
                      borderColor: "rgba(0,0,0,0.2)",
                      borderRadius: 10,
                      borderWidth: 0.5,
                      marginHorizontal: 20,
                      paddingLeft: 10,
                      marginVertical: 5,
                    }}
                    placeholder="Tên"
                    placeholderTextColor="gray"
                    onChangeText={(e) => this.onInputChange("firstName", e)}
                    onEndEditing={(e) =>
                      this.onHandleChangeInput("firstName", e.nativeEvent.text)
                    }
                    value={this.state.firstName}
                  />
                </View>
                {(!this.state.isValidFirstName ||
                  !this.state.isValidLastName) && (
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "center",
                      marginTop: 10,
                    }}>
                    <Text style={{fontSize: 14, alignContent: "center"}}>
                      Dùng tên thật của bạn để dễ dàng trao đổi hơn
                    </Text>
                  </View>
                )}

                <View
                  style={{
                    flexDirection: "row",
                    width: "100%",
                    marginTop: 20,
                    justifyContent: "center",
                  }}>
                  {this.state.isValidFirstName && this.state.isValidLastName && (
                    <TouchableOpacity
                      onPress={() => {
                        if (
                          this.state.isValidFirstName &&
                          this.state.isValidLastName
                        ) {
                          const currentStep = this.state.step;
                          this.setState((preState: any) => ({
                            ...preState,
                            step: currentStep + 1,
                          }));
                        }
                      }}>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "center",
                          alignItems: "center",
                          width: "100%",
                          backgroundColor: "#45a5ca",
                          height: 30,
                          borderRadius: 5,
                        }}>
                        <Text
                          style={{
                            color: "white",
                          }}>
                          Tiếp
                        </Text>
                      </View>
                    </TouchableOpacity>
                  )}
                </View>
              </Animatable.View>
            )}
            {this.state.step === 2 && (
              <GestureRecognizer
                style={{flex: 1}}
                onSwipeRight={() => this.backStep()}>
                <Animatable.View
                  style={{paddingTop: "30%"}}
                  animation={"fadeInLeft"}
                  duration={500}>
                  <View style={{display: "flex", justifyContent: "center"}}>
                    <View
                      style={{flexDirection: "row", justifyContent: "center"}}>
                      <Text
                        style={{
                          fontSize: 20,
                          color: "#1877f2",
                          alignContent: "center",
                        }}>
                        Giới tính của bạn là gì?
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "center",
                        paddingTop: 15,
                        paddingBottom: 15,
                      }}>
                      <Text style={{fontSize: 14, alignContent: "center"}}>
                        Về sau, bạn có thể cập nhật lại trong trang thông tin
                        của bạn
                      </Text>
                    </View>
                  </View>
                  <List>
                    <ListItem onPress={() => this.setGender(true)}>
                      <Body
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between",
                        }}>
                        <Text style={{fontSize: 16, color: "#1877f2"}}>
                          Nam
                        </Text>
                        <RadioButton
                          value="Second"
                          status={this.state.gender ? "checked" : "unchecked"}
                          onPress={() => this.setGender(true)}
                        />
                      </Body>
                    </ListItem>
                    <ListItem onPress={() => this.setGender(false)}>
                      <Body
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between",
                        }}>
                        <Text style={{fontSize: 16, color: "#1877f2"}}>Nữ</Text>
                        <RadioButton
                          value="Second"
                          status={
                            this.state.gender === false
                              ? "checked"
                              : "unchecked"
                          }
                          onPress={() => this.setGender(false)}
                        />
                      </Body>
                    </ListItem>
                    <ListItem onPress={() => this.setGender(null)}>
                      <Body
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between",
                        }}>
                        <Text style={{fontSize: 16, color: "#1877f2"}}>
                          Khác
                        </Text>
                        <RadioButton
                          value="third"
                          status={
                            this.state.gender === null ? "checked" : "unchecked"
                          }
                          onPress={() => this.setGender(null)}
                        />
                      </Body>
                    </ListItem>
                  </List>
                  <View
                    style={{
                      flexDirection: "row",
                      width: "100%",
                      marginTop: 20,
                      justifyContent: "center",
                    }}>
                    <TouchableOpacity
                      onPress={() => {
                        const currentStep = this.state.step;
                        this.setState((preState: any) => ({
                          ...preState,
                          step: currentStep + 1,
                        }));
                      }}>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "center",
                          alignItems: "center",
                          width: "100%",
                          backgroundColor: "#45a5ca",
                          height: 30,
                          borderRadius: 5,
                        }}>
                        <Text
                          style={{
                            color: "white",
                          }}>
                          Tiếp
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </Animatable.View>
              </GestureRecognizer>
            )}
            {this.state.step === 3 && (
              <GestureRecognizer
                style={{flex: 1}}
                onSwipeRight={() => this.backStep()}>
                <Animatable.View
                  style={{paddingTop: "25%"}}
                  animation={"fadeInLeft"}
                  duration={500}>
                  <View style={{display: "flex", justifyContent: "center"}}>
                    <View
                      style={{flexDirection: "row", justifyContent: "center"}}>
                      <Text
                        style={{
                          fontSize: 20,
                          color: "#1877f2",
                          alignContent: "center",
                        }}>
                        Chúng tôi có thể liên hệ với bạn qua?
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      width: "100%",
                      marginTop: 20,
                    }}>
                    <Text style={{marginHorizontal: 20, marginBottom: 15}}>
                      Số Điện Thoại:{" "}
                    </Text>
                    <TextInput
                      style={{
                        height: 50,
                        width: "90%",
                        borderColor: "rgba(0,0,0,0.2)",
                        borderRadius: 10,
                        borderWidth: 0.5,
                        marginHorizontal: 20,
                        paddingLeft: 10,
                        marginVertical: 5,
                      }}
                      keyboardType={"numeric"}
                      placeholder="Số điện thoại"
                      placeholderTextColor="gray"
                      onChangeText={(e) => this.onInputChange("phoneNumber", e)}
                      onEndEditing={(e) =>
                        this.onHandleChangeInput(
                          "phoneNumber",
                          e.nativeEvent.text,
                        )
                      }
                      value={this.state.phoneNumber}
                    />
                    {!this.state.isValidPhoneNumber && (
                      <Animatable.View animation="fadeInLeft" duration={500}>
                        <Text
                          style={{
                            fontSize: 14,
                            alignContent: "center",
                            marginHorizontal: 20,
                            fontStyle: "italic",
                            color: "red",
                          }}>
                          * Số Điện Thoại Không Hợp Lệ
                        </Text>
                      </Animatable.View>
                    )}
                  </View>
                  <View
                    style={{
                      width: "100%",
                      marginTop: 20,
                    }}>
                    <Text style={{marginHorizontal: 20, marginBottom: 15}}>
                      Email:{" "}
                    </Text>
                    <TextInput
                      style={{
                        height: 50,
                        width: "90%",
                        borderColor: "rgba(0,0,0,0.2)",
                        borderRadius: 10,
                        borderWidth: 0.5,
                        marginHorizontal: 20,
                        paddingLeft: 10,
                        marginVertical: 5,
                      }}
                      placeholder="Email"
                      placeholderTextColor="gray"
                      onChangeText={(e) => this.onInputChange("email", e)}
                      onEndEditing={(e) =>
                        this.onHandleChangeInput("email", e.nativeEvent.text)
                      }
                      value={this.state.email}
                    />
                    {!this.state.isValidEmail && (
                      <Animatable.View animation="fadeInLeft" duration={500}>
                        <Text
                          style={{
                            fontSize: 14,
                            alignContent: "center",
                            marginHorizontal: 20,
                            fontStyle: "italic",
                            color: "red",
                          }}>
                          * Email Không Hợp Lệ
                        </Text>
                      </Animatable.View>
                    )}
                    {this.state.isExistsEmail && (
                      <Animatable.View animation="fadeInLeft" duration={500}>
                        <Text
                          style={{
                            fontSize: 14,
                            alignContent: "center",
                            marginHorizontal: 20,
                            fontStyle: "italic",
                            color: "red",
                          }}>
                          * Email Đã tồn tại
                        </Text>
                      </Animatable.View>
                    )}
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "center",
                      paddingTop: 15,
                      paddingBottom: 15,
                    }}>
                    <Text style={{fontSize: 14, alignContent: "center"}}>
                      Địa chỉ email sẽ được dùng làm tên đăng nhập của bạn
                    </Text>
                  </View>
                  {this.state.isEditingStep3 &&
                    !this.state.isExistsEmail &&
                    this.state.isValidEmail &&
                    this.state.isValidPhoneNumber && (
                      <View
                        style={{
                          flexDirection: "row",
                          width: "100%",
                          marginTop: 20,
                          justifyContent: "center",
                        }}>
                        <TouchableOpacity
                          onPress={() => {
                            const currentStep = this.state.step;
                            this.setState((preState: any) => ({
                              ...preState,
                              step: currentStep + 1,
                            }));
                          }}>
                          <View
                            style={{
                              flexDirection: "row",
                              justifyContent: "center",
                              alignItems: "center",
                              width: "100%",
                              backgroundColor: "#45a5ca",
                              height: 30,
                              borderRadius: 5,
                            }}>
                            <Text
                              style={{
                                color: "white",
                              }}>
                              Tiếp Tục
                            </Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                    )}
                </Animatable.View>
              </GestureRecognizer>
            )}
            {this.state.step === 4 && (
              <View style={{paddingTop: "25%"}}>
                <View style={{display: "flex", justifyContent: "center"}}>
                  <View
                    style={{flexDirection: "row", justifyContent: "center"}}>
                    <Text
                      style={{
                        fontSize: 20,
                        color: "#1877f2",
                        alignContent: "center",
                      }}>
                      Bảo mật tài khoản của bạn
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    width: "100%",
                    marginTop: 20,
                  }}>
                  <Text style={{marginHorizontal: 20, marginBottom: 15}}>
                    Mật Khẩu:{" "}
                  </Text>
                  <TextInput
                    style={{
                      height: 50,
                      width: "90%",
                      borderColor: "rgba(0,0,0,0.2)",
                      borderRadius: 10,
                      borderWidth: 0.5,
                      marginHorizontal: 20,
                      paddingLeft: 10,
                      marginVertical: 5,
                    }}
                    placeholder="Mật khẩu"
                    placeholderTextColor="gray"
                    secureTextEntry={true}
                    onChangeText={(e) => this.onInputChange("password", e)}
                    onEndEditing={(e) =>
                      this.onHandleChangeInput("password", e.nativeEvent.text)
                    }
                    value={this.state.password}
                  />
                  {!this.state.isValidPassword && (
                    <Animatable.View animation="fadeInLeft" duration={500}>
                      <Text
                        style={{
                          fontSize: 14,
                          alignContent: "center",
                          marginHorizontal: 20,
                          fontStyle: "italic",
                          color: "red",
                        }}>
                        * Mật khẩu phải chứa ít nhất 6 kí tự
                      </Text>
                    </Animatable.View>
                  )}
                </View>
                <View
                  style={{
                    width: "100%",
                    marginTop: 20,
                  }}>
                  <Text style={{marginHorizontal: 20, marginBottom: 15}}>
                    Nhắc Lại Mật Khẩu:{" "}
                  </Text>
                  <TextInput
                    style={{
                      height: 50,
                      width: "90%",
                      borderColor: "rgba(0,0,0,0.2)",
                      borderRadius: 10,
                      borderWidth: 0.5,
                      marginHorizontal: 20,
                      paddingLeft: 10,
                      marginVertical: 5,
                    }}
                    placeholder="Nhắc lại mật khẩu"
                    placeholderTextColor="gray"
                    secureTextEntry={true}
                    onChangeText={(e) =>
                      this.onInputChange("confirmPassword", e)
                    }
                    onEndEditing={(e) =>
                      this.onHandleChangeInput(
                        "confirmPassword",
                        e.nativeEvent.text,
                      )
                    }
                    value={this.state.confirmPassword}
                  />
                  {!this.state.isValidConfirmPassword && (
                    <Animatable.View animation="fadeInLeft" duration={500}>
                      <Text
                        style={{
                          fontSize: 14,
                          alignContent: "center",
                          marginHorizontal: 20,
                          fontStyle: "italic",
                          color: "red",
                        }}>
                        * Mật khẩu Không trùng khớp
                      </Text>
                    </Animatable.View>
                  )}
                </View>
                {this.state.isEditingStep4 &&
                  this.state.isValidPassword &&
                  this.state.isValidConfirmPassword && (
                    <View
                      style={{
                        flexDirection: "row",
                        width: "100%",
                        marginTop: 20,
                        justifyContent: "center",
                      }}>
                      <TouchableOpacity onPress={() => this.registerUser()}>
                        <View
                          style={{
                            flexDirection: "row",
                            justifyContent: "center",
                            alignItems: "center",
                            width: "100%",
                            backgroundColor: "#45a5ca",
                            height: 30,
                            borderRadius: 5,
                          }}>
                          <Text
                            style={{
                              color: "white",
                            }}>
                            Hoàn tất
                          </Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  )}
              </View>
            )}
          </Content>
          <Footer>
            <Left />
            <Body>
              <TouchableOpacity
                onPress={() => {
                  Alert.alert("Trở lại", "Bạn có muốn hủy đăng ký không?", [
                    {
                      text: "Hủy",
                      onPress: () => {
                        this.props.navigation.navigate("Login");
                      },
                      style: "cancel",
                    },
                    {
                      text: "Tiếp Tục",
                      onPress: () => {},
                    },
                  ]);
                }}>
                <Text style={{fontSize: 14, color: "#1877f2"}}>
                  Bạn Đã Có Tài Khoản
                </Text>
              </TouchableOpacity>
            </Body>
            <Right />
          </Footer>
        </Container>
      </KeyboardAvoidingView>
    );
  }

  registerError(): any {
    this.setState((preState: any) => ({
      ...preState,
      isCallingApi: false,
    }));
    Alert.alert("Lỗi, đăng kí không thành công!");
  }

  registerSuccessfully(): any {
    this.setState((preState: any) => ({
      ...preState,
      isCallingApi: false,
    }));
    Alert.alert("Đăng kí thành công!");
    this.props.navigation.navigate("Login");
  }
}

export default RegisterScreen;
