import {StyleSheet} from "react-native";

export default class RegisterScreenStyle {
  public static style = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#dddddd",
    },
  });
}
