import {StyleSheet} from "react-native";

export default class LoginScreenStyle {
  public static style = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#fff",
      justifyContent: "flex-end",
      position: "relative",
      padding: 0,
    },
    imageBackground: {
      position: "absolute",
      zIndex: 1,
    },
    formSignIn: {
      flex: 1,
      backgroundColor: "white",
      ...(StyleSheet.absoluteFill as any),
      top: null,
    },
    buttonLogin: {
      backgroundColor: "white",
      height: 70,
      marginHorizontal: 20,
      borderRadius: 35,
      alignItems: "center",
      justifyContent: "center",
      marginVertical: 5,
    },
    textInputLogin: {
      height: 50,
      borderRadius: 25,
      borderWidth: 0.5,
      marginHorizontal: 20,
      paddingLeft: 10,
      marginVertical: 5,
      borderColor: "rgba(0,0,0,0.2)",
    },
    textInputLoginInvalid: {
      height: 50,
      borderRadius: 25,
      borderWidth: 0.5,
      marginHorizontal: 20,
      paddingLeft: 10,
      marginVertical: 5,
      borderColor: "rgba(255,0,0,1)",
    },
    closeButton: {
      height: 40,
      width: 40,
      backgroundColor: "white",
      borderRadius: 20,
      alignItems: "center",
      justifyContent: "center",
      top: -20,
      shadowColor: "black",
      shadowOpacity: 0.2,
    },
  });
}
