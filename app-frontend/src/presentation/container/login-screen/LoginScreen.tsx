import React from "react";
import {
  Alert,
  Animated,
  Dimensions,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  ScaledSize,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import LoginScreenStyle from "./LoginScreenStyle";
import Svg, {Circle, ClipPath, Image} from "react-native-svg";
import LoginInteractor, {
  LoginOutputPort,
} from "../../../domain/usecase/LoginInteractor";
import Account from "../../../data/model/Account";
import {connect} from "react-redux";
import {saveUserLogin} from "../../../domain/redux/action/user/UserAction";
import AwesomeLoading from "react-native-awesome-loading";

class LoginScreen extends React.Component<any, any> implements LoginOutputPort {
  private style = LoginScreenStyle.style;
  private screenInfo: ScaledSize = Dimensions.get("window");
  private loginUc: LoginInteractor = LoginInteractor.create(this);

  constructor(props: any) {
    super(props);
    this.state = {
      email: "",
      password: "",
      isCallingApi: false,
      isRunningAnimation: false,
      isValidEmail: true,
      isValidPassword: true,
      opacityViewButton: new Animated.Value(1),
      rotateCloseButton: new Animated.Value(180),
      zIndexFormSignIn: new Animated.Value(-1),
      opacitySignInForm: new Animated.Value(0),
      translateYBackground: new Animated.Value(0),
      translateYButton: new Animated.Value(0),
    };
  }

  loginSuccess(data: any): void {
    this.props.saveUserLogin(data);
    this.viewFormSignInFadeIn();
    this.setState((preState: any) => ({
      ...preState,
      email: "",
      password: "",
    }));
    this.props.navigation.navigate("Index");
  }

  loginError(error: any): void {
    Alert.alert("Tên đăng nhập hoặc mật khẩu không chính xác!");
    console.log("Login fail", error);
  }

  loginFinally(): void {
    this.setState((preState: any) => ({
      ...preState,
      isCallingApi: false,
    }));
  }

  componentDidMount() {}

  onLoginPressed() {
    Keyboard.dismiss();
    this.onHandleChangeEmail(this.state.email);
    this.onHandleChangePassword(this.state.password);
    if (
      this.state.isValidEmail &&
      this.state.isValidPassword &&
      this.state.email &&
      this.state.password
    ) {
      this.setState((preState: any) => ({
        ...preState,
        isCallingApi: true,
      }));
      const userLogin: Account = new Account();
      userLogin.email = this.state.email;
      userLogin.password = this.state.password;
      this.loginUc.login(userLogin);
    }
  }

  private viewButtonFadeOut = () => {
    this.setState({...this.state, isRunningAnimation: true});
    Animated.timing(this.state.opacityViewButton, {
      toValue: 0,
      duration: 900,
      useNativeDriver: false,
    }).start(() => {
      this.setState({...this.state, isRunningAnimation: false});
    });
    Animated.timing(this.state.translateYBackground, {
      toValue: -(this.screenInfo.height / 3 + 50),
      duration: 900,
      useNativeDriver: false,
    }).start();
    Animated.timing(this.state.translateYButton, {
      toValue: this.screenInfo.height / 3,
      duration: 900,
      useNativeDriver: false,
    }).start();
    Animated.timing(this.state.opacitySignInForm, {
      toValue: 1,
      duration: 900,
      useNativeDriver: false,
    }).start();
    Animated.timing(this.state.zIndexFormSignIn, {
      toValue: 1,
      duration: 900,
      useNativeDriver: false,
    }).start();
  };

  private viewFormSignInFadeIn = () => {
    Keyboard.dismiss();
    this.setState({...this.state, isRunningAnimation: true});
    Animated.timing(this.state.opacityViewButton, {
      toValue: 1,
      duration: 900,
      useNativeDriver: false,
      isInteraction: false,
    }).start(() => {
      this.setState({...this.state, isRunningAnimation: false});
    });
    Animated.timing(this.state.translateYBackground, {
      toValue: 0,
      duration: 900,
      useNativeDriver: false,
      isInteraction: false,
    }).start();
    Animated.timing(this.state.translateYButton, {
      toValue: 0,
      duration: 900,
      useNativeDriver: false,
      isInteraction: false,
    }).start();
    Animated.timing(this.state.opacitySignInForm, {
      toValue: 0,
      duration: 900,
      useNativeDriver: false,
    }).start();
    Animated.timing(this.state.zIndexFormSignIn, {
      toValue: -1,
      duration: 900,
      useNativeDriver: false,
    }).start();
  };

  public onInputChange = (field: string, value: string) => {
    this.setState((preState: any) => ({
      ...preState,
      [field]: value,
    }));
  };

  public onHandleChangeEmail(text: string) {
    const regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.setState((preState: any) => ({
      ...preState,
      isValidEmail: regExp.test(text),
    }));
  }

  public onHandleChangePassword(text: string) {
    this.setState((preState: any) => ({
      ...preState,
      isValidPassword: text.length >= 6,
    }));
  }
  render() {
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS ? "padding" : "height"}
        style={this.style.container}>
        <AwesomeLoading
          indicatorId={8}
          size={50}
          isActive={this.state.isCallingApi}
          text="loading"
        />
        <View
          style={{flex: 1, justifyContent: "flex-end", position: "relative"}}
          onTouchStart={() => {
            Keyboard.dismiss();
          }}>
          <Animated.View
            style={[
              {
                ...this.style.imageBackground,
                top: 0,
                left: 0,
                height: this.screenInfo.height,
                width: this.screenInfo.width,
                transform: [{translateY: this.state.translateYBackground}],
              },
            ]}>
            <Svg
              height={this.screenInfo.height + 50}
              width={this.screenInfo.width}>
              <ClipPath id="clip">
                <Circle
                  r={this.screenInfo.height + 50}
                  cx={this.screenInfo.width / 2}
                />
              </ClipPath>
              <Image
                href={require("../../../assets/image/background.jpeg")}
                height={this.screenInfo.height + 50}
                width={this.screenInfo.width}
                preserveAspectRatio="xMidYMid slice"
                clipPath="url(#clip)"
              />
            </Svg>
          </Animated.View>
          <Animated.View
            style={{
              height: this.screenInfo.height / 3,
              opacity: this.state.opacityViewButton,
              transform: [{translateY: this.state.translateYButton}],
              zIndex: 2,
            }}>
            <TouchableOpacity
              onPress={this.viewButtonFadeOut}
              disabled={this.state.isRunningAnimation}>
              <View
                style={{...this.style.buttonLogin, backgroundColor: "#2E71DC"}}>
                <Text
                  style={{fontSize: 20, fontWeight: "bold", color: "white"}}>
                  ĐĂNG NHẬP
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("Registration");
              }}>
              <View style={{...this.style.buttonLogin}}>
                <Text style={{fontSize: 20, fontWeight: "bold"}}>ĐĂNG KÍ</Text>
              </View>
            </TouchableOpacity>
          </Animated.View>
          <Animated.View
            style={{
              ...this.style.formSignIn,
              height: this.screenInfo.height / 3,
              width: this.screenInfo.width,
              opacity: this.state.opacitySignInForm,
              zIndex: this.state.zIndexFormSignIn,
            }}>
            <View>
              <TouchableOpacity
                onPress={this.viewFormSignInFadeIn}
                disabled={this.state.isRunningAnimation}
                style={{
                  ...this.style.closeButton,
                  left: this.screenInfo.width / 2 - 20,
                }}>
                <View>
                  <Text>X</Text>
                </View>
              </TouchableOpacity>
            </View>
            <TextInput
              placeholder="Tên đăng nhập / Email"
              placeholderTextColor="black"
              style={
                this.state.isValidEmail
                  ? this.style.textInputLogin
                  : this.style.textInputLoginInvalid
              }
              onChangeText={(e) => this.onInputChange("email", e)}
              onEndEditing={(e) => this.onHandleChangeEmail(e.nativeEvent.text)}
              value={this.state.email}
            />
            <TextInput
              placeholder="Mật khẩu"
              placeholderTextColor="black"
              secureTextEntry={true}
              style={
                this.state.isValidPassword
                  ? this.style.textInputLogin
                  : this.style.textInputLoginInvalid
              }
              value={this.state.password}
              onEndEditing={(e) =>
                this.onHandleChangePassword(e.nativeEvent.text)
              }
              onChangeText={(e) => this.onInputChange("password", e)}
            />
            <TouchableOpacity
              onPress={() => {
                this.onLoginPressed();
              }}>
              <View
                style={{
                  ...this.style.buttonLogin,
                  height: 60,
                  shadowColor: "black",
                  shadowOpacity: 0.2,
                }}>
                <Text style={{fontSize: 20, fontWeight: "bold"}}>Xác Nhận</Text>
              </View>
            </TouchableOpacity>
          </Animated.View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    token: state.UserReducers.USER_TOKEN,
    userInfo: state.UserReducers.USER_INFO,
  };
};

export default connect(mapStateToProps, {saveUserLogin})(LoginScreen);
