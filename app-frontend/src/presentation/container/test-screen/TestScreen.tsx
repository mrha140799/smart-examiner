import React from "react";
import AwesomeLoading from "react-native-awesome-loading/index";
import {
  Body,
  Button,
  Container,
  Header,
  Left,
  ListItem,
  Right,
  Text,
  Title,
  View,
} from "native-base";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";
import {Alert, FlatList, TextInput, TouchableOpacity} from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Moment from "moment";
import {CheckBox} from "react-native-elements";
import * as Animatable from "react-native-animatable";
import TestInteractor, {
  ITestOutputPort,
} from "../../../domain/usecase/TestInteractor";
import Test from "../../../data/model/Test";

class TestScreen extends React.Component<any, any> implements ITestOutputPort {
  private testUc: TestInteractor = TestInteractor.create(this);
  private readonly DETAIL_TYPE: string = "detail";
  private readonly UPDATE_TYPE: string = "update";
  private readonly DELETE_TYPE: string = "delete";
  constructor(props: any) {
    super(props);
    this.state = {
      screenType: "create",
      isLoading: false,
      testDay: new Date(),
      testId: null,
      isShowDatePicker: false,
      screenName: "TẠO BÀI TEST",
      testCode: "",
      subject: "",
      answers: {},
      invalidQuestions: {},
      validTestCode: true,
      validSubject: true,
      isNotExistsTestCode: true,
      disableDate: new Date(),
    };
  }

  componentDidMount() {
    if (this.props.route.params) {
      if (this.props.route.params.actionType === this.DETAIL_TYPE) {
        this.setState((preState: any) => ({
          ...preState,
          screenType: this.DETAIL_TYPE,
          screenName: "XEM BÀI TEST",
          isCallingApi: true,
        }));
        this.testUc.getById(this.props.route.params.testId);
      } else if (this.props.route.params.actionType === this.UPDATE_TYPE) {
        this.setState((preState: any) => ({
          ...preState,
          screenType: this.UPDATE_TYPE,
          screenName: "SỬA BÀI TEST",
          isCallingApi: true,
        }));
        this.testUc.getById(this.props.route.params.testId);
      }
    } else {
      this.generateDefaultAnswer();
    }
  }

  public onDateChange = (date?: Date) => {
    this.setState((preState: any) => ({
      ...preState,
      testDay: date,
      isShowDatePicker: false,
    }));
  };
  public onVisibleDate = () => {
    if (this.state.screenType !== this.DETAIL_TYPE) {
      this.setState((preState: any) => ({
        ...preState,
        isShowDatePicker: true,
      }));
    }
  };

  public onHandleHideDatePicker = () => {
    this.setState((preState: any) => ({
      ...preState,
      isShowDatePicker: false,
    }));
  };

  private generateDefaultAnswer = () => {
    const answer = {};
    const invalidQuestions = {};
    for (let i = 0; i < 50; i++) {
      (answer as any)["question_" + i] = "";
      (invalidQuestions as any)["question_" + i] = false;
    }
    this.setState((preState: any) => ({
      ...preState,
      answers: answer,
      invalidQuestions: invalidQuestions,
    }));
  };
  public onCheckedAnswer = (field: string, value: string) => {
    if (this.state.screenType !== this.DETAIL_TYPE) {
      const answers = this.state.answers;
      const invalidQuestions = this.state.invalidQuestions;
      answers[field] = value;
      invalidQuestions[field] = false;
      this.setState((preState: any) => ({
        ...preState,
        answers: answers,
        invalidQuestions: invalidQuestions,
      }));
    }
  };

  private setForState(field: string, data: any) {
    this.setState((preState: any) => ({
      ...preState,
      [field]: data,
    }));
  }

  private checkValidAnswers() {
    let valid = true;
    const invalidQuestions = this.state.invalidQuestions;
    for (const [key, value] of Object.entries(this.state.answers)) {
      if (value === "") {
        (invalidQuestions as any)[key] = true;
        valid = false;
      }
    }
    this.setForState("invalidQuestions", invalidQuestions);
    return valid;
  }

  private onSavePressed() {
    const validQuestions = this.checkValidAnswers();
    this.onHandleChangeInput("subject", this.state.subject);
    if (!this.state.testCode.length) {
      this.setForState("validTestCode", false);
    }
    if (
      this.state.isNotExistsTestCode &&
      this.state.validTestCode &&
      this.state.validSubject &&
      validQuestions
    ) {
      this.setForState("isCallingApi", true);
      const test: Test = new Test();
      test.id = this.state.testId;
      test.code = this.state.testCode;
      test.subject = this.state.subject;
      test.testDay = this.state.testDay;
      test.answers = this.getTestAnswers();
      if (this.state.screenType === this.UPDATE_TYPE) {
        this.testUc.updateTest(test);
      } else {
        this.testUc.createOne(test);
      }
    }
  }

  private getTestAnswers(): string {
    let answers = "";
    for (const [key, value] of Object.entries(this.state.answers)) {
      answers += value + "/";
    }
    return answers.slice(0, 99);
  }

  public onInputChange = (field: string, value: string) => {
    this.setState((preState: any) => ({
      ...preState,
      [field]: value,
    }));
  };
  public onHandleChangeInput = (field: string, text: string) => {
    const regex = /^[0-9]{4}/g;
    if (field === "testCode") {
      const valid = regex.test(text);
      if (valid) {
        this.testUc.isValidTestCode(text);
        this.setState((preState: any) => ({
          ...preState,
          isCallingApi: true,
          validTestCode: valid,
        }));
      } else {
        this.setState((preState: any) => ({
          ...preState,
          validTestCode: regex.test(text),
        }));
      }
    } else if (field === "subject") {
      this.setState((preState: any) => ({
        ...preState,
        validSubject: text.length > 1,
      }));
    }
  };

  updateTestError(): any {
    this.setForState("isCallingApi", false);
    Alert.alert("Lỗi, sửa bài thi thất bại!");
  }

  updateTestSuccessfully(): any {
    this.setForState("isCallingApi", false);
    Alert.alert("Sửa bài thi thành công!");
    this.props.navigation.navigate("ListTest");
  }

  createOneError(): any {
    this.setForState("isCallingApi", false);
    Alert.alert("Lỗi, Tạo bài thi thất bại!");
  }

  createOneSuccessfully(): any {
    this.setForState("isCallingApi", false);
    Alert.alert("Tạo bài thi thành công!");
    this.props.navigation.navigate("ListTest");
  }

  invalidTestCode(): any {
    this.setState((preState: any) => ({
      ...preState,
      isCallingApi: false,
      isNotExistsTestCode: false,
    }));
  }

  isValidTestCode(): any {
    this.setState((preState: any) => ({
      ...preState,
      isCallingApi: false,
      isNotExistsTestCode: true,
    }));
  }

  getByIdError(): any {
    Alert.alert("Lỗi, không tìm thấy thông tin bài test");
    this.setForState("isCallingApi", false);
  }

  getByIdSuccessfully(test: Test): any {
    this.setState((preState: any) => ({
      ...preState,
      isCallingApi: false,
      testCode: test.code,
      subject: test.subject,
      testDay: test.testDay,
      testId: test.id,
      disableDate: new Date(test.testDay),
    }));
    this.convertToListAnswers(test.answers);
  }

  private convertToListAnswers(answerTest: string) {
    const answers = answerTest.split("/");
    const answer = {};
    const invalidQuestions = {};
    for (let i = 0; i < 50; i++) {
      (answer as any)["question_" + i] = answers[i];
      (invalidQuestions as any)["question_" + i] = false;
    }
    this.setState((preState: any) => ({
      ...preState,
      answers: answer,
      invalidQuestions: invalidQuestions,
    }));
  }

  render() {
    return (
      <>
        <AwesomeLoading
          indicatorId={8}
          size={50}
          isActive={this.state.isCallingApi}
          text="loading"
        />
        <Container>
          <Header style={{borderBottomColor: "red", borderBottomWidth: 1}}>
            <Left>
              <Button
                transparent
                onPress={() => {
                  this.props.navigation.goBack();
                }}>
                <FontAwesome5Icon
                  style={{fontSize: 25, paddingLeft: 5, color: "red"}}
                  name="angle-left"
                />
              </Button>
            </Left>
            <Body>
              <Title>{this.state.screenName}</Title>
            </Body>
            <Right>
              {this.state.screenType !== this.DETAIL_TYPE && (
                <Button
                  transparent
                  onPress={() => {
                    this.onSavePressed();
                  }}>
                  <FontAwesome5Icon
                    style={{fontSize: 20, paddingLeft: 5, color: "red"}}
                    name="check"
                  />
                </Button>
              )}
            </Right>
          </Header>
          <View style={{flex: 1}}>
            <View style={{marginVertical: 10}}>
              <View style={{marginHorizontal: 10, marginBottom: 5}}>
                <Text>Mã Đề:</Text>
              </View>
              <View>
                <TextInput
                  style={{
                    height: 40,
                    width: "90%",
                    borderColor: "rgba(0,0,0,0.2)",
                    borderRadius: 10,
                    borderWidth: 0.5,
                    marginHorizontal: 20,
                    paddingLeft: 10,
                    marginVertical: 5,
                  }}
                  editable={this.state.screenType !== this.DETAIL_TYPE}
                  placeholder="Mã đề"
                  placeholderTextColor="gray"
                  onChangeText={(e) => this.onInputChange("testCode", e)}
                  onEndEditing={(e) =>
                    this.onHandleChangeInput("testCode", e.nativeEvent.text)
                  }
                  value={this.state.testCode}
                />
                {!this.state.validTestCode && (
                  <Animatable.View animation="fadeInLeft" duration={500}>
                    <Text
                      style={{
                        fontSize: 14,
                        alignContent: "center",
                        marginHorizontal: 20,
                        fontStyle: "italic",
                        color: "red",
                      }}>
                      * Mã đề không hợp lệ
                    </Text>
                  </Animatable.View>
                )}
                {!this.state.isNotExistsTestCode && (
                  <Animatable.View animation="fadeInLeft" duration={500}>
                    <Text
                      style={{
                        fontSize: 14,
                        alignContent: "center",
                        marginHorizontal: 20,
                        fontStyle: "italic",
                        color: "red",
                      }}>
                      * Mã đề đã tồn tại
                    </Text>
                  </Animatable.View>
                )}
              </View>
            </View>
            <View style={{marginVertical: 10}}>
              <View style={{marginHorizontal: 10, marginBottom: 5}}>
                <Text>Tên Môn Thi:</Text>
              </View>
              <View>
                <TextInput
                  style={{
                    height: 40,
                    width: "90%",
                    borderColor: "rgba(0,0,0,0.2)",
                    borderRadius: 10,
                    borderWidth: 0.5,
                    marginHorizontal: 20,
                    paddingLeft: 10,
                    marginVertical: 5,
                  }}
                  editable={this.state.screenType !== this.DETAIL_TYPE}
                  placeholder="Tên môn thi"
                  placeholderTextColor="gray"
                  onChangeText={(e) => this.onInputChange("subject", e)}
                  onEndEditing={(e) =>
                    this.onHandleChangeInput("subject", e.nativeEvent.text)
                  }
                  value={this.state.subject}
                />
                {!this.state.validSubject && (
                  <Animatable.View animation="fadeInLeft" duration={500}>
                    <Text
                      style={{
                        fontSize: 14,
                        alignContent: "center",
                        marginHorizontal: 20,
                        fontStyle: "italic",
                        color: "red",
                      }}>
                      * Tên môn học không hợp lệ
                    </Text>
                  </Animatable.View>
                )}
              </View>
            </View>
            <View style={{marginVertical: 10}}>
              <View>
                <View style={{marginHorizontal: 10, marginBottom: 5}}>
                  <Text>Ngày thi:</Text>
                </View>
                <TouchableOpacity onPress={this.onVisibleDate}>
                  <View
                    style={{
                      height: 40,
                      width: "90%",
                      borderColor: "rgba(0,0,0,0.2)",
                      borderRadius: 10,
                      borderWidth: 0.5,
                      marginHorizontal: 20,
                      paddingLeft: 10,
                      marginVertical: 5,
                      flexDirection: "row",
                      alignItems: "center",
                    }}>
                    <Text
                      style={{
                        alignContent: "center",
                        color: "black",
                        fontSize: 14,
                      }}>
                      {Moment(this.state.testDay).format("DD/MM/YYYY")}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <DateTimePickerModal
                headerTextIOS={"Chọn ngày thi"}
                confirmTextIOS={"Xác nhận"}
                minimumDate={this.state.disableDate}
                cancelTextIOS={"Hủy bỏ"}
                isVisible={this.state.isShowDatePicker}
                mode="date"
                onConfirm={this.onDateChange}
                locale={"vi"}
                onCancel={this.onHandleHideDatePicker}
              />
              <View />
            </View>
            <View>
              <ListItem
                thumbnail
                style={{
                  borderBottomWidth: 1,
                  width: "90%",
                }}>
                <Left style={{width: "10%"}}>
                  <Text>Câu</Text>
                </Left>
                <Body
                  style={{
                    flexDirection: "row",
                    width: "75%",
                    paddingRight: 5,
                    borderBottomWidth: 0,
                  }}>
                  <Text
                    style={{
                      width: "25%",
                      textAlign: "center",
                      marginLeft: 5,
                    }}>
                    A
                  </Text>
                  <Text
                    style={{
                      width: "25%",
                      textAlign: "center",
                      marginLeft: -10,
                    }}>
                    B
                  </Text>
                  <Text
                    style={{
                      width: "25%",
                      textAlign: "center",
                      marginLeft: -10,
                    }}>
                    C
                  </Text>
                  <Text
                    style={{
                      width: "25%",
                      textAlign: "center",
                      marginLeft: -10,
                    }}>
                    D
                  </Text>
                </Body>
              </ListItem>
            </View>
            <View style={{height: "42%"}}>
              <FlatList
                data={Object.keys(this.state.answers)}
                renderItem={({item, index}) => (
                  <ListItem
                    thumbnail
                    key={item}
                    style={{
                      borderBottomWidth: 1,
                      width: "90%",
                    }}>
                    <Left
                      style={{
                        width: "10%",
                        flexDirection: "row",
                        justifyContent: "center",
                      }}>
                      <Text
                        style={
                          this.state.invalidQuestions[item]
                            ? {textAlign: "center", color: "red"}
                            : {textAlign: "center"}
                        }>
                        {index + 1}
                      </Text>
                    </Left>
                    <Body
                      style={{
                        flexDirection: "row",
                        width: "75%",
                        paddingRight: 5,
                        borderBottomWidth: 0,
                      }}>
                      <View
                        style={{
                          width: "22%",
                          marginLeft: 10,
                          flexDirection: "row",
                          justifyContent: "center",
                        }}>
                        <CheckBox
                          size={33}
                          checkedColor={"red"}
                          uncheckedColor={
                            this.state.invalidQuestions[item]
                              ? "red"
                              : undefined
                          }
                          checkedIcon="dot-circle-o"
                          uncheckedIcon="circle-o"
                          checked={this.state.answers[item] === "A"}
                          onPress={() => this.onCheckedAnswer(item, "A")}
                        />
                      </View>
                      <View
                        style={{
                          width: "22%",
                          marginLeft: 10,
                          flexDirection: "row",
                          justifyContent: "center",
                        }}>
                        <CheckBox
                          size={33}
                          uncheckedColor={
                            this.state.invalidQuestions[item]
                              ? "red"
                              : undefined
                          }
                          checkedColor={"red"}
                          checkedIcon="dot-circle-o"
                          uncheckedIcon="circle-o"
                          checked={this.state.answers[item] === "B"}
                          onPress={() => this.onCheckedAnswer(item, "B")}
                        />
                      </View>
                      <View
                        style={{
                          width: "22%",
                          marginLeft: 10,
                          flexDirection: "row",
                          justifyContent: "center",
                        }}>
                        <CheckBox
                          size={33}
                          uncheckedColor={
                            this.state.invalidQuestions[item]
                              ? "red"
                              : undefined
                          }
                          checkedColor={"red"}
                          checkedIcon="dot-circle-o"
                          uncheckedIcon="circle-o"
                          checked={this.state.answers[item] === "C"}
                          onPress={() => this.onCheckedAnswer(item, "C")}
                        />
                      </View>
                      <View
                        style={{
                          width: "22%",
                          marginLeft: 10,
                          flexDirection: "row",
                          justifyContent: "center",
                        }}>
                        <CheckBox
                          size={33}
                          uncheckedColor={
                            this.state.invalidQuestions[item]
                              ? "red"
                              : undefined
                          }
                          checkedColor={"red"}
                          checkedIcon="dot-circle-o"
                          uncheckedIcon="circle-o"
                          checked={this.state.answers[item] === "D"}
                          onPress={() => this.onCheckedAnswer(item, "D")}
                        />
                      </View>
                    </Body>
                  </ListItem>
                )}
                keyExtractor={(item) => item}
              />
            </View>
          </View>
        </Container>
      </>
    );
  }
}

export default TestScreen;
