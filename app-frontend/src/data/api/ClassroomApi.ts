import BaseApi from "./BaseApi";
import IClassroomRepository from "../repository/IClassroomRepository";

export default class ClassroomApi
  extends BaseApi
  implements IClassroomRepository {
  getAllMyClassroom(): Promise<any> {
    return this.get("/api/user/my-classrooms", {});
  }
}
