import ITestRepository from "../repository/ITestRepository";
import Test from "../model/Test";
import BaseApi from "./BaseApi";

export default class TestApi extends BaseApi implements ITestRepository {
  myTests(): Promise<any> {
    return this.get("/api/user/my-tests", {});
  }
  createOne(test: Test): Promise<any> {
    return this.post("/api/test", test);
  }

  isValidTestCode(code: string): Promise<any> {
    return this.get(`/api/test/valid-test-code?code=${code}`, {});
  }

  deleteById(id: number): Promise<any> {
    return this.delete(`/api/test/${id}`, {});
  }

  getById(id: number): Promise<any> {
    return this.get(`/api/test/${id}`, {});
  }

  updateTest(test: Test): Promise<any> {
    return this.put("/api/test", {}, test);
  }
}
