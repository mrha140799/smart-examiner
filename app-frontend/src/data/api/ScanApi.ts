import BaseApi from "./BaseApi";
import IScanRepository from "../repository/IScanRepository";

export default class ScanApi extends BaseApi implements IScanRepository {
  saveFile(fileContent: string): Promise<any> {
    return this.post("/api/scan/my-answer", {fileContent: fileContent});
  }

  myScanHistories(): Promise<any> {
    return this.get("/api/scan/my-histories", {});
  }
}
