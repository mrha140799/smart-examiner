import IStudentCodeRepository from "../repository/IStudentCodeRepository";
import BaseApi from "./BaseApi";
import StudentCodeRequest from "../model/StudentCodeRequest";

export default class StudentCodeApi
  extends BaseApi
  implements IStudentCodeRepository {
  getAllByTestId(testId: number): Promise<any> {
    return this.get(`/api/student-codes/get-by-test-id?testId=${testId}`, {});
  }

  createOne(studentCode: StudentCodeRequest): Promise<any> {
    return this.post("/api/student-codes", studentCode);
  }

  updateOne(studentCode: StudentCodeRequest): Promise<any> {
    return this.put("/api/student-codes", {}, studentCode);
  }

  getOneById(id: number): Promise<any> {
    return this.get(`/api/student-codes/${id}`, {});
  }

  deleteById(id: number | null): Promise<any> {
    return this.delete(`/api/student-codes/${id}`, {});
  }
}
