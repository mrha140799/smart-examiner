import axios from "axios";
import Commons from "../../assets/json/Commons.json";
import Store from "../../domain/sotre/Store";

export default abstract class BaseApi {
  protected readonly tokenKey: string = Commons.storageKey.TOKEN_KEY;
  protected readonly PRODUCT_API_URI: string = Commons.api.product_uri;
  protected readonly DEV_API_URI: string = Commons.api.dev_uri;

  public constructor() {
    axios.interceptors.response.use(
      function (response: any) {
        return response;
      },
      function (error: any) {
        return Promise.reject(error);
      },
    );
  }

  protected baseUrl(): string {
    // return this.PRODUCT_API_URI;
    return this.DEV_API_URI;
    // return "http://localhost:8080";
  }

  protected get(path: string, params: object): Promise<any> {
    const headers = this.generateHeaders();
    if (!this.isEmptyObject(headers)) {
      return axios.get(this.baseUrl() + path, {
        headers: headers,
        params: params,
      });
    }
    return axios.get(this.baseUrl() + path, {params: params});
  }

  protected post(path: string, body: object): Promise<any> {
    const headers = this.generateHeaders();
    if (!this.isEmptyObject(headers)) {
      return axios.post(this.baseUrl() + path, body, {headers: headers});
    }
    return axios.post(this.baseUrl() + path, body);
  }

  protected put(path: string, params: object, body: object): Promise<any> {
    const headers = this.generateHeaders();
    if (!this.isEmptyObject(headers)) {
      return axios.put(this.baseUrl() + path, body, {
        headers: headers,
        params: params,
      });
    }
    return axios.put(this.baseUrl() + path, params);
  }

  protected delete(path: string, params: object): Promise<any> {
    const headers = this.generateHeaders();
    if (!this.isEmptyObject(headers)) {
      return axios.delete(this.baseUrl() + path, {
        data: params,
        headers: headers,
      });
    }
    return axios.delete(this.baseUrl() + path, {data: params});
  }

  private isEmptyObject(object: any): boolean {
    return Object.keys(object).length === 0 || !object.Authorization;
  }

  private generateHeaders() {
    const token = Store.getState().UserReducers.USER_TOKEN;
    return {
      "Content-type": "application/json; charset=UTF-8",
      Authorization: token ? `Bearer ${token}` : false,
      Accept: "*",
    };
  }
}
