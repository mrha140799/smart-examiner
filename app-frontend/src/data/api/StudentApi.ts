import BaseApi from "./BaseApi";
import IStudentRepository from "../repository/IStudentRepository";

export default class StudentApi extends BaseApi implements IStudentRepository {
  getMyStudents(): Promise<any> {
    return this.get("/api/user/my-students", {});
  }

  getAllByClassId(id: number): Promise<any> {
    return this.get(`/api/students/get-all-by-class-id?classId=${id}`, {});
  }
}
