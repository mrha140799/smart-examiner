import BaseApi from "./BaseApi";
import Account from "../model/Account";
import IAuthRepository from "../repository/IAuthRepository";
import ResponseMessage from "../model/ResponseMessage";
import User from "../model/User";

export default class AuthApi extends BaseApi implements IAuthRepository {
  register(user: User): Promise<any> {
    return this.post("/api/auth/register", user);
  }
  isValidEmail(email: string): Promise<any> {
    return this.get(`/api/auth/is-valid-email?email=${email}`, {});
  }
  public login(account: Account): Promise<ResponseMessage<any>> {
    return this.post(
      `/api/auth/login?email=${account.email}&password=${account.password}`,
      {},
    );
  }
}
