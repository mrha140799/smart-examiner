import BaseApi from "./BaseApi";
import IUserRepository from "../repository/IUserRepository";
import User from "../model/User";

export default class UserApi extends BaseApi implements IUserRepository {
  updateProfile(user: User): Promise<any> {
    return this.post("/api/user/change-my-profile", user);
  }
  me(): Promise<any> {
    return this.get("/api/user/me", {});
  }

  changeAvatar(fileContent: String): Promise<any> {
    return this.post("/api/user/change-my-avatar", {fileContent: fileContent});
  }
}
