export default class Test {
  public id: number | null = null;
  public code: string = "0010";
  public subject: string = "Toán";
  public testDay: Date = new Date();
  public answers: string = "";
}
