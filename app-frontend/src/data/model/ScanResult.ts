export default class ScanResult {
  public studentFullName: string = "";
  public studentCode: string = "";
  public testCode: string = "";
  public score: number = 0;
  public numberQuestionCorrect: string = "";
  public subjectName: string = "";
  public testDay: Date = new Date();
  public imageFullFileName: string = "";
  public imageAnswerFileName: string = "";
  public imageUserInfoFileName: string = "";
  public createdTime: Date = new Date();
}
