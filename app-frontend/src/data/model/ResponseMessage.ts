export default class ResponseMessage<T> {
  public status_code: number = 404;
  public message: string = "";
  public data?: T;
}
