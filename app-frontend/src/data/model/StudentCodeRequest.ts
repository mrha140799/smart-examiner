export default class StudentCodeRequest {
  public id: number | null = null;
  public code: string = "";
  public studentId: number | null = null;
  public testId: number | null = null;
}
