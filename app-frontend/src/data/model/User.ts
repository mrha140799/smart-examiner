export default class User {
  public id?: string;
  public firstName: string = "";
  public lastName: string = "";
  public gender: boolean | null = true;
  public phoneNumber: string = "";
  public email: string = "";
  public password: string = "";
}
