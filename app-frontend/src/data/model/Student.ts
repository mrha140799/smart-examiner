import Classroom from "./Classroom";

export default class Student {
  public id: number | null = null;
  public fullName: string = "";
  public dateOfBirth: Date = new Date();
  public classroom: Classroom = new Classroom();
}
