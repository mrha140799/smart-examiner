export default class StudentCode {
  public id: number | null = null;
  public code: string = "";
  public fullName: string = "";
  public className: string = "";
}
