import Test from "../model/Test";

export default abstract class ITestRepository {
  abstract createOne(test: Test): Promise<any>;
  abstract isValidTestCode(code: string): Promise<any>;
  abstract myTests(): Promise<any>;
  abstract deleteById(id: number | null): Promise<any>;
  abstract updateTest(test: Test): Promise<any>;
  abstract getById(id: number): Promise<any>;
}
