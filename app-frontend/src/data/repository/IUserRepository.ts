import User from "../model/User";

export default abstract class IUserRepository {
  abstract me(): Promise<any>;
  abstract updateProfile(user: User): Promise<any>;
  abstract changeAvatar(fileContent: String): Promise<any>;
}
