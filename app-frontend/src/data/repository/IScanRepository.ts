export default abstract class IScanRepository {
  abstract saveFile(fileContent: string): Promise<any>;
  abstract myScanHistories(): Promise<any>;
}
