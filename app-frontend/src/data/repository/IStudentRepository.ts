export default abstract class IStudentRepository {
  abstract getMyStudents(): Promise<any>;
  abstract getAllByClassId(id: number): Promise<any>;
}
