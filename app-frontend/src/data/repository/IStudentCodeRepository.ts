import StudentCodeRequest from "../model/StudentCodeRequest";

export default abstract class IStudentCodeRepository {
  abstract getAllByTestId(testId: number): Promise<any>;
  abstract createOne(studentCode: StudentCodeRequest): Promise<any>;
  abstract updateOne(studentCode: StudentCodeRequest): Promise<any>;
  abstract getOneById(id: number): Promise<any>;
  abstract deleteById(id: number | null): Promise<any>;
}
