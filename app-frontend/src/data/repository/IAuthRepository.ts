import Account from "../model/Account";
import User from "../model/User";

export default abstract class IAuthRepository {
  abstract login(user: Account): Promise<any>;
  abstract isValidEmail(email: string): Promise<any>;
  abstract register(user: User): Promise<any>;
}
