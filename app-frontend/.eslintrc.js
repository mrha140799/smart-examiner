module.exports = {
  root: true,
  extends: ["@react-native-community"],
  plugins: ["prettier"],
  rules: {
    "prettier/prettier": "error",
    "quotes": ["error", "double"],
    "semi": ["error", "always"],
    "semi-spacing": ["error", {"after": true, "before": false}],
    "semi-style": ["error", "last"],
    'space-before-function-paren': 0,
    "no-extra-semi": "error",
    "no-unexpected-multiline": "error",
    "no-unreachable": "error",
  }
};
