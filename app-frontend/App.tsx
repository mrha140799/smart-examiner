/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */
import "react-native-gesture-handler";
import React from "react";
import {Text, View} from "react-native";
import AppNavigation from "./src/presentation/navigation/AppNavigation";
import {Provider} from "react-redux";
import Store from "./src/domain/sotre/Store";
import {SafeAreaProvider} from "react-native-safe-area-view";
import {ThemeProvider} from "react-native-elements";

export default class App extends React.Component {
  private isReady: boolean = true;

  constructor(props: any) {
    super(props);
  }

  render() {
    return !this.isReady ? (
      <View>
        <Text>Loading....</Text>
      </View>
    ) : (
      <Provider store={Store}>
        <SafeAreaProvider>
          <ThemeProvider>
            <AppNavigation />
          </ThemeProvider>
        </SafeAreaProvider>
      </Provider>
    );
  }
}
