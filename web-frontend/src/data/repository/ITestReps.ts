import Test from "@/model/Test";

export default abstract class ITestReps {
  abstract getAllMyTests(): Promise<any>;
  abstract createOne(test: Test): Promise<any>;
  abstract isValidTestCode(code: string, testId: number | null): Promise<any>;
  abstract deleteById(id: number | null): Promise<any>;
  abstract updateTest(test: Test): Promise<any>;
  abstract getById(id: number): Promise<any>;
  abstract getCreatedFile(): Promise<any>;
  abstract getFormAnswerFile(): Promise<any>;
}
