import { IAccount } from "@/model/Account";
import { IUser } from "@/model/User";

export default abstract class IAuthReps {
  abstract login(user: IAccount): Promise<any>;
  abstract register(user: IUser): Promise<any>;
}
