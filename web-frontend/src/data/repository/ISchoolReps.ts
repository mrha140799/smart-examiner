import School from "@/model/School";

export default abstract class ISchoolReps {
  abstract getAllMySchools(): Promise<any>;
  abstract createOne(school: School): Promise<any>;
  abstract getOneById(id: any): Promise<any>;
  abstract deleteById(id: any): Promise<any>;
  abstract updateOne(school: School): Promise<any>;
}
