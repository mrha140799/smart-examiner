import Students from "@/model/Students";
import Student from "@/model/Student";

export default abstract class IStudentReps {
  abstract getMyStudents(): Promise<any>;
  abstract getAllByClassId(id: number): Promise<any>;
  abstract getCreatedFile(): Promise<any>;
  abstract createMany(students: Students): Promise<any>;
  abstract createOne(student: Student, classroomId: number): Promise<any>;
  abstract updateOne(student: Student, classroomId: number): Promise<any>;
  abstract deleteById(id: any): Promise<any>;
}
