import { IUser } from "@/model/User";

export default abstract class IUserReps {
  abstract saveUserInfo(token: string, userInf: string): void;
  abstract getCurrentUser(): IUser;
  abstract isLoggedOn(): boolean;
  abstract logout(): void;
}
