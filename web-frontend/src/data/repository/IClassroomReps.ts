import School from "@/model/School";
import Classroom from "@/model/Classroom";

export default abstract class IClassroomReps {
  abstract getAllMyClasses(): Promise<any>;
  abstract createOne(classroom: Classroom): Promise<any>;
  abstract getOneById(id: any): Promise<any>;
  abstract updateOne(classroom: Classroom): Promise<any>;
  abstract deleteById(id: any): Promise<any>;
}
