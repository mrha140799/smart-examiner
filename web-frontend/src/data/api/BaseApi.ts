import axios from "axios";
import Commons from "../../assets/json/Commons.json";
import { injectable } from "inversify";

@injectable()
export default abstract class BaseApi {
  protected readonly tokenKey: string = Commons.storageKey.TOKEN_KEY;

  constructor() {
    axios.interceptors.response.use(function (response) {
      return (response);
    }, function (error: any) {
      if (error.response.status === 403) {
        (window as any).vm.$router.push("/auth/login");
      }
      return Promise.reject(error);
    });
  }

  protected baseUrl(): string {
    return "http://localhost:8080";
    // return process.env.VUE_APP_API_URL;
  }

  protected get(path: string, params: object, header?: any): Promise<any> {
    const headers = this.generateHeaders();
    if (!this.isEmptyObject(headers)) {
      return axios.get(this.baseUrl() + path, {
        headers: headers,
        params: params
      });
    }
    return axios.get(this.baseUrl() + path, { params: params });
  }

  protected post(path: string, body: object): Promise<any> {
    const headers = this.generateHeaders();
    if (!this.isEmptyObject(headers)) {
      return axios.post(this.baseUrl() + path, body, { headers: headers });
    }
    return axios.post(this.baseUrl() + path, body);
  }

  protected put(path: string, params: object, body: object): Promise<any> {
    const headers = this.generateHeaders();
    if (!this.isEmptyObject(headers)) {
      return axios.put(this.baseUrl() + path, body, {
        headers: headers,
        params: params
      });
    }
    return axios.put(this.baseUrl() + path, params);
  }

  protected delete(path: string, params: object): Promise<any> {
    const headers = this.generateHeaders();
    if (!this.isEmptyObject(headers)) {
      return axios.delete(this.baseUrl() + path, {
        data: params,
        headers: headers
      });
    }
    return axios.delete(this.baseUrl() + path, { data: params });
  }

  protected deconste(path: string, params: object): Promise<any> {
    const headers = this.generateHeaders();
    if (!this.isEmptyObject(headers)) {
      return axios.delete(this.baseUrl() + path, {
        data: params,
        headers: headers
      });
    }
    return axios.delete(this.baseUrl() + path, { data: params });
  }

  private isEmptyObject(object: any): boolean {
    return (Object.keys(object).length === 0 || !object.Authorization);
  }

  private generateHeaders(): object {
    if (localStorage.getItem(this.tokenKey)) {
      return {
        "Content-type": "application/json; charset=UTF-8",
        Accept: "*",
        Authorization: `Bearer ${localStorage.getItem(this.tokenKey)}`
      };
    } else {
      return {};
    }
  }
}
