import BaseApi from "@/data/api/BaseApi";
import ISchoolReps from "@/data/repository/ISchoolReps";
import { injectable } from "inversify";
import School from "@/model/School";
import IClassroomReps from "@/data/repository/IClassroomReps";
import Classroom from "@/model/Classroom";

@injectable()
export default class ClassroomApi extends BaseApi implements IClassroomReps {
  getAllMyClasses(): Promise<any> {
    return this.get("/api/user/my-classrooms", {});
  }

  createOne(classroom: Classroom) {
    return this.post("/api/classroom", classroom);
  }

  getOneById(id: any): Promise<any> {
    return this.get("/api/classroom/" + id, {});
  }

  updateOne(classroom: Classroom): Promise<any> {
    return this.put("/api/classroom", {}, classroom);
  }

  deleteById(id: any): Promise<any> {
    return this.delete("/api/classroom/" + id, {});
  }
}
