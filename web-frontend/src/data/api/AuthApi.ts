import BaseApi from "@/data/api/BaseApi";
import { IAccount } from "@/model/Account";
import IAuthReps from "@/data/repository/IAuthReps";
import { injectable } from "inversify";
import { IUser } from "@/model/User";

@injectable()
export default class AuthApi extends BaseApi implements IAuthReps {
  public register(user: IUser): Promise<any> {
    return this.post("/api/auth/register", user);
  }

  public login(user: IAccount): Promise<any> {
    return this.post(`/api/auth/login?email=${user.email}&password=${user.password}`, {});
  }
}
