import BaseApi from "@/data/api/BaseApi";
import ISchoolReps from "@/data/repository/ISchoolReps";
import { injectable } from "inversify";
import School from "@/model/School";

@injectable()
export default class SchoolApi extends BaseApi implements ISchoolReps {
  getAllMySchools(): Promise<any> {
    return this.get("/api/user/my-schools", {});
  }

  createOne(school: School) {
    return this.post("/api/school", school);
  }

  getOneById(id: any): Promise<any> {
    return this.get("/api/school/" + id, {});
  }

  updateOne(school: School): Promise<any> {
    return this.put("/api/school", {}, school);
  }

  deleteById(id: any): Promise<any> {
    return this.delete("/api/school/" + id, {});
  }
}
