import BaseApi from "./BaseApi";
import IStudentReps from "../repository/IStudentReps";
import Students from "@/model/Students";
import Student from "@/model/Student";

export default class StudentApi extends BaseApi implements IStudentReps {
  getMyStudents(): Promise<any> {
    return this.get("/api/user/my-students", {});
  }

  getAllByClassId(id: number): Promise<any> {
    return this.get(`/api/students/get-all-by-class-id?classId=${id}`, {});
  }

  createMany(students: Students): Promise<any> {
    return this.post("/api/students/create-many", students);
  }

  createOne(student: Student, classroomId: number): Promise<any> {
    return this.post(`/api/students?classroomId=${classroomId}`, student);
  }

  updateOne(student: Student, classroomId: number): Promise<any> {
    return this.put(`/api/students?classroomId=${classroomId}`, {}, student);
  }

  getCreatedFile(): Promise<any> {
    return this.get("/api/user/file-student-created", {});
  }

  deleteById(id: any): Promise<any> {
    return this.delete("/api/students/" + id, {});
  }
}
