import BaseApi from "@/data/api/BaseApi";
import ISchoolReps from "@/data/repository/ISchoolReps";
import { injectable } from "inversify";
import School from "@/model/School";
import ITestReps from "@/data/repository/ITestReps";
import Test from "@/model/Test";

@injectable()
export default class TestApi extends BaseApi implements ITestReps {
  getFormAnswerFile(): Promise<any> {
    return this.get("/api/user/file-form-answer", {});
  }

  getAllMyTests(): Promise<any> {
    return this.get("/api/user/my-tests", {});
  }

  createOne(test: Test) {
    return this.post("/api/test", test);
  }

  isValidTestCode(code: string, testId: number | null): Promise<any> {
    if (testId) {
      return this.get(`/api/test/valid-test-code?code=${code}&testId=${testId}`, {});
    } else {
      return this.get(`/api/test/valid-test-code?code=${code}`, {});
    }
  }

  deleteById(id: number): Promise<any> {
    return this.delete(`/api/test/${id}`, {});
  }

  getById(id: number): Promise<any> {
    return this.get(`/api/test/${id}`, {});
  }

  updateTest(test: Test): Promise<any> {
    return this.put("/api/test", {}, test);
  }

  getCreatedFile(): Promise<any> {
    return this.get("/api/user/file-test-created", {});
  }
}
