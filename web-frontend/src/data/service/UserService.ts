import Commons from "@/assets/json/Commons.json";
import { injectable } from "inversify";
import IUserReps from "@/data/repository/IUserReps";
import { IUser } from "@/model/User";

@injectable()
export default class UserService implements IUserReps {
  private readonly TOKEN_KEY: string;
  private readonly USERNAME_KEY: string;

  public constructor() {
    this.TOKEN_KEY = Commons.storageKey.TOKEN_KEY;
    this.USERNAME_KEY = Commons.storageKey.USERNAME_KEY;
  }

  isLoggedOn(): boolean | any {
    return localStorage.getItem(this.TOKEN_KEY) && localStorage.getItem(this.USERNAME_KEY);
  }

  saveUserInfo(token: string, username: string): void {
    localStorage.setItem(this.TOKEN_KEY, token);
    localStorage.setItem(this.USERNAME_KEY, username);
  }

  logout() {
    localStorage.getItem(this.TOKEN_KEY) && localStorage.removeItem(this.TOKEN_KEY);
    localStorage.getItem(this.USERNAME_KEY) && localStorage.removeItem(this.USERNAME_KEY);
  }

  getCurrentUser(): IUser {
    return JSON.parse(localStorage.getItem(this.USERNAME_KEY) ?? "");
  }
}
