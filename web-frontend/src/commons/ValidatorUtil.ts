export default class ValidatorUtil {
  private static _instance: ValidatorUtil;
  private readonly _required: any;
  private readonly _maxLength: any;
  private readonly _isImageURL: any;
  public readonly minLength: any;

  private constructor() {
    this._required = {
      required: true,
      message: "Bắt buộc nhập",
      trigger: "blur"
    };
    this._maxLength = function (length: number) {
      return {
        max: length,
        message: "Độ dài nhỏ hơn " + length + " ký tự",
        trigger: "blur"
      };
    };
    this.minLength = function (length: number) {
      return {
        min: length,
        message: "Độ dài lớn hơn " + length + " ký tự",
        trigger: "blur"
      };
    };
    this._isImageURL = {
      pattern: "https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@!^$*:%_\\+.~#?&//=]*)\\.(jpeg|png|jpg)$",
      message: "Không đúng định dạng",
      trigger: "blur"
    };
  }

  public static get instance(): ValidatorUtil {
    if (this._instance === undefined) {
      this._instance = new ValidatorUtil();
    }
    return this._instance;
  }

  get required(): any {
    return this._required;
  }

  get maxLength(): any {
    return this._maxLength;
  }

  get isImageURL(): any {
    return this._isImageURL;
  }
}
