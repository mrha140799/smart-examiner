import Navigations from "../../../assets/json/Navigation.json";
import BasePage from "@/presentation/views/BasePage";
import Component from "vue-class-component";
import DIContainer from "@/core/DIContainer";
import IUserReps from "@/data/repository/IUserReps";
import User, { IUser } from "@/model/User";

@Component
export default class DefaultContainer extends BasePage {
  public isShowMenuContent: boolean;
  public navigationName: string;
  public navigationList: Array<INavigation>;
  public userService: IUserReps;
  public currentUser: IUser = new User();

  public constructor() {
    super();
    this.navigationName = "Trang quản trị";
    this.navigationList = Navigations;
    this.isShowMenuContent = false;
    this.userService = DIContainer.instance.get(IUserReps);
  }

  public onChangeMenuContent() {
    this.isShowMenuContent = !this.isShowMenuContent;
  }

  public created() {
    if (!this.userService.isLoggedOn()) {
      this.$router.push("/auth/login");
    } else {
      this.currentUser = this.userService.getCurrentUser();
    }
  }

  public mounted() {
    const _this = this;
    const divOutSide = document.getElementById("outSide");
    if (divOutSide) {
      divOutSide.addEventListener("click", function (e) {
        _this.isShowMenuContent = false;
      });
    }
  }

  public logout() {
    this.userService.logout();
    this.popupSuccess("Đăng xuất thành công!");
    this.$router.push("/auth/login");
  }

  public checkRoleUser(rolesPage: Array<string>): boolean {
    if (this.currentUser && this.currentUser.roles && this.currentUser.roles.length) {
      for (let i = 0; i < this.currentUser.roles.length; i++) {
        if (this.currentUser.roles[i] === "Admin system") {
          return true;
        }
        if (rolesPage.includes(this.currentUser.roles[i])) {
          return true;
        }
      }
    }
    return false;
  }

  public isActiveLink(uri: string): boolean {
    return this.$router.currentRoute.path === uri;
  }

  public getComponentName(name: string) {
    this.navigationName = name;
  }
}

declare interface INavigation {
  nameKey: string,
  url: string,
  icon: string,
  "meta": {
    allowAccess: Array<string>,
    page: string
  }
}
