import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import DefaultComponent from "@/presentation/container/default/DefaultContainer.vue";
import Login from "@/presentation/views/auth/login/Login.vue";
import Register from "@/presentation/views/auth/register/Register.vue";
import SchoolPage from "@/presentation/views/admin/school/SchoolPage.vue";
import SchoolListPage from "@/presentation/views/admin/school/list/SchoolListPage.vue";
import ClassroomListPage from "@/presentation/views/admin/classroom/list/ClassroomListPage.vue";
import ClassroomPage from "@/presentation/views/admin/classroom/ClassroomPage.vue";
import TestListPage from "@/presentation/views/admin/test/list/TestListPage.vue";
import TestPage from "@/presentation/views/admin/test/TestPage.vue";
import StudentPage from "@/presentation/views/admin/student/StudentPage.vue";
import StudentListPage from "@/presentation/views/admin/student/list/StudentListPage.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/auth",
    redirect: "/auth/login",
    name: "Auth",
    component: {
      render(c: any) {
        return c("router-view");
      }
    },
    children: [
      {
        path: "login",
        name: "LoginPage",
        component: Login,
        meta: {
          page: "auth"
        }
      },
      {
        path: "register",
        name: "RegisterPage",
        component: Register,
        meta: {
          page: "auth"
        }
      }
    ]
  },
  {
    path: "/",
    name: "Default",
    component: DefaultComponent,
    children: [
      {
        path: "/admin",
        redirect: "/admin/school",
        name: "Auth",
        component: {
          render(c: any) {
            return c("router-view");
          }
        },
        children: [
          {
            path: "school",
            name: "SchoolListPage",
            component: SchoolListPage,
            meta: {
              page: "auth"
            }
          },
          {
            path: "school-new",
            name: "SchoolPage",
            component: SchoolPage,
            meta: {
              page: "auth"
            }
          },
          {
            path: "classroom",
            name: "ClassroomListPage",
            component: ClassroomListPage,
            meta: {
              page: "auth"
            }
          },
          {
            path: "classroom-new",
            name: "ClassroomPage",
            component: ClassroomPage,
            meta: {
              page: "auth"
            }
          },
          {
            path: "test",
            name: "TestListPage",
            component: TestListPage,
            meta: {
              page: "auth"
            }
          },
          {
            path: "test-edit/:id",
            name: "TestEditPage",
            component: TestPage,
            meta: {
              page: "auth"
            }
          },
          {
            path: "test-view/:id",
            name: "TestViewPage",
            component: TestPage,
            meta: {
              page: "auth"
            }
          },
          {
            path: "test-new",
            name: "TestPage",
            component: TestPage,
            meta: {
              page: "auth"
            }
          },
          {
            path: "student",
            name: "StudentListPage",
            component: StudentListPage,
            meta: {
              page: "auth"
            }
          },
          {
            path: "student-new",
            name: "StudentPage",
            component: StudentPage,
            meta: {
              page: "auth"
            }
          }
        ]
      }
    ]
  },
  {
    path: "/about",
    name: "About",
    component: () => import(/* webpackChunkName: "about" */ "../views/about/About.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
