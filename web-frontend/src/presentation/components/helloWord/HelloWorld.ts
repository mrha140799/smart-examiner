import BaseComponent from "@/presentation/views/BaseComponent";
import { Component } from "vue-property-decorator";

@Component({
  props: {
    msg: String
  }
})
export default class HelloWorld extends BaseComponent {
  public created() {
    this.popupSuccess("load success");
  }
}
