import BasePage from "@/presentation/views/BasePage";
import { Component } from "vue-property-decorator";
import HelloWorld from "../../components/helloWord/HelloWorld.vue"; // @ is an alias to /src

@Component({
  components: {
    HelloWorld
  }
})
export default class Home extends BasePage {

}
