import BasePage from "@/presentation/views/BasePage";
import { Component } from "vue-property-decorator";
import StudentListInteractor, { IStudentListOutputPort } from "@/domain/usecases/StudentListInteractor";
import Student from "@/model/Student";
import moment from "moment";
import ValidatorUtil from "@/commons/ValidatorUtil";
import Classroom from "@/model/Classroom";

@Component
export default class StudentListPage extends BasePage implements IStudentListOutputPort {
  public isLoadingForm: boolean = false;
  public students: Array<Student> = [];
  public classrooms: Array<Classroom> = [];
  private studentListUC: StudentListInteractor = StudentListInteractor.create(this);
  private validator: ValidatorUtil = ValidatorUtil.instance;
  public studentModel: Student = new Student();
  public studentActionName: string = "create";
  public createStudentDialogVisible: boolean = false;
  public validClassroom: boolean = true;
  private classroomIdChoice: number | null = null;
  private classroomIdChoice2Create: number | null = null;
  public disableFeature: any = {
    disabledDate(time: Date) {
      return time.getTime() > Date.now();
    }
  };

  public studentValidate: any = {
    fullName: [this.validator.required, this.validator.minLength(3)],
    dateOfBirth: [this.validator.required]
  };

  created() {
    this.isLoadingForm = true;
    this.studentListUC.getDataInit();
  }

  studentAction(type: string, scope: any) {
    if (type === "createMany") {
      this.$router.push("/admin/student-new");
    } else if (type === "createOne") {
      this.studentActionName = "create";
      this.createStudentDialogVisible = true;
    } else if (type === "edit") {
      this.studentActionName = "update";
      this.classroomIdChoice2Create = scope.row.classroom.id;
      this.studentModel.id = scope.row.id;
      this.studentModel.fullName = scope.row.fullName;
      this.studentModel.dateOfBirth = scope.row.dateOfBirth;
      this.createStudentDialogVisible = true;
    } else if (type === "delete") {
      this.$confirm("Bạn có xóa học sinh và những dữ liệu liên quan không?", {
        confirmButtonText: "Đồng ý",
        cancelButtonText: "Hủy bỏ",
        type: "info"
      }).then(() => {
        this.isLoadingForm = true;
        this.studentListUC.deleteById(scope.row.id);
      }).catch(() => {
      });
    }
  }

  public filterByClassroom() {
    this.isLoadingForm = true;
    this.studentListUC.getStudentsByClassId(this.classroomIdChoice);
  }

  public onCreateOneStudentClicked() {
    this.validClassroom = this.classroomIdChoice2Create !== null;
    (this.$refs.formCreate as any).validate((valid: boolean) => {
      if (valid && this.validClassroom) {
        if (this.studentActionName === "create") {
          this.isLoadingForm = true;
          this.studentListUC.createOne(this.studentModel, (this.classroomIdChoice2Create as number));
        } else {
          this.isLoadingForm = true;
          this.studentListUC.updateOne(this.studentModel, (this.classroomIdChoice2Create as number));
        }
      }
    });
  }

  getDataInitError(): any {
    this.isLoadingForm = false;
  }

  getDataInitSuccessfully(students: Array<Student>, classrooms: Array<Classroom>): any {
    this.isLoadingForm = false;
    this.students = students;
    this.classrooms = classrooms;
  }

  createOrUpdateOneError(): any {
    this.isLoadingForm = false;
    this.popupError("Lỗi, thêm mới thất bại!");
  }

  createOrUpdateOneSuccessfully(): any {
    if (this.studentActionName === "update") {
      this.popupSuccess("Cập nhật thành công!");
    } else {
      this.popupSuccess("Thêm mới thành công!");
    }
    this.createStudentDialogVisible = false;
    this.classroomIdChoice = null;
    this.studentModel = new Student();
    this.classroomIdChoice2Create = null;
    this.studentListUC.getDataInit();
  }

  getStudentsByClassIdSuccessfully(students: Student[]) {
    this.isLoadingForm = false;
    this.students = students;
  }

  getStudentsByClassIdError() {
    this.isLoadingForm = false;
    this.students = [];
  }

  deleteByIdSuccessfully(): void {
    this.popupSuccess("Xóa thông tin thành công!");
    this.isLoadingForm = true;
    if (this.classroomIdChoice) {
      this.studentListUC.getStudentsByClassId(this.classroomIdChoice);
    } else {
      this.studentListUC.getDataInit();
    }
  }

  deleteByIdError(): void {
    this.isLoadingForm = false;
    this.popupError("Xóa thông tin thất bại!");
  }

  public formatDate(date: Date) {
    return moment(date).format("DD/MM/YYYY");
  }
}
