import BasePage from "@/presentation/views/BasePage";
import Component from "vue-class-component";
import ValidatorUtil from "@/commons/ValidatorUtil";
import Students from "@/model/Students";
import Classroom from "@/model/Classroom";
import * as XLSX from "xlsx";
import StudentInteractor, { IStudentOutputPort } from "@/domain/usecases/StudentInteractor";
import Student from "@/model/Student";
import moment from "moment";

@Component
export default class StudentPage extends BasePage implements IStudentOutputPort {
  public pageName: string = "Thêm Mới Thí Sinh";
  public pageType: string = "new";
  public studentIndex: number = -1;
  public studentActionName: string = "create";
  private validator: ValidatorUtil = ValidatorUtil.instance;
  public studentsModel: Students = new Students();
  public classrooms: Array<Classroom> = [];
  public isLoading: boolean = false;
  public upLoadFileDialogVisible: boolean = false;
  public createStudentDialogVisible: boolean = false;
  public fileUpload: any = null;
  public studentsDataFile: Array<any> = [];
  public studentsInvalid: any = {};
  public studentModel: Student = new Student();
  public disableFeature: any = {
    disabledDate(time: Date) {
      return time.getTime() > Date.now();
    }
  };

  public files: Array<any> = [];

  public studentsValidate: any = {
    classroomId: [this.validator.required]
  };

  public studentValidate: any = {
    fullName: [this.validator.required, this.validator.minLength(3)],
    dateOfBirth: [this.validator.required]
  };

  public studentUC: StudentInteractor = StudentInteractor.create(this);

  created() {
    this.studentUC.getAllMyClasses();
  }

  checkValidListStudents() {
    this.studentsModel.students.map((e, index) => {
      this.studentsInvalid["student_" + index] = !e.fullName || !e.dateOfBirth;
    });
  }

  studentAction(type: string, scope: any) {
    if (type === "new") {
      this.$router.push("/admin/student-new");
    } else if (type === "edit") {
      this.studentIndex = scope.$index;
      this.studentModel = scope.row;
      this.studentActionName = "update";
      this.createStudentDialogVisible = true;
    } else if (type === "delete") {
      this.studentsModel.students.splice(scope.$index, 1);
      delete this.studentsInvalid[this.studentsModel.students.length - 1];
      this.checkValidListStudents();
    }
  }

  onSubmitStudentsClicked() {
    const self = this;
    (this.$refs.studentsForm as any).validate((valid: boolean) => {
      if (valid) {
        const invalidStudents = Object.keys(this.studentsInvalid).filter((e: any, index: number) => {
          return self.studentsInvalid[e];
        });
        if (invalidStudents.length) {
          this.popupError("Lỗi, một số thí sinh không hợp lên");
        } else {
          this.isLoading = true;
          this.studentUC.createMany(this.studentsModel);
        }
      }
    });
  }

  downloadFileUpload() {
    this.isLoading = true;
    this.studentUC.downloadCreatedFile();
  }

  public onUploadChange(file: any) {
    const fileReader: FileReader = new FileReader();
    fileReader.readAsArrayBuffer(file.raw);
    fileReader.onload = (e: any) => {
      const result = e.target.result;
      const wb = XLSX.read(result, { type: "buffer" });
      const wsName = wb.SheetNames[0];
      const ws = wb.Sheets[wsName];
      this.studentsDataFile = XLSX.utils.sheet_to_json(ws);
    };
    this.files.push(file);
  }

  public onRemoveFile() {
    this.files = [];
    this.studentsDataFile = [];
  }

  public formatDate(date: Date) {
    return moment(date).format("DD/MM/YYYY");
  }

  public onCreateOneStudentClicked() {
    (this.$refs.formCreate as any).validate((valid: boolean) => {
      if (valid) {
        if (this.studentActionName === "create") {
          this.studentsModel.students.push(this.studentModel);
        } else {
          this.studentsModel.students[this.studentIndex] = this.studentModel;
          this.studentActionName = "create";
          (this.$refs.tableStudents as any).doLayout();
        }
        this.studentModel = new Student();
        this.createStudentDialogVisible = false;
      }
      this.checkValidListStudents();
    });
  }

  public onRefreshModelClicked() {
    this.$confirm("Bạn có muốn làm mới lại từ đầu?", {
      confirmButtonText: "Đồng ý",
      cancelButtonText: "Hủy bỏ",
      type: "info"
    }).then(() => {
      this.studentsModel = new Students();
    }).catch(() => {
    });
  }

  public onUploadFileClicked() {
    this.studentsModel.students = [];
    this.studentsInvalid = {};
    this.studentsDataFile.forEach((e: any, index: number) => {
      const student: Student = new Student();
      if (!e["Họ Và Tên"] || typeof e["Họ Và Tên"] !== "string" || e["Họ Và Tên"].length < 3) {
        this.studentsInvalid["student_" + index] = true;
      }
      try {
        const date = new Date(e["Ngày Sinh (mm/dd/yyyy)"]);
        if (date.getFullYear() >= (new Date()).getFullYear()) {
          this.studentsInvalid["student_" + index] = true;
        }
      } catch (e) {
        this.studentsInvalid["student_" + index] = true;
      }
      student.fullName = e["Họ Và Tên"] ? e["Họ Và Tên"] : "";
      student.dateOfBirth = e["Ngày Sinh (mm/dd/yyyy)"] ? new Date(e["Ngày Sinh (mm/dd/yyyy)"]) : new Date();
      this.studentsModel.students.push(student);
    });
    console.log(this.studentsInvalid);
    this.checkValidListStudents();
    this.upLoadFileDialogVisible = false;
  }

  getAllMyClassesError(): any {
  }

  getAllMyClassesSuccessfully(classrooms: Array<Classroom>): any {
    this.isLoading = false;
    this.classrooms = classrooms;
  }

  createManySuccessfully() {
    this.isLoading = false;
    this.popupSuccess("Tạo thành công!");
    this.$router.push("/admin/student");
  }

  createManyError() {
    this.isLoading = false;
    this.popupError("Lỗi, dữ liệu không chính xác!");
  }

  downloadCreatedFileError(): any {
    this.isLoading = false;
    this.popupError("Lỗi, tải file không thành công!");
  }

  downloadCreatedFileSuccessfully(file: any): any {
    const byteCharacters = atob(file);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
    const url: string = window.URL.createObjectURL(blob);
    const linkElement: any = document.createElement("a");
    linkElement.href = url;
    linkElement.setAttribute("download", "Phieu-Tao-Hoc-Vien-Smart-Exammier.xlsx");
    document.body.appendChild(linkElement);
    linkElement.click();
    this.isLoading = false;
  }
}
