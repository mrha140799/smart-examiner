import BasePage from "@/presentation/views/BasePage";
import Component from "vue-class-component";
import ValidatorUtil from "@/commons/ValidatorUtil";
import School from "@/model/School";
import TestInteractor, { ITestOutputPort } from "@/domain/usecases/TestInteractor";
import Test from "@/model/Test";
import * as XLSX from "xlsx";

@Component
export default class TestPage extends BasePage implements ITestOutputPort {
  public pageName: string = "Thêm Mới Bài Kiểm Tra";
  public pageType: string = "new";
  public readonly correctAnswers: Array<string> = ["A", "B", "C", "D"];
  public invalidCodeMessage: string = "* Độ dài 4 kí tự số";
  private validator: ValidatorUtil = ValidatorUtil.instance;
  public testModel: Test = new Test();
  public isLoading: boolean = false;
  public validTestCode: boolean = true;
  public upLoadFileDialogVisible: boolean = false;
  public dataTableAnswers: Array<any> = [];
  public invalidQuestions: Array<any> = [];
  public files: Array<any> = [];
  public fileUpload: any = null;
  public answers: any = {};
  public testsDataFile: Array<any> = [];

  public testValidation = {
    subject: [this.validator.required, this.validator.minLength(2)],
    code: [this.validator.required, this.validator.minLength(4), this.validator.maxLength(4)],
    testDay: [this.validator.required]
  };

  public testUc: TestInteractor = TestInteractor.create(this);

  testAction(type: string) {
    if (type === "back") {
      this.$router.push("/admin/test");
    }
  }

  public checkValidTestCode() {
    const regex = /^[0-9]{4}$/g;
    if (regex.test(this.testModel.code)) {
      this.isLoading = true;
      this.testUc.isValidTestCode(this.testModel.code, this.testModel.id);
    } else {
      this.validTestCode = false;
      this.invalidCodeMessage = "* Chuỗi 0-9 và độ dài 4 kí tự";
    }
  }

  created() {
    this.generateDefaultValue();
    if (this.$router.currentRoute.name === "TestEditPage") {
      this.pageType = "edit";
      this.pageName = "Chỉnh Sửa Đề Thi";
    } else if (this.$router.currentRoute.name === "TestViewPage") {
      this.pageType = "view";
      this.pageName = "Xem Chi Tiết Đề Thi";
    }
    if (this.pageType === "edit" || this.pageType === "view") {
      this.isLoading = true;
      this.testUc.getById((parseInt(this.$router.currentRoute.params.id)));
    }
  }

  onCreateTestClicked() {
    this.invalidQuestions = this.invalidQuestionIndexes().filter(e => {
      return e !== undefined;
    });
    if (this.invalidQuestions.length) {
      this.popupError("Lỗi, chưa thêm đủ số lượng câu hỏi!");
    }
    (this.$refs.testForm as any).validate((valid: boolean) => {
      if (valid && !this.invalidQuestions.length && this.validTestCode) {
        const answersCorrect = this.dataTableAnswers.map(e => {
          return e.answer;
        }).toString();
        this.testModel.answers = answersCorrect.replaceAll(",", "/");
        this.isLoading = true;
        if (this.pageType === "edit") {
          this.testUc.updateTest(this.testModel);
        } else {
          this.testUc.createOne(this.testModel);
        }
      }
    });
  }

  private convertToListAnswers(answerTest: string) {
    const answers = answerTest.split("/");
    const answer = {};
    const invalidQuestions = {};
    for (let i = 0; i < 50; i++) {
      (answer as any)["question_" + i] = answers[i];
      this.dataTableAnswers[i].answer = answers[i];
    }
    this.answers = answer;
  }

  private invalidQuestionIndexes(): Array<any> {
    return this.dataTableAnswers.map((e, index) => {
      if (!e.answer || e.answer === "") {
        return (e.index);
      }
    });
  }

  public isNotConstrainIndex(array: Array<any>, index: number): boolean {
    return !array.includes(index);
  }

  onUploadFileClicked() {
    this.testsDataFile.forEach((e, index) => {
      if (e["Câu Trả Lời Đúng"] && this.correctAnswers.includes(e["Câu Trả Lời Đúng"])) {
        (this.dataTableAnswers[index] as any).answer = e["Câu Trả Lời Đúng"];
      } else {
        (this.dataTableAnswers[index] as any).answer = "";
      }
      if (e["Câu Trả Lời Đúng_1"] && this.correctAnswers.includes(e["Câu Trả Lời Đúng_1"])) {
        (this.dataTableAnswers[index + 25] as any).answer = e["Câu Trả Lời Đúng_1"];
      } else {
        (this.dataTableAnswers[index + 25] as any).answer = "";
      }
    });
    this.invalidQuestions = this.invalidQuestionIndexes().filter(e => {
      return e !== undefined;
    });
    this.upLoadFileDialogVisible = false;
  }

  downloadFileUpload() {
    this.isLoading = true;
    this.testUc.downloadCreatedFile();
  }

  public onRemoveFile() {
    this.files = [];
    this.testsDataFile = [];
  }

  public onUploadChange(file: any) {
    const fileReader: FileReader = new FileReader();
    fileReader.readAsArrayBuffer(file.raw);
    fileReader.onload = (e: any) => {
      const result = e.target.result;
      const wb = XLSX.read(result, { type: "buffer" });
      const wsName = wb.SheetNames[0];
      const ws = wb.Sheets[wsName];
      this.testsDataFile = XLSX.utils.sheet_to_json(ws);
      console.log(this.testsDataFile);
    };
    this.files.push(file);
  }

  private generateDefaultValue() {
    for (let i = 0; i < 50; i++) {
      this.dataTableAnswers.push({
        index: i,
        label: i + 1,
        answer: ""
      });
      (this.answers as any)["question_" + i] = "";
    }
  }

  public getCurrentRow(row: any) {
    (this.answers as any)["question_" + row.index] = "A";
  }

  createOneError(): any {
    this.isLoading = false;
    this.popupSuccess("Thêm mới thất bại!");
  }

  createOneSuccessfully(): any {
    this.isLoading = false;
    this.popupSuccess("Thêm mới thành công!");
    this.$router.push("/admin/test");
  }

  isValidTestCode() {
    this.isLoading = false;
    this.validTestCode = true;
  }

  invalidTestCode() {
    this.isLoading = false;
    this.validTestCode = false;
    this.invalidCodeMessage = "* Đã tồn tại số báo danh";
  }

  getByIdSuccessfully(test: Test) {
    this.isLoading = false;
    this.testModel = test;
    this.convertToListAnswers(test.answers);
  }

  getByIdError() {
    this.isLoading = false;
    throw new Error("Method not implemented.");
  }

  updateTestSuccessfully() {
    this.isLoading = false;
    this.popupSuccess("Chỉnh sửa thành công!");
    this.$router.push("/admin/test");
  }

  updateTestError() {
    this.isLoading = false;
    this.popupError("Lỗi, chỉnh sửa thất bại!");
  }

  downloadCreatedFileError(): any {
    this.isLoading = false;
    this.popupError("Lỗi, tải file không thành công!");
  }

  downloadCreatedFileSuccessfully(file: any): any {
    const byteCharacters = atob(file);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
    const url: string = window.URL.createObjectURL(blob);
    const linkElement: any = document.createElement("a");
    linkElement.href = url;
    linkElement.setAttribute("download", "Phieu-Tao-De-Thi-Smart-Exammier.xlsx");
    document.body.appendChild(linkElement);
    linkElement.click();
    this.isLoading = false;
  }
}
