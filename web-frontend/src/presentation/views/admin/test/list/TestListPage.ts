import BasePage from "@/presentation/views/BasePage";
import { Component } from "vue-property-decorator";
import TestListInteractor, { ITestListOutputPort } from "@/domain/usecases/TestListInteractor";
import Test from "@/model/Test";
import moment from "moment";

@Component
export default class TestListPage extends BasePage implements ITestListOutputPort {
  public isLoadingForm: boolean = false;
  public dataTableTests: Array<any> = [];
  private testListUc: TestListInteractor = TestListInteractor.create(this);

  created() {
    this.getAllTests();
  }

  private getAllTests() {
    this.isLoadingForm = true;
    this.testListUc.getAllMyTests();
  }

  testAction(type: string, id?: string) {
    if (type === "new") {
      this.$router.push("/admin/test-new");
    } else if (type === "edit") {
      this.$router.push("/admin/test-edit/" + id);
    } else if (type === "view") {
      this.$router.push("/admin/test-view/" + id);
    } else if (type === "delete") {
      this.$confirm("Bạn có xóa lớp và những dữ liệu liên quan không?", {
        confirmButtonText: "Đồng ý",
        cancelButtonText: "Hủy bỏ",
        type: "info"
      }).then(() => {
        this.isLoadingForm = true;
        this.testListUc.deleteById(id);
      }).catch(() => {
      });
    } else if (type === "downloadFormFile") {
      this.isLoadingForm = true;
      this.testListUc.downloadFormAnswerFile();
    }
  }

  private formatToDataTable(elements: Array<any>): Array<any> {
    return elements.map((element, index) => {
      element.index = index + 1;
      return element;
    });
  }

  public formatDate(date: Date): string {
    return moment(date).format("DD/MM/YYYY");
  }

  getAllMyTestsError(): any {
    this.isLoadingForm = false;
    this.popupError("Lỗi, không tìm thấy thông tin!");
  }

  getAllMyTestsSuccessfully(schools: Array<Test>): any {
    this.isLoadingForm = false;
    this.dataTableTests = this.formatToDataTable(schools);
  }

  deleteByIdSuccessfully(): void {
    this.isLoadingForm = false;
    this.popupSuccess("Xóa thông tin thành công!");
    this.getAllTests();
  }

  deleteByIdError(): void {
    this.isLoadingForm = false;
    this.popupError("Xóa thông tin thất bại!");
  }

  downloadFormAnswerFileSuccessfully(fileContent: string): void {
    this.isLoadingForm = false;
    const element = document.createElement("a");
    const blob = this.dataURItoBlob(fileContent);
    const url = URL.createObjectURL(blob);
    element.setAttribute("href", url);
    element.setAttribute("download", "Phieu-Tra-Loi-Trac-Nghiem.pdf");
    element.style.display = "none";
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

  downloadFormAnswerFileError(): void {
    throw new Error("Method not implemented.");
  }

  dataURItoBlob(dataURI: string) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: "application/pdf" });
    return blob;
  }
}
