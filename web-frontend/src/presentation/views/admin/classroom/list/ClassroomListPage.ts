import BasePage from "@/presentation/views/BasePage";
import { Component } from "vue-property-decorator";
import ValidatorUtil from "@/commons/ValidatorUtil";
import School from "@/model/School";
import Classroom from "@/model/Classroom";
import ClassroomListInteractor, { IClassroomListOutputPort } from "@/domain/usecases/ClassroomListInteractor";
import Students from "@/model/Students";

@Component
export default class ClassroomListPage extends BasePage implements IClassroomListOutputPort {
  public isLoadingForm: boolean = false;
  public classroomDialogVisible: boolean = false;
  public dataTableClassrooms: Array<any> = [];
  public schools: Array<School> = [];
  public classroomActionName: string = "Thêm Mới Phòng Học";
  public classroomActionType: string = "new";
  private validator: ValidatorUtil = ValidatorUtil.instance;
  public classroomModel: Classroom = new Classroom();
  public isLoading: boolean = false;
  private classroomListUC: ClassroomListInteractor = ClassroomListInteractor.create(this);
  public classroomValidate: any = {
    name: [this.validator.required]
  };

  created() {
    this.getAllRooms();
  }

  private getAllRooms() {
    this.isLoading = true;
    this.classroomListUC.getAllMyClassrooms();
  }

  classroomAction(type: string, id?: string) {
    if (type === "new") {
      this.$router.push("/admin/classroom-new");
    } else if (type === "edit") {
      this.classroomActionType = "edit";
      this.isLoading = true;
      this.classroomListUC.getOneById(id);
      this.classroomListUC.getAllMySchool();
    } else if (type === "view") {
      this.$router.push("/admin/classroom-view/" + id);
    } else if (type === "delete") {
      this.$confirm("Bạn có xóa lớp và những dữ liệu liên quan không?", {
        confirmButtonText: "Đồng ý",
        cancelButtonText: "Hủy bỏ",
        type: "info"
      }).then(() => {
        this.isLoading = true;
        this.classroomListUC.deleteById(id);
      }).catch(() => {
      });
    }
  }

  private formatToDataTable(elements: Array<any>): Array<any> {
    return elements.map((element, index) => {
      element.index = index + 1;
      return element;
    });
  }

  public onOpenFromClicked() {
    this.classroomDialogVisible = true;
    this.isLoadingForm = true;
    this.classroomListUC.getAllMySchool();
  }

  public onCreateClassroomClicked() {
    (this.$refs.classroomForm as any).validate((valid: boolean) => {
      if (valid) {
        this.isLoadingForm = true;
        if (this.classroomActionType === "new") {
          this.classroomListUC.createClassroom(this.classroomModel);
        } else {
          this.classroomListUC.updateOne(this.classroomModel);
        }
      }
    });
  }

  public onCancelClicked() {
    this.classroomActionType = "new";
    this.classroomModel = new Classroom();
    this.classroomDialogVisible = false;
  }

  getAllMyClassroomsError(): any {
    this.isLoading = false;
    this.popupError("Lỗi, không tìm thấy thông tin!");
  }

  getAllMyClassroomsSuccessfully(classrooms: Array<Classroom>): any {
    this.dataTableClassrooms = this.formatToDataTable(classrooms);
    this.isLoading = false;
  }

  getAllMySchoolError(): any {
    this.isLoadingForm = false;
    this.popupError("Lỗi, không tìm thấy thông tin!");
  }

  getAllMySchoolSuccessfully(schools: Array<School>): any {
    this.isLoadingForm = false;
    this.schools = schools;
  }

  createOneError(): any {
    this.isLoadingForm = false;
    this.popupError("Lỗi, không thể thêm mới thông tin!");
  }

  createOneSuccessfully(): any {
    this.isLoadingForm = false;
    this.classroomModel = new Classroom();
    this.classroomDialogVisible = false;
    this.popupSuccess("Tạo mới thành công!");
    this.getAllRooms();
  }

  getOneByIdError(): void {
    this.isLoading = false;
    this.popupError("Lỗi, không tìm thấy thông tin!");
  }

  getOneByIdSuccessfully(classroom: Classroom): void {
    this.classroomModel = classroom;
    this.isLoading = false;
    this.classroomDialogVisible = true;
  }

  updateOneError(): void {
    this.isLoadingForm = false;
    this.popupError("Lỗi, không thể sửa thông tin!");
  }

  updateOneSuccessfully(): void {
    this.isLoadingForm = false;
    this.classroomModel = new Classroom();
    this.classroomDialogVisible = false;
    this.classroomActionType = "new";
    this.popupSuccess("Sửa thông tin thành công!");
    this.getAllRooms();
  }

  deleteByIdError(): void {
    this.isLoadingForm = false;
    this.popupError("Xóa thông tin thất bại!");
  }

  deleteByIdSuccessfully(): void {
    this.isLoadingForm = false;
    this.popupSuccess("Xóa thông tin thành công!");
    this.getAllRooms();
  }
}
