import BasePage from "@/presentation/views/BasePage";
import Component from "vue-class-component";

@Component
export default class ClassroomPage extends BasePage {
  public componentName: String = "Quản lý trường";

  mounted() {
    this.$emit("componentName", this.componentName);
  }
}
