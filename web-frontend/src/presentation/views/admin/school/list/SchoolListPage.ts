import BasePage from "@/presentation/views/BasePage";
import { Component } from "vue-property-decorator";
import SchoolListInteractor, { ISchoolListOutputPort } from "@/domain/usecases/SchoolListInteractor";
import School from "@/model/School";
import ValidatorUtil from "@/commons/ValidatorUtil";
import Classroom from "@/model/Classroom";

@Component
export default class SchoolListPage extends BasePage implements ISchoolListOutputPort {
  public isLoadingForm: boolean = false;
  public dataTableSchools: Array<any> = [];
  private schoolListUc: SchoolListInteractor = SchoolListInteractor.create(this);
  public schoolActionName: string = "Thêm Mới Trường học";
  public schoolActionType: string = "new";
  private validator: ValidatorUtil = ValidatorUtil.instance;
  public schoolModel: School = new School();
  public isLoading: boolean = false;
  public schoolDialogVisible: boolean = false;
  public schoolValidate: any = {
    name: [this.validator.required],
    address: [this.validator.required]
  };

  created() {
    this.isLoadingForm = false;
    this.schoolListUc.getAllMySchools();
  }

  public onCancelClicked() {
    this.schoolActionType = "new";
    this.schoolModel = new School();
    this.schoolDialogVisible = false;
  }

  public onCreateClassroomClicked() {
    (this.$refs.schoolForm as any).validate((valid: boolean) => {
      if (valid) {
        this.isLoadingForm = true;
        if (this.schoolActionType === "new") {
          this.schoolListUc.createSchool(this.schoolModel);
        } else {
          this.schoolListUc.updateSchool(this.schoolModel);
        }
      }
    });
  }

  schoolAction(type: string, id?: string) {
    if (type === "new") {
      this.schoolActionType = "new";
      this.schoolDialogVisible = true;
    } else if (type === "edit") {
      this.schoolActionType = "edit";
      this.schoolActionName = "Sửa Thông Tin Trường Học";
      this.schoolListUc.getOneById(id);
    } else if (type === "delete") {
      this.$confirm("Bạn có xóa trường học và những dữ liệu liên quan không?", {
        confirmButtonText: "Đồng ý",
        cancelButtonText: "Hủy bỏ",
        type: "info"
      }).then(() => {
        this.isLoading = true;
        this.schoolListUc.deleteById(id);
      }).catch(() => {
      });
    }
  }

  private formatToDataTable(elements: Array<any>): Array<any> {
    return elements.map((element, index) => {
      element.index = index + 1;
      return element;
    });
  }

  getAllMySchoolsError(): any {
    this.isLoadingForm = false;
    this.popupError("Lỗi, không tìm thấy thông tin trường học!");
  }

  getAllMySchoolsSuccessfully(schools: Array<School>): any {
    this.isLoadingForm = false;
    this.dataTableSchools = this.formatToDataTable(schools);
  }

  createSchoolError(): any {
    this.isLoadingForm = false;
    this.popupError("Lỗi, không thể thêm mới thông tin!");
  }

  createSchoolSuccessfully(): any {
    this.isLoading = false;
    this.schoolModel = new School();
    this.schoolDialogVisible = false;
    this.popupSuccess("Tạo mới thành công!");
    this.schoolListUc.getAllMySchools();
  }

  getOneByIdError(): any {
    this.isLoading = false;
    this.popupError("Lỗi, không tìm thấy thông tin!");
  }

  getOneByIdSuccessfully(school: School): any {
    this.schoolModel = school;
    this.isLoading = false;
    this.schoolDialogVisible = true;
  }

  updateSchoolError(): any {
    this.isLoadingForm = false;
    this.popupError("Lỗi, không thể sửa thông tin!");
  }

  updateSchoolSuccessfully(): any {
    this.isLoading = false;
    this.schoolModel = new School();
    this.schoolDialogVisible = false;
    this.schoolActionType = "new";
    this.popupSuccess("Sửa thông tin thành công!");
    this.schoolListUc.getAllMySchools();
  }

  deleteByIdSuccessfully() {
    this.isLoadingForm = false;
    this.popupSuccess("Xóa thông tin thành công!");
    this.schoolListUc.getAllMySchools();
  }

  deleteByIdError() {
    this.isLoadingForm = false;
    this.popupError("Xóa thông tin thất bại!");
  }
}
