import BasePage from "@/presentation/views/BasePage";
import Component from "vue-class-component";
import ValidatorUtil from "@/commons/ValidatorUtil";
import School from "@/model/School";
import SchoolInteractor, { ISchoolOutputPort } from "@/domain/usecases/SchoolInteractor";

@Component
export default class SchoolPage extends BasePage implements ISchoolOutputPort {
  public pageName: string = "Thêm Mới Trường Học";
  public pageType: string = "new";
  private validator: ValidatorUtil = ValidatorUtil.instance;
  public schoolModel: School = new School();
  public isLoading: boolean = false;
  public schoolValidate: any = {
    name: [this.validator.required],
    address: [this.validator.required]
  };

  public schoolUc: SchoolInteractor = SchoolInteractor.create(this);

  schoolAction(type: string) {
    if (type === "back") {
      this.$router.push("/admin/school");
    }
  }

  created() {
    if (this.$router.currentRoute.name === "CsvcEditPage") {
      this.pageType = "edit";
      this.pageName = "Chỉnh sửa cơ sở vật chất";
    } else if (this.$router.currentRoute.name === "CsvcViewPage") {
      this.pageType = "view";
      this.pageName = "Xem chi tiết cơ sở vật chất";
    }
    if (this.pageType === "edit" || this.pageType === "view") {
      this.schoolUc.findSchoolById(this.$router.currentRoute.params.id);
    }
  }

  onSubmitCsvc() {
    (this.$refs.csvcForm as any).validate((valid: boolean) => {
      if (valid) {
        this.isLoading = true;
        if (this.pageType === "new") {
          this.schoolUc.createSchool(this.schoolModel);
        } else if (this.pageType === "edit") {
          this.schoolUc.updateSchool(this.schoolModel);
        }
      }
    });
  }

  createSchoolSuccessfully() {
    this.isLoading = false;
    this.popupSuccess("Thạo mới trường thành công!");
    this.$router.push("/admin/school");
  }

  createSchoolError() {
    this.isLoading = false;
    this.popupError("Lỗi, Tạo mới thất bại!");
  }
}
