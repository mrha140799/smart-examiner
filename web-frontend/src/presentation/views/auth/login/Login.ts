import BasePage from "@/presentation/views/BasePage";
import Account, { IAccount } from "@/model/Account";
import { Component } from "vue-property-decorator";
import LoginInteractor, { ILoginOutputPort } from "@/domain/usecases/LoginInteractor";
import ValidatorUtil from "@/commons/ValidatorUtil";
import DIContainer from "@/core/DIContainer";
import IUserReps from "@/data/repository/IUserReps";

@Component
export default class Login extends BasePage implements ILoginOutputPort {
  private user: IAccount = new Account();
  private validator: ValidatorUtil = ValidatorUtil.instance
  private isLoading: boolean = false;
  private authUC: LoginInteractor;
  private userService: IUserReps;
  private userValid: any = {
    email: [this.validator.required, this.validator.minLength(5)],
    password: [this.validator.required, this.validator.minLength(6)]
  }

  public constructor() {
    super();
    this.authUC = LoginInteractor.create(this);
    this.userService = DIContainer.instance.get(IUserReps);
  }

  public onClickToLogin() {
    this.isLoading = true;
    this.authUC.login(this.user);
  }

  public created() {
    if (this.userService.isLoggedOn()) {
      this.userService.logout();
    }
  }

  loginComplete(res: any): void {
    this.isLoading = false;
    this.popupSuccess("Login successfully");
    this.$router.push("/");
  }

  loginError(): void {
    this.isLoading = false;
    this.popupError("Fail to login");
  }
}
