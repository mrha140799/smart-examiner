import { Component } from "vue-property-decorator";
import BasePage from "@/presentation/views/BasePage";
import User, { IUser } from "@/model/User";
import ValidatorUtil from "@/commons/ValidatorUtil";
import RegisterInteractor, { IRegisterOutputPort } from "@/domain/usecases/RegisterInteractor";

@Component
export default class Register extends BasePage implements IRegisterOutputPort {
  private user: IUser = new User();
  private validator: ValidatorUtil = ValidatorUtil.instance;
  private registerUC: RegisterInteractor = RegisterInteractor.create(this);
  private confirmPassword: string = "";
  private userValid: any = {
    firstName: [this.validator.required, this.validator.minLength(2)],
    lastName: [this.validator.required, this.validator.minLength(2)],
    email: [this.validator.required],
    phoneNumber: [this.validator.required],
    confirmPassword: [this.validator.required],
    password: [this.validator.required],
    gender: [this.validator.required]
  };

  public isLoading: boolean = false;
  public invalidPassword: boolean = false;

  public onClickToRegister() {
    this.onChangeValidateConfirmPassword();
    (this.$refs.formRegister as any).validate((valid: boolean) => {
      if (valid && !this.invalidPassword) {
        this.registerUC.register(this.user);
      }
    });
    this.isLoading = true;
  }

  onChangeValidateConfirmPassword() {
    this.invalidPassword = this.confirmPassword !== this.user.password;
  }

  registerSuccessfully() {
    this.isLoading = false;
    this.popupSuccess("Đăng kí thành công!");
    this.$router.push("/auth/login");
  }

  registerError() {
    this.isLoading = false;
    this.popupError("Đăng kí thất bại!");
  }
}
