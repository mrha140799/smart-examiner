import { Vue } from "vue-property-decorator";

export default abstract class BaseComponent extends Vue {
  public stopLadda() {
    setTimeout(function () {
      window.Ladda.stopAll();
    }, 100);
  }

  public stopLoading() {
    this.stopLadda();
    window.NProgress.done();
    window.$("body").css("pointer-events", "auto");
  }

  public startLoading() {
    window.NProgress.start();
    window.$("body").css("pointer-events", "none");
  }

  public startProgress() {
    window.NProgress.start();
  }

  public stopProgress() {
    window.NProgress.done();
  }

  public popupError(message: string) {
    window.toastr.error("<div class='d-flex justify-content-between'><div class='d-flex align-items-center'>" + message + "</div><div class='pt-1'><button type=\"button\" class=\"close-toastr el-button el-button--danger el-button--small btn-toast\">Đóng</button></div></div>");
    window.$(".close-toastr").on("click", function () {
      // @ts-ignore
      window.toastr.clear(window.$(this).closest(".toast"), { force: true });
    });
  }

  public popupSuccess(message: string) {
    window.toastr.success("<div class='d-flex justify-content-between'><div class='d-flex align-items-center'>" + message + "</div><div class='pt-1'><button type=\"button\" class=\"close-toastr el-button el-button--success el-button--small btn-toast\">Đóng</button></div></div>");
    window.$(".close-toastr").on("click", function () {
      // @ts-ignore
      window.toastr.clear(window.$(this).closest(".toast"), { force: true });
    });
  }

  public popupInfo(message: string) {
    window.toastr.info("<div class='d-flex justify-content-between'><div class='d-flex align-items-center'>" + message + "</div><div class='pt-1'><button type=\"button\" class=\"close-toastr el-button el-button--info el-button--small btn-toast\">Đóng</button></div></div>");
    window.$(".close-toastr").on("click", function () {
      // @ts-ignore
      window.toastr.clear(window.$(this).closest(".toast"), { force: true });
    });
  }
}
