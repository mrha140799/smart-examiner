import DIContainer from "@/core/DIContainer";
import School from "@/model/School";
import IStudentReps from "@/data/repository/IStudentReps";
import Student from "@/model/Student";
import IClassroomReps from "@/data/repository/IClassroomReps";
import Classroom from "@/model/Classroom";

export default class StudentListInteractor {
  private outputPort: IStudentListOutputPort;
  private studentReps: IStudentReps = DIContainer.instance.get(IStudentReps);
  private classReps: IClassroomReps = DIContainer.instance.get(IClassroomReps);

  public deleteById(id: any) {
    this.studentReps.deleteById(id).then(() => {
      this.outputPort.deleteByIdSuccessfully();
    }).catch(() => {
      this.outputPort.deleteByIdError();
    });
  }

  public getDataInit() {
    const promises: Array<Promise<any>> = [];
    promises.push(this.studentReps.getMyStudents(), this.classReps.getAllMyClasses());
    Promise.all(promises).then((responses: Array<any>) => {
      this.outputPort.getDataInitSuccessfully(responses[0].data.data, responses[1].data.data);
    }).catch(() => {
      this.outputPort.getDataInitError();
    });
  }

  public getStudentsByClassId(classId: number | null) {
    if (classId) {
      this.studentReps.getAllByClassId(classId).then((resp: any) => {
        this.outputPort.getStudentsByClassIdSuccessfully(resp.data.data);
      }).catch(() => {
        this.outputPort.getStudentsByClassIdError();
      });
    } else {
      this.studentReps.getMyStudents().then((resp: any) => {
        this.outputPort.getStudentsByClassIdSuccessfully(resp.data.data);
      }).catch(() => {
        this.outputPort.getStudentsByClassIdError();
      });
    }
  }

  public createOne(student: Student, classroomId: number) {
    this.studentReps.createOne(student, classroomId).then(() => {
      this.outputPort.createOrUpdateOneSuccessfully();
    }).catch(() => {
      this.outputPort.createOrUpdateOneError();
    });
  }

  public updateOne(student: Student, classroomId: number) {
    this.studentReps.updateOne(student, classroomId).then(() => {
      this.outputPort.createOrUpdateOneSuccessfully();
    }).catch(() => {
      this.outputPort.createOrUpdateOneError();
    });
  }

  private constructor(outputPort: IStudentListOutputPort) {
    this.outputPort = outputPort;
  }

  public static create(outputPort: IStudentListOutputPort) {
    return new StudentListInteractor(outputPort);
  }
}

export abstract class IStudentListOutputPort {
  abstract getDataInitError(): any;

  abstract getDataInitSuccessfully(students: Array<Student>, classrooms: Array<Classroom>): any;

  abstract createOrUpdateOneSuccessfully(): any;

  abstract createOrUpdateOneError(): any;

  abstract getStudentsByClassIdSuccessfully(students: Array<Student>): any;

  abstract getStudentsByClassIdError(): any;

  abstract deleteByIdSuccessfully(): void;

  abstract deleteByIdError(): void;
}
