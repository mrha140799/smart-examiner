import IAuthReps from "@/data/repository/IAuthReps";
import DIContainer from "@/core/DIContainer";
import { IUser } from "@/model/User";

export default class RegisterInteractor {
  private outputPort: IRegisterOutputPort;
  private authResp: IAuthReps = DIContainer.instance.get(IAuthReps);

  public register(user: IUser) {
    this.authResp.register(user).then(() => {
      this.outputPort.registerSuccessfully();
    }).catch(() => {
      this.outputPort.registerError();
    });
  }

  private constructor(outputPort: IRegisterOutputPort) {
    this.outputPort = outputPort;
  }

  public static create(outputPort: IRegisterOutputPort) {
    return new RegisterInteractor(outputPort);
  }
}

export abstract class IRegisterOutputPort {
  abstract registerSuccessfully(): any;

  abstract registerError(): any;
}
