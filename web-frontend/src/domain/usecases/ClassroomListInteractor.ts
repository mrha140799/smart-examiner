import DIContainer from "@/core/DIContainer";
import School from "@/model/School";
import IClassroomReps from "@/data/repository/IClassroomReps";
import Classroom from "@/model/Classroom";
import ISchoolReps from "@/data/repository/ISchoolReps";

export default class ClassroomListInteractor {
  private outputPort: IClassroomListOutputPort;
  private classroomReps: IClassroomReps = DIContainer.instance.get(IClassroomReps);
  private schoolReps: ISchoolReps = DIContainer.instance.get(ISchoolReps);

  public createClassroom(classroom: Classroom) {
    this.classroomReps.createOne(classroom).then(() => {
      this.outputPort.createOneSuccessfully();
    }).catch(() => {
      this.outputPort.createOneError();
    });
  }

  public deleteById(id: any) {
    this.classroomReps.deleteById(id).then(() => {
      this.outputPort.deleteByIdSuccessfully();
    }).catch(() => {
      this.outputPort.deleteByIdError();
    });
  }

  private constructor(outputPort: IClassroomListOutputPort) {
    this.outputPort = outputPort;
  }

  public static create(outputPort: IClassroomListOutputPort) {
    return new ClassroomListInteractor(outputPort);
  }

  public getAllMyClassrooms() {
    this.classroomReps.getAllMyClasses().then((resp: any) => {
      this.outputPort.getAllMyClassroomsSuccessfully(resp.data.data);
    }).catch(() => {
      this.outputPort.getAllMyClassroomsError();
    });
  }

  public getAllMySchool() {
    this.schoolReps.getAllMySchools().then((resp) => {
      this.outputPort.getAllMySchoolSuccessfully(resp.data.data);
    }).catch(() => {
      this.outputPort.getAllMySchoolError();
    });
  }

  public getOneById(id: any) {
    this.classroomReps.getOneById(id).then((resp: any) => {
      this.outputPort.getOneByIdSuccessfully(resp.data.data);
    }).catch(() => {
      this.outputPort.getOneByIdError();
    });
  }

  public updateOne(classroom: Classroom) {
    this.classroomReps.updateOne(classroom).then(() => {
      this.outputPort.updateOneSuccessfully();
    }).catch(() => {
      this.outputPort.updateOneError();
    });
  }
}

export abstract class IClassroomListOutputPort {
  abstract getAllMyClassroomsSuccessfully(classrooms: Array<Classroom>): any;

  abstract getAllMyClassroomsError(): any;

  abstract getAllMySchoolSuccessfully(schools: Array<School>): any;

  abstract getAllMySchoolError(): any;

  abstract createOneSuccessfully(): any;

  abstract createOneError(): any;

  abstract getOneByIdSuccessfully(classroom: Classroom): void;

  abstract getOneByIdError(): void;

  abstract updateOneSuccessfully(): void;

  abstract updateOneError(): void;

  abstract deleteByIdSuccessfully(): void;

  abstract deleteByIdError(): void;
}
