import IAuthReps from "@/data/repository/IAuthReps";
import DIContainer from "@/core/DIContainer";
import { IUser } from "@/model/User";
import School from "@/model/School";
import ISchoolReps from "@/data/repository/ISchoolReps";
import ApiResponse from "@/model/ApiResponse";
import ITestReps from "@/data/repository/ITestReps";
import Test from "@/model/Test";

export default class TestInteractor {
  private outputPort: ITestOutputPort;
  private testReps: ITestReps = DIContainer.instance.get(ITestReps);

  public createOne(test: Test) {
    this.testReps.createOne(test).then(() => {
      this.outputPort.createOneSuccessfully();
    }).catch(() => {
      this.outputPort.createOneError();
    });
  }

  public isValidTestCode(code: string, testId: number | null) {
    this.testReps
      .isValidTestCode(code, testId)
      .then(() => {
        this.outputPort.isValidTestCode();
      })
      .catch(() => {
        this.outputPort.invalidTestCode();
      });
  }

  public getById(id: number) {
    this.testReps
      .getById(id)
      .then((resp: any) => {
        this.outputPort.getByIdSuccessfully(resp.data.data);
      })
      .catch(() => {
        this.outputPort.getByIdError();
      });
  }

  public updateTest(test: Test) {
    this.testReps
      .updateTest(test)
      .then(() => {
        this.outputPort.updateTestSuccessfully();
      })
      .catch((e) => {
        this.outputPort.updateTestError();
      });
  }

  public downloadCreatedFile() {
    this.testReps.getCreatedFile()
      .then((resp: any) => {
        this.outputPort.downloadCreatedFileSuccessfully(resp.data);
      }).catch(() => {
        this.outputPort.downloadCreatedFileError();
      });
  }

  private constructor(outputPort: ITestOutputPort) {
    this.outputPort = outputPort;
  }

  public static create(outputPort: ITestOutputPort) {
    return new TestInteractor(outputPort);
  }
}

export abstract class ITestOutputPort {
  abstract createOneSuccessfully(): any;

  abstract createOneError(): any;

  abstract isValidTestCode(): any;

  abstract invalidTestCode(): any;

  abstract getByIdSuccessfully(test: Test): any;

  abstract getByIdError(): any;

  abstract updateTestSuccessfully(): any;

  abstract updateTestError(): any;

  abstract downloadCreatedFileSuccessfully(file: any): any;

  abstract downloadCreatedFileError(): any;
}
