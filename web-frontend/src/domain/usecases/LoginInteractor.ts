import IAuthReps from "@/data/repository/IAuthReps";
import DIContainer from "@/core/DIContainer";
import { IAccount } from "@/model/Account";
import IUserReps from "@/data/repository/IUserReps";
import { IUser } from "@/model/User";

export default class LoginInteractor {
  private repos: IAuthReps = DIContainer.instance.get(IAuthReps);
  private outputPort: ILoginOutputPort;
  private userRepos: IUserReps = DIContainer.instance.get(IUserReps);

  private constructor(outputPort: ILoginOutputPort) {
    this.outputPort = outputPort;
  }

  public static create(outputPort: ILoginOutputPort) {
    return new LoginInteractor(outputPort);
  }

  public login(user: IAccount) {
    this.repos.login(user).then((resp) => {
      const dataSuccess = resp.data;
      if (dataSuccess.statusCode === 200) {
        this.userRepos.saveUserInfo(dataSuccess.data.token, JSON.stringify(dataSuccess.data.userInfo));
        this.outputPort.loginComplete(resp);
      } else {
        this.outputPort.loginError();
      }
    }).catch(() => {
      this.outputPort.loginError();
    });
  }

  public register(user: IUser) {

  }
}

export abstract class ILoginOutputPort {
  abstract loginComplete(res: any): void;

  abstract loginError(): void;
}
