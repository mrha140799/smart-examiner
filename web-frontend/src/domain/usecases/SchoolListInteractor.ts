import IAuthReps from "@/data/repository/IAuthReps";
import DIContainer from "@/core/DIContainer";
import { IUser } from "@/model/User";
import School from "@/model/School";
import ISchoolReps from "@/data/repository/ISchoolReps";
import ApiResponse from "@/model/ApiResponse";

export default class SchoolListInteractor {
  private outputPort: ISchoolListOutputPort;
  private schoolReps: ISchoolReps = DIContainer.instance.get(ISchoolReps);

  public updateSchool(school: School) {
    this.schoolReps.updateOne(school).then(() => {
      this.outputPort.updateSchoolSuccessfully();
    }).catch(() => {
      this.outputPort.updateSchoolError();
    });
  }

  public deleteById(id: any) {
    this.schoolReps.deleteById(id).then(() => {
      this.outputPort.deleteByIdSuccessfully();
    }).catch(() => {
      this.outputPort.deleteByIdError();
    });
  }

  public getOneById(id: any) {
    this.schoolReps.getOneById(id).then((resp: any) => {
      this.outputPort.getOneByIdSuccessfully(resp.data.data);
    }).catch(() => {
      this.outputPort.getOneByIdError();
    });
  }

  public getAllMySchools() {
    this.schoolReps.getAllMySchools().then((resp: any) => {
      this.outputPort.getAllMySchoolsSuccessfully(resp.data.data);
    }).catch(() => {
      this.outputPort.getAllMySchoolsError();
    });
  }

  public createSchool(school: School) {
    this.schoolReps.createOne(school).then(() => {
      this.outputPort.createSchoolSuccessfully();
    }).catch(() => {
      this.outputPort.createSchoolError();
    });
  }

  private constructor(outputPort: ISchoolListOutputPort) {
    this.outputPort = outputPort;
  }

  public static create(outputPort: ISchoolListOutputPort) {
    return new SchoolListInteractor(outputPort);
  }
}

export abstract class ISchoolListOutputPort {
  abstract getAllMySchoolsError(): any;

  abstract getAllMySchoolsSuccessfully(schools: Array<School>): any;

  abstract createSchoolSuccessfully(): any;

  abstract createSchoolError(): any;

  abstract updateSchoolSuccessfully(): any;

  abstract updateSchoolError(): any;

  abstract getOneByIdSuccessfully(school: School): any;

  abstract getOneByIdError(): any;

  abstract deleteByIdSuccessfully(): any;

  abstract deleteByIdError(): any;
}
