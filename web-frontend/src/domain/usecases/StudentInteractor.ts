import DIContainer from "@/core/DIContainer";
import School from "@/model/School";
import IClassroomReps from "@/data/repository/IClassroomReps";
import Classroom from "@/model/Classroom";
import ISchoolReps from "@/data/repository/ISchoolReps";
import Students from "@/model/Students";
import IStudentReps from "@/data/repository/IStudentReps";
import Student from "@/model/Student";

export default class StudentInteractor {
  private outputPort: IStudentOutputPort;
  private studentReps: IStudentReps = DIContainer.instance.get(IStudentReps);
  private classReps: IClassroomReps = DIContainer.instance.get(IClassroomReps);

  private constructor(outputPort: IStudentOutputPort) {
    this.outputPort = outputPort;
  }

  public static create(outputPort: IStudentOutputPort) {
    return new StudentInteractor(outputPort);
  }

  public getAllMyClasses() {
    this.classReps.getAllMyClasses().then((resp) => {
      this.outputPort.getAllMyClassesSuccessfully(resp.data.data);
    }).catch(() => {
      this.outputPort.getAllMyClassesError();
    });
  }

  public downloadCreatedFile() {
    this.studentReps.getCreatedFile()
      .then((resp: any) => {
        this.outputPort.downloadCreatedFileSuccessfully(resp.data);
      }).catch(() => {
        this.outputPort.downloadCreatedFileError();
      });
  }

  public createMany(students: Students) {
    this.studentReps.createMany(students).then(() => {
      this.outputPort.createManySuccessfully();
    }).catch(() => {
      this.outputPort.createManyError();
    });
  }
}

export abstract class IStudentOutputPort {
  abstract getAllMyClassesSuccessfully(classrooms: Array<Classroom>): any;
  abstract getAllMyClassesError(): any;
  abstract createManySuccessfully(): any;
  abstract createManyError(): any;
  abstract downloadCreatedFileSuccessfully(file: any): any;
  abstract downloadCreatedFileError(): any;
}
