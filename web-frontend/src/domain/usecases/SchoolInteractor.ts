import DIContainer from "@/core/DIContainer";
import School from "@/model/School";
import ISchoolReps from "@/data/repository/ISchoolReps";

export default class SchoolInteractor {
  private outputPort: ISchoolOutputPort;
  private schoolReps: ISchoolReps = DIContainer.instance.get(ISchoolReps);

  public createSchool(school: School) {
    this.schoolReps.createOne(school).then(() => {
      this.outputPort.createSchoolSuccessfully();
    }).catch(() => {
      this.outputPort.createSchoolError();
    });
  }

  public updateSchool(school: School) {
  }

  public findSchoolById(id: any) {
  }

  private constructor(outputPort: ISchoolOutputPort) {
    this.outputPort = outputPort;
  }

  public static create(outputPort: ISchoolOutputPort) {
    return new SchoolInteractor(outputPort);
  }
}

export abstract class ISchoolOutputPort {
  abstract createSchoolSuccessfully(): any;

  abstract createSchoolError(): any;
}
