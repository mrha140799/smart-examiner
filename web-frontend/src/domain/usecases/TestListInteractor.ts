import IAuthReps from "@/data/repository/IAuthReps";
import DIContainer from "@/core/DIContainer";
import { IUser } from "@/model/User";
import School from "@/model/School";
import ISchoolReps from "@/data/repository/ISchoolReps";
import ApiResponse from "@/model/ApiResponse";
import ITestReps from "@/data/repository/ITestReps";
import Test from "@/model/Test";

export default class TestListInteractor {
  private outputPort: ITestListOutputPort;
  private testReps: ITestReps = DIContainer.instance.get(ITestReps);

  public getAllMyTests() {
    this.testReps.getAllMyTests().then((resp: any) => {
      this.outputPort.getAllMyTestsSuccessfully(resp.data.data);
    }).catch(() => {
      this.outputPort.getAllMyTestsError();
    });
  }

  public deleteById(id: any) {
    this.testReps.deleteById(id).then(() => {
      this.outputPort.deleteByIdSuccessfully();
    }).catch(() => {
      this.outputPort.deleteByIdError();
    });
  }

  public downloadFormAnswerFile() {
    this.testReps.getFormAnswerFile().then((resp: any) => {
      this.outputPort.downloadFormAnswerFileSuccessfully(resp.data);
    }).catch(() => {
      this.outputPort.downloadFormAnswerFileError();
    });
  }

  private constructor(outputPort: ITestListOutputPort) {
    this.outputPort = outputPort;
  }

  public static create(outputPort: ITestListOutputPort) {
    return new TestListInteractor(outputPort);
  }
}

export abstract class ITestListOutputPort {
  abstract getAllMyTestsError(): any;

  abstract getAllMyTestsSuccessfully(schools: Array<Test>): any;

  abstract deleteByIdSuccessfully(): void;

  abstract deleteByIdError(): void;

  abstract downloadFormAnswerFileSuccessfully(fileContent: string): void;

  abstract downloadFormAnswerFileError(): void;
}
