import Student from "@/model/Student";

export default class Students {
  public classroomId: number | null = null;
  public students: Array<Student> = [];
}
