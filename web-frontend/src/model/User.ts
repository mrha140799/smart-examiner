export default class User implements IUser {
  constructor(
    public password?: string,
    public firstName?: string,
    public lastName?: string,
    public email?: string,
    public phoneNumber?: string,
    public gender?: boolean,
    public imageAvatarName?: string,
    public roles?: Array<string>
  ) {
  }
}

export interface IUser {
  password?: string,
  firstName?: string,
  lastName?: string,
  email?: string,
  phoneNumber?: string,
  gender?: boolean,
  imageAvatarName?: string
  roles?: Array<string>
}
