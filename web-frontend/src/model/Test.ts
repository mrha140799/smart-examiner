export default class Test {
  public id: number | null = null;
  public code: string = "";
  public subject: string = "";
  public testDay: Date = new Date();
  public answers: string = "";
}
