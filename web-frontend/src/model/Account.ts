export default class Account implements IAccount {
  constructor(public email?: string, public password?: string) {
  }
}

export interface IAccount {
  email?: string,
  password?: string
}
