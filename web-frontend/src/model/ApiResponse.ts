export default class ApiResponse<T> {
  public statusCode: number = -1;
  public message: string = "";
  public data: T | undefined;
}
