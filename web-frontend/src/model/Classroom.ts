import School from "@/model/School";

export default class Classroom {
  public id ?: number;
  public name: string = "";
  public school: School | null = null;
}
