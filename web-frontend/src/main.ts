import Vue, { DirectiveFunction } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./presentation/router";
import store from "./data/store";
import * as Sentry from "@sentry/browser";
import * as Integrations from "@sentry/integrations";
import BootstrapVue from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import "bootstrap";
import Element from "element-ui";
import * as Ladda from "ladda";
// @ts-ignore
import lang from "element-ui/lib/locale/lang/vi";
// @ts-ignore
import locale from "element-ui/lib/locale";

declare global {
  interface Window {
    c: any;
    jQuery: any;
    $: any;
    Ladda: any;
    NProgress: any;
    toastr: any;
  }
}
window.toastr = require("toastr");
window.jQuery = window.$ = require("jquery");
const NProgress = require("nprogress");
NProgress.configure({
  trickleSpeed: 1000,
  minimum: 0.2
});
window.NProgress = NProgress;
locale.use(lang);
Vue.use(BootstrapVue);
Vue.use(Element, {
  size: "small",
  zIndex: 3000
});
window.toastr.options = {
  closeButton: false,
  debug: false,
  newestOnTop: false,
  progressBar: true,
  positionClass: "toast-top-right",
  preventDuplicates: true,
  onclick: null,
  showDuration: "300",
  hideDuration: "1000",
  timeOut: "6000",
  extendedTimeOut: "1000",
  tapToDismiss: false
};
window.Ladda = Ladda;
// @ts-ignore
Vue.directive("ladda", function LaddaDirectiveOptions(el: any, binding: any, vnode: any): DirectiveFunction {
  window.$(el).attr("data-style", "slide-left");
  window.Ladda.bind(el);
});
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
Sentry.init({
  enabled: process.env.VUE_APP_SENTRY_DSN,
  dsn: process.env.VUE_APP_SENTRY_DSN,
  environment: process.env.NODE_ENV,
  integrations: [new Integrations.Vue({ Vue, attachProps: true })]
});
