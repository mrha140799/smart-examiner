import { Container } from "inversify";
import "reflect-metadata";
import IUserReps from "@/data/repository/IUserReps";
import UserService from "@/data/service/UserService";
import IAuthReps from "@/data/repository/IAuthReps";
import AuthApi from "@/data/api/AuthApi";
import ISchoolReps from "@/data/repository/ISchoolReps";
import SchoolApi from "@/data/api/SchoolApi";
import IClassroomReps from "@/data/repository/IClassroomReps";
import ClassroomApi from "@/data/api/ClassroomApi";
import ITestReps from "@/data/repository/ITestReps";
import TestApi from "@/data/api/TestApi";
import IStudentReps from "@/data/repository/IStudentReps";
import StudentApi from "@/data/api/StudentApi";

export default class DIContainer {
  private static _instance: DIContainer;
  private container: Container;

  private constructor() {
    this.container = new Container();
    this.container.bind(IUserReps).to(UserService).inSingletonScope();
    this.container.bind(IAuthReps).to(AuthApi).inSingletonScope();
    this.container.bind(ISchoolReps).to(SchoolApi).inSingletonScope();
    this.container.bind(IClassroomReps).to(ClassroomApi).inSingletonScope();
    this.container.bind(ITestReps).to(TestApi).inSingletonScope();
    this.container.bind(IStudentReps).to(StudentApi).inSingletonScope();
  }

  public static get instance(): DIContainer {
    if (!this._instance) {
      this._instance = new DIContainer();
    }
    return this._instance;
  }

  public get(serviceIdentifier: any): any {
    return this.container.get(serviceIdentifier);
  }
}
