package com.siten.backend.core.auth;

import com.siten.backend.data.AuthorityName;
import com.siten.backend.data.entity.User;
import com.siten.backend.data.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MyAuthenticationProvider implements AuthenticationProvider {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getPrincipal().toString();
        String password = authentication.getCredentials().toString();
        if (!userRepository.existsByEmail(username)) {
            throw new BadCredentialsException("Email not found!");
        }
        User user = userRepository.findByEmail(username).get();
        if (!passwordEncoder.matches(password, user.getPassword())) {
            throw new BadCredentialsException("Password not matched!");
        }
        if (!user.isEnabled()) {
            throw new BadCredentialsException("User is disable");
        }
        List<GrantedAuthority> authorities = user.getAuthorities().stream().map(role ->
                new SimpleGrantedAuthority(AuthorityName.getRoleByValue(role.getName()))).collect(Collectors.toList());
        return new UsernamePasswordAuthenticationToken(UserPrinciple.build(user), null, authorities);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }
}
