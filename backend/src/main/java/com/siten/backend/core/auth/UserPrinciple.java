package com.siten.backend.core.auth;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.siten.backend.data.AuthorityName;
import com.siten.backend.data.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserPrinciple implements UserDetails {
    @JsonIgnore
    private String id;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String imageAvatarName;
    private boolean gender;
    private String email;
    @JsonIgnore
    private String password;
    private Set<String> roles = new HashSet<>();
    @JsonIgnore
    private Collection<? extends GrantedAuthority> authorities;

    public UserPrinciple(String id, String email, String firstName, String lastName, String phoneNumber, String imageAvatarName,
                         boolean gender, String password, List<GrantedAuthority> authorities) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.imageAvatarName = imageAvatarName;
        this.gender = gender;
        this.email = email;
        this.password = password;
        this.authorities = authorities;
        this.roles = this.getRolesStringByGrantedAuthorities(authorities);
    }

    public static UserPrinciple build(User user) {
        List<GrantedAuthority> authorities = user.getAuthorities().stream().map(role ->
                new SimpleGrantedAuthority(AuthorityName.getRoleByValue(role.getName()))).collect(Collectors.toList());
        return new UserPrinciple(user.getId(), user.getEmail(), user.getFirstName(), user.getLastName(), user.getPhoneNumber(),
                user.getImageAvatarName(), user.getGender(), user.getPassword(), authorities);
    }

    private Set<String> getRolesStringByGrantedAuthorities(List<GrantedAuthority> grantedAuthorities) {
        Set<String> roles = new HashSet<>();
        for (GrantedAuthority authority : grantedAuthorities) {
            roles.add(AuthorityName.valueOf(authority.getAuthority()).getValue());
        }
        return roles;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return true;
    }
}
