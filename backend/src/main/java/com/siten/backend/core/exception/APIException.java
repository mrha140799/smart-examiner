package com.siten.backend.core.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class APIException extends RuntimeException {
    private HttpStatus httpStatus;
    private String message;


    private APIException() {
    }

    public static APIException from(HttpStatus httpStatus) {
        APIException ret = new APIException();
        ret.httpStatus = httpStatus;
        return  ret;
    }

    public static APIException incorrectData(String message) {
        APIException ret = new APIException();
        ret.httpStatus = HttpStatus.BAD_REQUEST;
        ret.message = message;
        return ret;
    }

    public APIException withMessage(String message) {
        this.message = message;
        return this;
    }
}
