package com.siten.backend.data.repository;

import com.siten.backend.data.entity.Classroom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClassroomRepository extends JpaRepository<Classroom, Long> {
    List<Classroom> findAllBySchool_CreatedUser_Email(String email);
}
