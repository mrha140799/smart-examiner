package com.siten.backend.data.entity;

import com.siten.backend.data.AuthorityName;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Table(name = "authorities")
@Entity
@Getter
@Setter
public class Authority {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id")
    private String id;
    @Column(name = "authority_name")
    @NotNull
    private String name;

    public Authority(@NotNull AuthorityName name) {
        this.name = name.getValue();
    }

    public Authority() {
    }
}
