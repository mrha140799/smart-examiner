package com.siten.backend.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Table(name = "tests")
@Entity
@Getter
@Setter
public class Test {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    @Length(min = 4, max = 4)
    @Column(name = "code", length = 4, nullable = false)
    private String code;
    @NotNull
    @Column(name = "subject")
    private String subject;
    @NotNull
    @Column(name = "test_day")
    private Date testDay;
    @Column(name = "answers")
    private String answers;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "created_user")
    private User createdUser;
    @JsonIgnore
    @OneToMany(mappedBy = "test", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<StudentCode> studentCode;
}
