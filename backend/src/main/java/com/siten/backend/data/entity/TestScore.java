package com.siten.backend.data.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Table(name = "test_score")
@Entity
@Getter
@Setter
public class TestScore {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "score")
    private float score;
    @Column(name = "user_answers")
    private String userAnswers;
    @Column(name = "result")
    private String result;
    @Column(name = "created_time")
    private Date createdTime;
    @Column(name = "test_image_full_name")
    private String testImageFullName;
    @Column(name = "test_image_answer_name")
    private String testImageAnswerName;
    @Column(name = "test_image_uswer_info_name")
    private String testImageUserInfoName;
    @OneToOne(mappedBy = "testScore")
    private StudentCode studentCode;

    public TestScore() {
        this.createdTime = new Date();
    }
}
