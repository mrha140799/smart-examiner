package com.siten.backend.data;

import java.util.HashMap;
import java.util.Map;

public enum AuthorityName {
    ROLE_ADMIN("Admin"),
    ROLE_USER("User"),
    ROLE_ADMIN_SYSTEM("Admin system");
    private final String value;

    AuthorityName( String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static String getRoleByValue(String value) {
        for(AuthorityName e : AuthorityName.values()){
            if(e.value.equals(value)) return e.name();
        }
        return null;
    }

}
