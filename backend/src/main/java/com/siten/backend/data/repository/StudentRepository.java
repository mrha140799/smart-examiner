package com.siten.backend.data.repository;

import com.siten.backend.data.entity.Student;
import com.siten.backend.data.entity.StudentCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    Student findByFullName(String fullName);
    List<Student> findAllByClassroom_School_CreatedUser_Email(String email);
    Optional<Student> findByStudentCodesContains(StudentCode studentCode);
    List<Student> findAllByClassroom_Id(Long id);
}
