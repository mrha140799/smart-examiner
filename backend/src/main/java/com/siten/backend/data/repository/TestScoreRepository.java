package com.siten.backend.data.repository;

import com.siten.backend.data.entity.TestScore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TestScoreRepository extends JpaRepository<TestScore, Long> {
}
