package com.siten.backend.data.repository;

import com.siten.backend.data.entity.Authority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority, String> {
    Authority findByName(String name);
    Boolean existsByName(String name);
}
