package com.siten.backend.data.repository;

import com.siten.backend.data.entity.Test;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TestRepository extends JpaRepository<Test, Long> {
    List<Test> findAllByCreatedUser_Email(String email);
    Optional<Test> findByCodeAndCreatedUser_Email(String code, String email);
    boolean existsByCodeAndIdNotAndCreatedUser_Email(String code, Long id, String email);
    boolean existsByCodeAndCreatedUser_Email(String code, String email);
}
