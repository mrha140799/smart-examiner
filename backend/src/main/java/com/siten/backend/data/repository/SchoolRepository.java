package com.siten.backend.data.repository;

import com.siten.backend.data.entity.Classroom;
import com.siten.backend.data.entity.School;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.JoinType;
import java.awt.print.Book;
import java.util.List;
import java.util.Optional;

@Repository
public interface SchoolRepository extends JpaRepository<School, Long>, JpaSpecificationExecutor<School> {
    List<School> findAllByCreatedUser_Email(String email);
    Optional<School> findByIdAndCreatedUser_Email(Long id, String email);

    static Specification<School> hasAuthor(String name) {
        return (school, cq, cb) -> cb.equal(school.get("name"), name);
    }

    static Specification<School> hasAddress(String address) {
        return (school, cq, cb) -> cb.equal(school.get("address"), address);
    }

    static Specification<School> containAddress(String address) {
        return (school, cq, cb) -> cb.equal(school.get("name"), address);
    }

    static Specification<School> containClassroomName(String classroomName) {
        return (school, criteriaQuery, criteriaBuilder) -> {
            school.join("classrooms", JoinType.INNER);
            return criteriaBuilder.equal(school.get("classrooms").get("name"), classroomName);
        };
    }
}
