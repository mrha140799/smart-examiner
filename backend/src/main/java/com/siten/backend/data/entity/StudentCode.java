package com.siten.backend.data.entity;

import com.siten.backend.model.dto.StudentCodeDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "student_code")
@NoArgsConstructor
@Getter
@Setter
@NamedNativeQuery(
        name = "getAllStudentCodeDTO",
        query = "select distinct student_code.id, code, date_of_birth, full_name FROM student_code INNER JOIN students ON student_id = students.id",
        resultSetMapping ="student_code_dto"
)
@SqlResultSetMapping(
        name = "student_code_dto",
        classes = @ConstructorResult(
                targetClass = StudentCodeDTO.class,
                columns = {
                        @ColumnResult(name = "id", type = Long.class),
                        @ColumnResult(name = "code", type = String.class),
                        @ColumnResult(name = "date_of_birth", type = Date.class),
                        @ColumnResult(name = "full_name", type = String.class)
                }
        )
)
public class StudentCode {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @Column(name = "code", length = 7, nullable = false)
    private String code;
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST,CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "student_id")
    private Student student;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "test_score_id")
    private TestScore testScore;
    @ManyToOne
    @JoinColumn(name = "test_id")
    private Test test;
}
