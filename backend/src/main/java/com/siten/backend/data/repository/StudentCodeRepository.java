package com.siten.backend.data.repository;

import com.siten.backend.data.entity.StudentCode;
import com.siten.backend.model.dto.StudentCodeDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.List;
import java.util.Optional;

@Repository
public interface StudentCodeRepository extends JpaRepository<StudentCode, Long> {
    Optional<StudentCode> findByCodeAndTest_IdAndStudent_Classroom_School_CreatedUser_Email(String code, Long testId, String email);
    List<StudentCode> findAllByTest_IdAndStudent_Classroom_School_CreatedUser_Email(Long testId, String email);
    boolean existsByCodeAndTest_Id(String code, Long id);
    boolean existsByStudent_IdAndTest_Id(Long studentId, Long TestId);
    List<StudentCode> findAllByTestScoreIsNotNullAndTest_CreatedUser_Email(String email);

    @Query(value = "select studentCode from StudentCode studentCode left join studentCode.student where studentCode.code = :code and studentCode.test.id = :id and studentCode.student.classroom.school.createdUser.email = :email")
    StudentCode findMyStudentByCode(@Param("code") String code, @Param("id") Long testId, @Param("email") String email);

    @Query(name = "getAllStudentCodeDTO", nativeQuery = true)
    List<StudentCodeDTO> getAllStudentCodeDTO();
}
