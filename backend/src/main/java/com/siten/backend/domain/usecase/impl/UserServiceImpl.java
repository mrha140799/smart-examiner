package com.siten.backend.domain.usecase.impl;

import com.siten.backend.core.auth.Commons;
import com.siten.backend.core.auth.UserPrinciple;
import com.siten.backend.core.exception.APIException;
import com.siten.backend.data.entity.*;
import com.siten.backend.data.repository.*;
import com.siten.backend.domain.usecase.UserService;
import com.siten.backend.model.SuccessResponse;
import com.siten.backend.model.request.UserDTO;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final String IMAGE_BASE_64_PREFIX = "data:image/png;base64,";
    @Autowired
    private SchoolRepository schoolRepository;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private ClassroomRepository classroomRepository;
    @Autowired
    private TestRepository testRepository;
    @Autowired
    private UserRepository userRepository;

    @Value("${file.director.output}")
    private String FILE_OUT_PUT;

    @Override
    public SuccessResponse getCurrentUser() {
        UserPrinciple userPrinciple = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return SuccessResponse.ok(userPrinciple);
    }

    @Override
    public SuccessResponse changeMyProfile(UserDTO userDTO) {
        UserPrinciple userPrinciple = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = this.userRepository.findByEmail(userPrinciple.getEmail()).orElseThrow(() -> APIException.incorrectData("Email not found!"));
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setGender(userDTO.isGender());
        user.setPhoneNumber(userDTO.getPhoneNumber());
        userRepository.save(user);
        return SuccessResponse.ok(UserPrinciple.build(user));
    }

    @Override
    public SuccessResponse changeMyAvatar(String fileContent) {
        UserPrinciple userPrinciple = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = this.userRepository.findByEmail(userPrinciple.getEmail()).orElseThrow(() -> APIException.incorrectData("Email not found!"));
        String fileName = "avatar_" + (new Date()).getTime() + ".png";
        byte[] data = Base64.decodeBase64(fileContent);
        try (FileOutputStream fos = new FileOutputStream("./files/avatar/" + fileName)) {
            fos.write(data);
            user.setImageAvatarName(fileName);
            this.userRepository.save(user);
        } catch (IOException exception) {
            throw APIException.incorrectData("Lỗi, file không chính xác");
        }

        return SuccessResponse.ok(fileName);
    }

    @Override
    public SuccessResponse getAllMySchools() {
        UserPrinciple userPrinciple = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<School> schools;
        if (userPrinciple.getRoles().contains(Commons.ROLE_ADMIN_SYSTEM_NAME)) {
            schools = this.schoolRepository.findAll();
        } else {
            schools = this.schoolRepository.findAllByCreatedUser_Email(userPrinciple.getEmail());
        }
        return SuccessResponse.ok(schools);
    }

    @Override
    public SuccessResponse getAllMyClassRooms() {
        UserPrinciple userPrinciple = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Classroom> classrooms;
        if (userPrinciple.getRoles().contains(Commons.ROLE_ADMIN_SYSTEM_NAME)) {
            classrooms = this.classroomRepository.findAll();
        } else {
            classrooms = this.classroomRepository.findAllBySchool_CreatedUser_Email(userPrinciple.getEmail());
        }
        return SuccessResponse.ok(classrooms);
    }

    @Override
    public SuccessResponse getAllMyStudents() {
        UserPrinciple userPrinciple = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Student> students;
        if (userPrinciple.getRoles().contains(Commons.ROLE_ADMIN_SYSTEM_NAME)) {
            students = this.studentRepository.findAll();
        } else {
            students = this.studentRepository.findAllByClassroom_School_CreatedUser_Email(userPrinciple.getEmail());
        }
        return SuccessResponse.ok(students);
    }

    @Override
    public SuccessResponse getAllMyTests() {
        UserPrinciple userPrinciple = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Test> tests;
        if (userPrinciple.getRoles().contains(Commons.ROLE_ADMIN_SYSTEM_NAME)) {
            tests = this.testRepository.findAll();
        } else {
            tests = this.testRepository.findAllByCreatedUser_Email(userPrinciple.getEmail());
        }
        return SuccessResponse.ok(tests);
    }

    @Override
    public String getTestCreatedFile() throws IOException {
        Path path = Paths.get("./files/output/Phieu-Tao-De-Thi-Smart-Exammier.xlsx");
        return Base64.encodeBase64String(Files.readAllBytes(path));
    }

    @Override
    public String getStudentCreatedFile() throws IOException {
        Path path = Paths.get("./files/output/Phieu-Tao-Hoc-Vien-Smart-Exammier.xlsx");
        return Base64.encodeBase64String(Files.readAllBytes(path));
    }

    @Override
    public String getFormAnswerFile() throws IOException {
        Path path = Paths.get("./files/output/Phieu-Cham-Thi.pdf");
        return Base64.encodeBase64String(Files.readAllBytes(path));
    }
}
