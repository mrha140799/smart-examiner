package com.siten.backend.domain.usecase;

import com.siten.backend.data.entity.Classroom;
import com.siten.backend.data.entity.Student;
import com.siten.backend.model.SuccessResponse;

public interface ClassroomService {
    SuccessResponse createOne(Classroom classroom);
    SuccessResponse deleteById(Long id);
    SuccessResponse findOneById(Long id);
    SuccessResponse updateOne(Classroom classroom);
}
