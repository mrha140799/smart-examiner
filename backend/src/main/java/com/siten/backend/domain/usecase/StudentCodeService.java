package com.siten.backend.domain.usecase;

import com.siten.backend.model.SuccessResponse;
import com.siten.backend.model.request.StudentCodeRequest;

public interface StudentCodeService {
    SuccessResponse getAllMyStudentCodesByTestId(Long testId);
    SuccessResponse createOne(StudentCodeRequest studentCodeRequest);
    SuccessResponse updateOne(StudentCodeRequest studentCodeRequest);
    SuccessResponse existsByStudentCodeAndTestId(String studentCode, Long testId);
    SuccessResponse getOneById( Long studentCodeId);
    SuccessResponse deleteById( Long studentCodeId);
    SuccessResponse test();
}
