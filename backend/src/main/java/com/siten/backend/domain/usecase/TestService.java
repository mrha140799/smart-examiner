package com.siten.backend.domain.usecase;

import com.siten.backend.data.entity.Test;
import com.siten.backend.model.SuccessResponse;

public interface TestService {
    SuccessResponse createOne(Test test);
    SuccessResponse updateTest(Test test);
    SuccessResponse isValidTestCode(String testCode, Long testId);
    SuccessResponse getById(Long id);
    SuccessResponse deleteById(Long id);
}
