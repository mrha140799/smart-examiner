package com.siten.backend.domain.usecase;

import com.siten.backend.model.SuccessResponse;
import com.siten.backend.model.request.UserDTO;
import org.springframework.core.io.ByteArrayResource;

import java.io.IOException;

public interface UserService {
    SuccessResponse getCurrentUser();
    SuccessResponse changeMyProfile(UserDTO userDTO);
    SuccessResponse changeMyAvatar(String userDTO);
    SuccessResponse getAllMySchools();
    SuccessResponse getAllMyClassRooms();
    SuccessResponse getAllMyStudents();
    SuccessResponse getAllMyTests();
    String getTestCreatedFile() throws IOException;
    String getStudentCreatedFile() throws IOException;
    String getFormAnswerFile() throws IOException;
}
