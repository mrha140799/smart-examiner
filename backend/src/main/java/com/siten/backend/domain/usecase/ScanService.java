package com.siten.backend.domain.usecase;

import com.siten.backend.model.SuccessResponse;

public interface ScanService {
    SuccessResponse saveFile(String fileContent);
    SuccessResponse myAnswer(String fileContent);
    SuccessResponse myHistories();
}
