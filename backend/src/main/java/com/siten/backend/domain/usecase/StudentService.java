package com.siten.backend.domain.usecase;

import com.siten.backend.data.entity.Student;
import com.siten.backend.model.SuccessResponse;
import com.siten.backend.model.request.StudentsRequest;

public interface StudentService {
    SuccessResponse createOne(Student student, Long classroomId);
    SuccessResponse updateOne(Student student, Long classroomId);
    SuccessResponse createMany(StudentsRequest studentsRequest);
    SuccessResponse findOneById(Long id);
    SuccessResponse getAllByClassId(Long classId);
    SuccessResponse deleteById(Long id);
}
