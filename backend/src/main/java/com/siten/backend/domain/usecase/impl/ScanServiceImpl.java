package com.siten.backend.domain.usecase.impl;

import com.siten.backend.core.auth.UserPrinciple;
import com.siten.backend.core.exception.APIException;
import com.siten.backend.data.entity.StudentCode;
import com.siten.backend.data.entity.Test;
import com.siten.backend.data.entity.TestScore;
import com.siten.backend.data.repository.StudentCodeRepository;
import com.siten.backend.data.repository.StudentRepository;
import com.siten.backend.data.repository.TestRepository;
import com.siten.backend.data.repository.TestScoreRepository;
import com.siten.backend.domain.usecase.ScanService;
import com.siten.backend.model.ScanResponse;
import com.siten.backend.model.SuccessResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.tomcat.util.codec.binary.Base64;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

@Service
@Transactional
public class ScanServiceImpl implements ScanService {
    @Autowired
    private TestRepository testRepository;
    @Autowired
    private StudentCodeRepository studentCodeRepository;
    @Autowired
    private TestScoreRepository testScoreRepository;
    @Autowired
    private StudentRepository studentRepository;

    private final String GET_ANSWER_TYPE = "GET_ANSWER_TYPE";
    private final String GET_USER_INFO_TYPE = "GET_USER_INFO_TYPE";
    private final String ANSWER_LABEL = "DCBA";
    private final Log LOGGER = LogFactory.getLog(getClass());
    @Value("${file.director.output}")
    private String FILE_OUT_PUT;
    @Value("${file.director.scan-result}")
    private String FILE_SCAN_RESULTS;

    @Override
    public SuccessResponse saveFile(String fileContent) {
        return SuccessResponse.ok();
    }

    @Override
    public SuccessResponse myAnswer(String fileContent) {
        String currentUserEmail = this.getCurrentUserEmail();
        byte[] data = Base64.decodeBase64(fileContent);
        Mat src = Imgcodecs.imdecode(new MatOfByte(data), Imgcodecs.CV_LOAD_IMAGE_UNCHANGED);
        Mat dstRotate = new Mat();
        if (src.width() > src.height()) {
            Core.rotate(src, dstRotate, Core.ROTATE_90_CLOCKWISE);
        } else {
            dstRotate = src;
        }
        try {
            writeFile(dstRotate, "test.png");
        } catch (Exception e) {
            System.out.println(e);
            throw APIException.incorrectData("Image incorrect!");
        }
        Mat srcGray = new Mat();
        Imgproc.cvtColor(dstRotate, srcGray, Imgproc.COLOR_RGB2GRAY);
        try {
            //IGO -> Image gray original
            Mat igoThreshold = this.thresholdImage(srcGray.clone(), 100, Imgproc.THRESH_BINARY_INV);
            // kernel to filter noise
            Mat kernelIGP = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5, 5));
            Mat matIGOFilterNoise = new Mat();
            // do filter noise
            Imgproc.dilate(igoThreshold, matIGOFilterNoise, kernelIGP);
            // find 4 rect edge to rotate portrait
            List<Rect> rectsEdge = findContoursRect(matIGOFilterNoise, Imgproc.RETR_EXTERNAL, 4500, 6500, 3000);
            // get 4 pont of rect edge to rotate
            Point[] pointsOfRectEdge = this.get4PointsEdge(rectsEdge);
            // do transform
            Mat matImgPortrait = this.transform4Point(1500, 1000, srcGray, pointsOfRectEdge);
            // the same, to transform answer form
            Mat matAnswerThreshold = this.adaptiveThresholdImage(matImgPortrait, 255, Imgproc.THRESH_BINARY_INV | Imgproc.THRESH_BINARY);
            Mat matAnswerFilterNoise = new Mat();
            Mat kernelAnswer = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3));
            Imgproc.dilate(matAnswerThreshold, matAnswerFilterNoise, kernelAnswer);
            List<Rect> rectsAnswerEdge = findContoursRect(matAnswerFilterNoise, Imgproc.RETR_CCOMP, 500, 800, 500);

            // get user info
            Point[] pointsEdgeOfUserInfoForm = this.get4pointsUserInfoForm(rectsAnswerEdge.subList(0, 4));
            Mat matImgUserInfoForm = this.transform4Point(300, 650, matImgPortrait, pointsEdgeOfUserInfoForm);
            Mat matThresholdUserInfo = thresholdImage(matImgUserInfoForm, 255, Imgproc.THRESH_BINARY_INV | Imgproc.THRESH_OTSU);
            List<MatOfPoint> listUserInfoBubble = new ArrayList<>();
            Imgproc.findContours(matThresholdUserInfo, listUserInfoBubble, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
            this.filterUserInfoBubbles(listUserInfoBubble, matThresholdUserInfo);
            this.sortBottomToTop(listUserInfoBubble);
            HashMap<String, String> userInfoChosen = this.getUserInfoChosen(listUserInfoBubble, matThresholdUserInfo);
            String testCode = userInfoChosen.get("testCode");
            String userCode = userInfoChosen.get("userCode");
            Test test = this.testRepository.findByCodeAndCreatedUser_Email(testCode, currentUserEmail).orElseThrow(
                    () -> APIException.incorrectData("Lỗi, không tìm thấy thông tin bài kiểm tra"));
            StudentCode studentCode = this.studentCodeRepository.findMyStudentByCode(userCode, test.getId(), currentUserEmail);
            if (studentCode == null) {
                throw APIException.incorrectData("Lỗi, học sinh chưa được thêm mới.");
            }
            this.checkInvalidStudentCodeWithTestId(studentCode, test);
            String answersTest = test.getAnswers().replace("/", "");

            // get user answer
            Point[] pointsEdgeOfAnswerForm = this.get4PointsEdge(rectsAnswerEdge.subList(4, 8));
            Mat matImgAnswerForm = this.transform4Point(500, 900, matImgPortrait, pointsEdgeOfAnswerForm);
            Mat matThresholdAnswer = thresholdImage(matImgAnswerForm, 255, Imgproc.THRESH_BINARY_INV | Imgproc.THRESH_OTSU);
            List<MatOfPoint> listAnswerBubble = new ArrayList<>();
            Mat dilateFormAnswer = new Mat();
            Mat kernelFormAnswer = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(1, 1));
            Imgproc.dilate(matThresholdAnswer, dilateFormAnswer, kernelFormAnswer);
            Imgproc.findContours(dilateFormAnswer, listAnswerBubble, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
            this.filterAnswerBubbles(listAnswerBubble, matThresholdAnswer);
            this.sortLeftToRight(listAnswerBubble);
            String[] questionResult = new String[50];
            String userAnswers = "";
            Mat distColor = new Mat();
            Imgproc.cvtColor(matImgAnswerForm, distColor, Imgproc.COLOR_GRAY2BGR);
            this.drawAndGetUserPoint(distColor, dilateFormAnswer, answersTest, questionResult, listAnswerBubble, userAnswers);
            // user's score
            float userScore = this.getUserScore(questionResult);
            long timeNow = new Date().getTime();
            String IMAGE_FILE_NAME_PREFIX = userCode + "_" + testCode;
            String imageFullFileName = IMAGE_FILE_NAME_PREFIX + "_" + timeNow + "_full.png";
            String imageAnswerFileName = IMAGE_FILE_NAME_PREFIX + "_" + timeNow + "_answer.png";
            String imageUserInfoFileName = IMAGE_FILE_NAME_PREFIX + "_" + timeNow + "_user_info.png";
            TestScore testScore = studentCode.getTestScore() != null ? studentCode.getTestScore() : new TestScore();
            testScore.setScore(userScore);
            testScore.setResult(String.join("", questionResult));
            testScore.setUserAnswers(userAnswers);
            testScore.setTestImageFullName(imageFullFileName);
            testScore.setTestImageAnswerName(imageAnswerFileName);
            testScore.setTestImageUserInfoName(imageUserInfoFileName);
            studentCode.setTestScore(testScore);
            this.studentCodeRepository.save(studentCode);
            writeFile(dstRotate, imageFullFileName);
            writeFile(distColor, imageAnswerFileName);
            writeFile(matImgUserInfoForm, imageUserInfoFileName);
            ScanResponse scanResponse = new ScanResponse();
            return SuccessResponse.ok(scanResponse.convertByStudentCode(studentCode));
        } catch (Exception e) {
            if (e.getClass() == APIException.class) {
                throw APIException.incorrectData(e.getMessage());
            }
            throw APIException.incorrectData("Lỗi, ảnh đầu vào không hợp lệ!");
        }
    }


    private String getCurrentUserEmail() {
        UserPrinciple userPrinciple = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userPrinciple.getEmail();
    }

    private void checkInvalidStudentCodeWithTestId(StudentCode studentCode, Test test) {
        if (!studentCode.getTest().getCode().equals(test.getCode())) {
            throw APIException.incorrectData("Số báo danh không hợp lệ với mã đề thi!");
        }
    }

    private float getUserScore(String[] userAnswers) {
        long correctAnswerCount = Arrays.stream(userAnswers).filter(e -> e.equals("1")).count();
        return (float) (correctAnswerCount * (1 / 5.0));
    }

    private void drawAndGetUserPoint(Mat src, Mat matThresholdAnswer,
                                     String answersCorrect,
                                     String[] questionsIncorrect,
                                     List<MatOfPoint> listAnswerBubble,
                                     String userAnswers) {
        int approx = 17;
        int questionIndex = 50;
        int colNumber = 3;
        int indexAnswerStart = 8;
        int indexAnswerEnd = 12;
        List<MatOfPoint> question34 = listAnswerBubble.subList(0, 4);
        questionsIncorrect[33] = String.valueOf(this.drawAndGetPointOfQuestion(src, question34, matThresholdAnswer,
                String.valueOf(answersCorrect.charAt(33)), userAnswers));
        List<MatOfPoint> question17 = listAnswerBubble.subList(4, 8);
        questionsIncorrect[16] = String.valueOf(this.drawAndGetPointOfQuestion(src, question17, matThresholdAnswer,
                String.valueOf(answersCorrect.charAt(16)), userAnswers));
        for (int i = 50; i >= 35; i--) {
            for (int j = 0; j < colNumber; j++) {
                List<MatOfPoint> listAnswerOfQuestion = listAnswerBubble.subList(indexAnswerStart, indexAnswerEnd);
                questionsIncorrect[questionIndex - (j * approx) - 1] = String.valueOf(
                        this.drawAndGetPointOfQuestion(src, listAnswerOfQuestion, matThresholdAnswer,
                                String.valueOf(answersCorrect.charAt(questionIndex - (j * approx) - 1)), userAnswers)
                );
                indexAnswerStart += 4;
                indexAnswerEnd += 4;
            }
            questionIndex--;
        }
    }

    private int drawAndGetPointOfQuestion(Mat src, List<MatOfPoint> data, Mat matThreshold, String questionAnswer, String userAnswers) {
        List<Integer> indexUserAnswers = new ArrayList<>();
        int indexAnswerCorrect = this.ANSWER_LABEL.indexOf(questionAnswer);
        for (int i = 0; i < data.size(); i++) {
            MatOfPoint2f approx = new MatOfPoint2f();
            MatOfPoint2f toMop2f = new MatOfPoint2f(data.get(i).toArray());
            Imgproc.approxPolyDP(toMop2f, approx, Imgproc.arcLength(toMop2f, true) * .01, true);
            Rect rect = Imgproc.boundingRect(data.get(i));
            int pixelOfContour = Core.countNonZero(matThreshold.submat(rect));
            if (pixelOfContour > 300) {
                indexUserAnswers.add(i);
            }
        }
        switch (indexUserAnswers.size()) {
            case 0:
                this.drawCircleToMat(src, data, new ArrayList<Integer>() {{
                    add(indexAnswerCorrect);
                }}, new Scalar(79, 232, 255)); //mau vang
                return 0;
            case 1:
                userAnswers += this.ANSWER_LABEL.charAt(indexUserAnswers.get(0));
                if (indexUserAnswers.get(0) == indexAnswerCorrect) {
                    this.drawCircleToMat(src, data, indexUserAnswers, new Scalar(46, 242, 98));
                    return 1;
                } else {
                    this.drawCircleToMat(src, data, indexUserAnswers, new Scalar(28, 50, 255)); //mau do
                    this.drawCircleToMat(src, data, new ArrayList<Integer>() {{
                        add(indexAnswerCorrect);
                    }}, new Scalar(79, 232, 255));
                    return 0;
                }
            default:
                this.drawCircleToMat(src, data, indexUserAnswers, new Scalar(28, 50, 255));
                this.drawCircleToMat(src, data, new ArrayList<Integer>() {{
                    add(indexAnswerCorrect);
                }}, new Scalar(79, 232, 255));
                return 0;
        }
//        if (indexUserAnswers.size() == 1) {
//            userAnswers += this.ANSWER_LABEL.charAt(indexUserAnswers.get(0));
//            if (indexUserAnswers.get(0) == indexAnswerCorrect) {
//                this.drawCircleToMat(src, data, indexUserAnswers, new Scalar(46, 242, 98));
//                return 1;
//            } else {
//                this.drawCircleToMat(src, data, indexUserAnswers, new Scalar(28, 50, 255));
//                return 0;
//            }
//        } else {
//            this.drawCircleToMat(src, data, indexUserAnswers, new Scalar(28, 50, 255));
//            this.drawCircleToMat(src, data, new ArrayList<Integer>(){{add(indexAnswerCorrect);}}, new Scalar(46, 242, 98));
//            return 0;
//        }

    }

    private void drawCircleToMat(Mat src, List<MatOfPoint> data, List<Integer> indexes, Scalar color) {
        indexes.forEach(index -> Imgproc.drawContours(src, data, index, color, 2));
    }

    private void writeFile(Mat src, String fileName) throws IOException {
        MatOfByte matOfByte = new MatOfByte();
        Imgcodecs.imencode(".png", src, matOfByte);
        byte[] bytesSrc = matOfByte.toArray();
        try (FileOutputStream fileOutputStream = new FileOutputStream(this.FILE_SCAN_RESULTS + fileName)) {
            fileOutputStream.write(bytesSrc);
        }
    }

    private Mat thresholdImage(Mat src, int thresholdNumber, int thresholdType) {
        Mat dst = new Mat();
        Imgproc.threshold(src, dst, thresholdNumber, 255, thresholdType);
        return dst;
    }

    private List<Rect> findContoursRect(Mat mathThreshold, int RETR_TYPE, int rectAreaGreater, int rectAreaLess, int pixelsOfContour) {
        List<MatOfPoint> matOfPoints = new ArrayList<>();
        Imgproc.findContours(mathThreshold, matOfPoints, new Mat(), RETR_TYPE, Imgproc.CHAIN_APPROX_SIMPLE);
        sortTopToButton(matOfPoints);
        List<Rect> rects = new ArrayList<>();
        for (MatOfPoint mop : matOfPoints) {
            MatOfPoint2f approx = new MatOfPoint2f();
            MatOfPoint2f toMop2f = new MatOfPoint2f(mop.toArray());
            Imgproc.approxPolyDP(toMop2f, approx, Imgproc.arcLength(toMop2f, true) * .01, true);
            if (Imgproc.contourArea(approx) > 100 && approx.rows() >= 4 && approx.rows() <= 6) {
                Rect rect = Imgproc.boundingRect(mop);
                double width = rect.width;
                double height = rect.height;
                boolean isRect = (width / height) < 1.5;
                int pixelOfContour = Core.countNonZero(mathThreshold.submat(rect));
                if (isRect && pixelOfContour > pixelsOfContour && rect.area() > rectAreaGreater && rect.area() < rectAreaLess) {
                    rects.add(rect);
                }
            }
        }
        return rects;
    }

    private void sortTopToButton(List<MatOfPoint> data) {
        data.sort((o1, o2) -> {
            Rect rect1 = Imgproc.boundingRect(o1);
            Rect rect2 = Imgproc.boundingRect(o2);
            return Double.compare(rect1.tl().y, rect2.tl().y);
        });
    }

    private Point[] get4PointsEdge(List<Rect> rectangles) {
        Point[] points = new Point[4];
        Rect rectXMax = rectangles.get(0);
        Rect rectXMin = rectangles.get(0);
        Rect rectYMax = rectangles.get(0);
        Rect rectYMin = rectangles.get(0);
        for (Rect rect : rectangles) {
            if (((rectXMax.tl().x + rectXMax.br().x) / 2) < ((rect.tl().x + rect.br().x) / 2)) {
                rectXMax = rect;
            }
            if (((rectXMin.tl().x + rectXMin.br().x) / 2) > ((rect.tl().x + rect.br().x) / 2)) {
                rectXMin = rect;
            }
            if (((rectYMax.tl().y + rectYMax.br().y) / 2) < ((rect.tl().y + rect.br().y) / 2)) {
                rectYMax = rect;
            }
            if (((rectYMin.tl().y + rectYMin.br().y) / 2) > ((rect.tl().y + rect.br().y) / 2)) {
                rectYMin = rect;
            }
        }
        double xAverage = (((rectXMax.tl().x + rectXMax.br().x) / 2) + (rectXMin.tl().x + rectXMin.br().x) / 2) / 2;
        double yAverage = (((rectYMax.tl().y + rectYMax.br().y) / 2) + (rectYMin.tl().y + rectYMin.br().y) / 2) / 2;
        for (Rect rect : rectangles) {
            if (((rect.tl().x + rect.br().x) / 2) < xAverage && ((rect.tl().y + rect.br().y) / 2) < yAverage) {
                points[2] = rect.br();
            }
            if (((rect.tl().x + rect.br().x) / 2) > xAverage && ((rect.tl().y + rect.br().y) / 2) < yAverage) {
                points[3] = new Point(rect.tl().x, rect.br().y);
            }
            if (((rect.tl().x + rect.br().x) / 2) < xAverage && ((rect.tl().y + rect.br().y) / 2) > yAverage) {
                points[1] = new Point(rect.br().x, rect.tl().y);
            }
            if (((rect.tl().x + rect.br().x) / 2) > xAverage && ((rect.tl().y + rect.br().y) / 2) > yAverage) {
                points[0] = rect.tl();
            }
        }
        return points;
    }

    private Mat transform4Point(int row, int col, Mat src, Point[] pointsEdge) {
        Mat destImage = new Mat(row, col, src.type());
        Mat src_mat = new MatOfPoint2f(pointsEdge[0], pointsEdge[1], pointsEdge[2], pointsEdge[3]);
        Mat dst = new MatOfPoint2f(new Point(destImage.width() - 1, destImage.height() - 1), new Point(0, destImage.height() - 1), new Point(0, 0), new Point(destImage.width() - 1, 0));
        Mat transform = Imgproc.getPerspectiveTransform(src_mat, dst);
        Imgproc.warpPerspective(src, destImage, transform, destImage.size());
        return destImage;
    }

    private Mat adaptiveThresholdImage(Mat src, int thresholdNumber, int thresholdType) {
        Mat dst = new Mat();
        Imgproc.adaptiveThreshold(src, dst, thresholdNumber, Imgproc.ADAPTIVE_THRESH_MEAN_C,
                thresholdType, 59, 18);
        return dst;
    }

    private Point[] get4pointsUserInfoForm(List<Rect> rectangles) {
        Point[] points = new Point[4];
        Rect rectXMax = rectangles.get(0);
        Rect rectXMin = rectangles.get(0);
        Rect rectYMax = rectangles.get(0);
        Rect rectYMin = rectangles.get(0);
        for (Rect rect : rectangles) {
            if (((rectXMax.tl().x + rectXMax.br().x) / 2) < ((rect.tl().x + rect.br().x) / 2)) {
                rectXMax = rect;
            }
            if (((rectXMin.tl().x + rectXMin.br().x) / 2) > ((rect.tl().x + rect.br().x) / 2)) {
                rectXMin = rect;
            }
            if (((rectYMax.tl().y + rectYMax.br().y) / 2) < ((rect.tl().y + rect.br().y) / 2)) {
                rectYMax = rect;
            }
            if (((rectYMin.tl().y + rectYMin.br().y) / 2) > ((rect.tl().y + rect.br().y) / 2)) {
                rectYMin = rect;
            }
        }
        points[0] = new Point(rectXMax.tl().x, rectXMax.tl().y);
        points[1] = new Point(rectXMin.br().x, rectXMax.tl().y);
        points[2] = new Point(rectXMin.br().x, rectXMin.br().y);
        points[3] = new Point(rectXMax.tl().x, rectXMin.br().y);
        return points;
    }

    private void sortLeftToRight(List<MatOfPoint> data) {
        for (int i = 0; i < data.size() - 1; i++) {
            for (int j = i + 1; j < data.size(); j++) {
                Rect rect1 = Imgproc.boundingRect(data.get(i));
                Rect rect2 = Imgproc.boundingRect(data.get(j));
                if (isBubbleXLessThan(rect1, rect2)) {
                    MatOfPoint mopTemp = data.get(i);
                    data.set(i, data.get(j));
                    data.set(j, mopTemp);
                }
            }
        }
    }

    private void sortBottomToTop(List<MatOfPoint> data) {
        for (int i = 0; i < data.size() - 1; i++) {
            for (int j = i + 1; j < data.size(); j++) {
                Rect rect1 = Imgproc.boundingRect(data.get(i));
                Rect rect2 = Imgproc.boundingRect(data.get(j));
                if (isBubbleYLessThan(rect1, rect2)) {
                    MatOfPoint mopTemp = data.get(i);
                    data.set(i, data.get(j));
                    data.set(j, mopTemp);
                }
            }
        }
    }

    private boolean isBubbleXLessThan(Rect rect1, Rect rect2) {
        int tolerance = 9;
        if (rect1.tl().y + tolerance < rect2.tl().y)
            return true;
        if (rect2.tl().y + tolerance < rect1.tl().y)
            return false;
        return (rect1.tl().x < rect2.tl().x);
    }

    private static boolean isBubbleYLessThan(Rect rect1, Rect rect2) {
        int tolerance = 9;
        if (rect1.tl().x + tolerance < rect2.tl().x)
            return true;
        if (rect2.tl().x + tolerance < rect1.tl().x)
            return false;
        return (rect1.tl().y < rect2.tl().y);
    }

    private boolean filterAnswerBubbles(List<MatOfPoint> listCircle, Mat mathThreshold) {
        return listCircle.removeIf(e -> {
            MatOfPoint2f approx = new MatOfPoint2f();
            MatOfPoint2f toMop2f = new MatOfPoint2f(e.toArray());
            Imgproc.approxPolyDP(toMop2f, approx, Imgproc.arcLength(toMop2f, true) * .01, true);
            Rect rect = Imgproc.boundingRect(e);
            int pixelOfContour = Core.countNonZero(mathThreshold.submat(rect));
            return approx.rows() < 7 || pixelOfContour < 130;
        });
    }

    private boolean filterUserInfoBubbles(List<MatOfPoint> listCircle, Mat mathThreshold) {
        return listCircle.removeIf(e -> {
            MatOfPoint2f approx = new MatOfPoint2f();
            MatOfPoint2f toMop2f = new MatOfPoint2f(e.toArray());
            Imgproc.approxPolyDP(toMop2f, approx, Imgproc.arcLength(toMop2f, true) * .01, true);
            Rect rect = Imgproc.boundingRect(e);
            int pixelOfContour = Core.countNonZero(mathThreshold.submat(rect));
            return approx.rows() < 7 || pixelOfContour < 200;
        });
    }

    private HashMap<Integer, String> getListAnswerChosen(List<MatOfPoint> data, Mat matThreshold) {
        int approx = 17;
        int questionIndex = 50;
        int colNumber = 3;
        int indexAnswerStart = 8;
        int indexAnswerEnd = 12;
        HashMap<Integer, String> result = new HashMap<>();
        List<MatOfPoint> question34 = data.subList(0, 4);
        result.put(34, this.getAnswerChosen(question34, matThreshold));
        List<MatOfPoint> question17 = data.subList(4, 8);
        result.put(17, this.getAnswerChosen(question17, matThreshold));
        for (int i = 50; i >= 35; i--) {
            for (int j = 0; j < colNumber; j++) {
                List<MatOfPoint> listAnswerOfQuestion = data.subList(indexAnswerStart, indexAnswerEnd);
                result.put(questionIndex - (j * approx), this.getAnswerChosen(listAnswerOfQuestion, matThreshold));
                indexAnswerStart += 4;
                indexAnswerEnd += 4;
            }
            questionIndex--;
        }
        return result;
    }

    private HashMap<String, String> getUserInfoChosen(List<MatOfPoint> data, Mat matThreshold) {
        int indexBubbleStart = 0;
        int indexBubbleEnd = 10;
        int indexBubbleTestCodeEnd = 40;
        HashMap<String, String> result = new HashMap<>();
        StringBuilder userCode = new StringBuilder();
        StringBuilder testCode = new StringBuilder();
        for (int i = 0; i < 11; i++) {
            List<MatOfPoint> listAnswerOfQuestion = data.subList(indexBubbleStart, indexBubbleEnd);
            String label = getUserInfoChosenLabel(listAnswerOfQuestion, matThreshold);
            boolean isTestCodeLabel = indexBubbleEnd <= indexBubbleTestCodeEnd;
            if (isTestCodeLabel) {
                if (!label.equals("")) {
                    testCode.insert(0, label);
                }
            } else {
                if (!label.equals("")) {
                    userCode.insert(0, label);
                }
            }
            indexBubbleStart += 10;
            indexBubbleEnd += 10;
        }
        result.put("userCode", userCode.toString());
        result.put("testCode", testCode.toString());
        return result;
    }

    private String getAnswerChosen(List<MatOfPoint> data, Mat matThreshold) {
        int indexChosen = -1;
        for (int i = 0; i < data.size(); i++) {
            MatOfPoint2f approx = new MatOfPoint2f();
            MatOfPoint2f toMop2f = new MatOfPoint2f(data.get(i).toArray());
            Imgproc.approxPolyDP(toMop2f, approx, Imgproc.arcLength(toMop2f, true) * .01, true);
            Rect rect = Imgproc.boundingRect(data.get(i));
            int pixelOfContour = Core.countNonZero(matThreshold.submat(rect));
            if (pixelOfContour > 387) {
                indexChosen = i;
                break;
            }
        }
        return String.valueOf(this.ANSWER_LABEL.charAt(indexChosen));
    }

    private String getUserInfoChosenLabel(List<MatOfPoint> data, Mat matThreshold) {
        List<Integer> indexChosen = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            MatOfPoint2f approx = new MatOfPoint2f();
            MatOfPoint2f toMop2f = new MatOfPoint2f(data.get(i).toArray());
            Imgproc.approxPolyDP(toMop2f, approx, Imgproc.arcLength(toMop2f, true) * .01, true);
            Rect rect = Imgproc.boundingRect(data.get(i));
            int pixelOfContour = Core.countNonZero(matThreshold.submat(rect));
            System.out.println(pixelOfContour);
            if (pixelOfContour > 400) {
                indexChosen.add(i);
                break;
            }
        }
        return convertIndexToUserInfoLabel(indexChosen);
    }

    private String convertIndexToAnswerLabel(int index) {
        switch (index) {
            case 0:
                return "D";
            case 1:
                return "C";
            case 2:
                return "B";
            case 3:
                return "A";
        }
        return null;
    }

    private String convertIndexToUserInfoLabel(List<Integer> indexes) {
        StringBuilder userInfoLabel = new StringBuilder();
        for (Integer index : indexes) {
            switch (index) {
                case 0:
                    userInfoLabel.append("9");
                    break;
                case 1:
                    userInfoLabel.append("8");
                    break;
                case 2:
                    userInfoLabel.append("7");
                    break;
                case 3:
                    userInfoLabel.append("6");
                    break;
                case 4:
                    userInfoLabel.append("5");
                    break;
                case 5:
                    userInfoLabel.append("4");
                    break;
                case 6:
                    userInfoLabel.append("3");
                    break;
                case 7:
                    userInfoLabel.append("2");
                    break;
                case 8:
                    userInfoLabel.append("1");
                    break;
                case 9:
                    userInfoLabel.append("0");
                    break;
            }
        }
        return userInfoLabel.toString();
    }


    @Override
    public SuccessResponse myHistories() {
        UserPrinciple userPrinciple = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<StudentCode> studentCodes = this.studentCodeRepository.findAllByTestScoreIsNotNullAndTest_CreatedUser_Email(userPrinciple.getEmail());
        List<ScanResponse> scanResponses = new ArrayList<>();
        studentCodes.forEach(e -> {
            scanResponses.add(new ScanResponse(e));
        });
        scanResponses.sort(new Comparator<ScanResponse>() {
            @Override
            public int compare(ScanResponse o1, ScanResponse o2) {
                return o1.getCreatedTime().compareTo(o2.getCreatedTime());
            }
        }.reversed());
        return SuccessResponse.ok(scanResponses);
    }
}
