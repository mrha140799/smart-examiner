package com.siten.backend.domain.usecase.impl;

import com.siten.backend.core.exception.APIException;
import com.siten.backend.data.entity.Classroom;
import com.siten.backend.data.entity.Student;
import com.siten.backend.data.repository.ClassroomRepository;
import com.siten.backend.domain.usecase.ClassroomService;
import com.siten.backend.model.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClassroomServiceImpl implements ClassroomService {
    @Autowired
    private ClassroomRepository classroomRepository;

    @Override
    public SuccessResponse createOne(Classroom classroom) {
        this.classroomRepository.save(classroom);
        return SuccessResponse.created(classroom);
    }

    @Override
    public SuccessResponse deleteById(Long id) {
        Classroom classroom = this.classroomRepository.findById(id).orElseThrow(() -> APIException.incorrectData("Lỗi, Không tìm thấy thông tin!"));
        this.classroomRepository.delete(classroom);
        return SuccessResponse.ok();
    }

    @Override
    public SuccessResponse findOneById(Long id) {
        Classroom classroom = this.classroomRepository.findById(id).orElseThrow(() -> APIException.incorrectData("Lỗi, Không tìm thấy thông tin!"));
        return SuccessResponse.ok(classroom);
    }

    @Override
    public SuccessResponse updateOne(Classroom classroom) {
        Classroom classroomModel = this.classroomRepository.findById(classroom.getId()).orElseThrow(() -> APIException.incorrectData("Lỗi, Không tìm thấy thông tin!"));
        classroomModel.setName(classroom.getName());
        classroomModel.setSchool(classroom.getSchool());
        this.classroomRepository.save(classroomModel);
        return SuccessResponse.ok();
    }
}
