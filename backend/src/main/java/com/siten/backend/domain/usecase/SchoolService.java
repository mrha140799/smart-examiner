package com.siten.backend.domain.usecase;

import com.siten.backend.data.entity.School;
import com.siten.backend.model.SuccessResponse;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface SchoolService {
    SuccessResponse createSchool(School school);
    SuccessResponse updateSchool(School school);
    SuccessResponse findSchoolById(Long schoolId);
    SuccessResponse findAllSchool(Pageable pageable);
    SuccessResponse deleteSchool(Long id);
    SuccessResponse findAllByAddress(String address);
    SuccessResponse searchSchool(Long idEqual, List<Long> idIn, String nameEqual, List<String> nameIn);
}
