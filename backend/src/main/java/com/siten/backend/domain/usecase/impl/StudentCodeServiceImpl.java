package com.siten.backend.domain.usecase.impl;

import com.siten.backend.core.auth.UserPrinciple;
import com.siten.backend.core.exception.APIException;
import com.siten.backend.data.entity.Classroom;
import com.siten.backend.data.entity.Student;
import com.siten.backend.data.entity.StudentCode;
import com.siten.backend.data.entity.Test;
import com.siten.backend.data.repository.ClassroomRepository;
import com.siten.backend.data.repository.StudentCodeRepository;
import com.siten.backend.data.repository.StudentRepository;
import com.siten.backend.data.repository.TestRepository;
import com.siten.backend.domain.usecase.StudentCodeService;
import com.siten.backend.model.StudentCodeResponse;
import com.siten.backend.model.SuccessResponse;
import com.siten.backend.model.request.StudentCodeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentCodeServiceImpl implements StudentCodeService {
    @Autowired
    private StudentCodeRepository studentCodeRepository;
    @Autowired
    private ClassroomRepository classroomRepository;
    @Autowired
    private TestRepository testRepository;
    @Autowired
    private StudentRepository studentRepository;

    @Override
    public SuccessResponse getAllMyStudentCodesByTestId(Long testId) {
        UserPrinciple userPrinciple = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<StudentCode> studentCodes = this.studentCodeRepository.findAllByTest_IdAndStudent_Classroom_School_CreatedUser_Email(testId, userPrinciple.getEmail());
        List<StudentCodeResponse> studentCodeResponses = new ArrayList<>();
        studentCodes.forEach(e -> {
            studentCodeResponses.add(new StudentCodeResponse(e));
        });
        return SuccessResponse.ok(studentCodeResponses);
    }

    @Override
    public SuccessResponse createOne(StudentCodeRequest studentCodeRequest) {
        Test test = this.testRepository.findById(studentCodeRequest.getTestId()).orElseThrow(() -> APIException.incorrectData("Lỗi, không tìm thấy thông tin bài kiểm tra"));
        Student student = this.studentRepository.findById(studentCodeRequest.getStudentId()).orElseThrow(() -> APIException.incorrectData("Lỗi, Không tìm thấy thông tin sinh viên!"));
        if (this.studentCodeRepository.existsByStudent_IdAndTest_Id(studentCodeRequest.getStudentId(), test.getId())) {
            SuccessResponse successResponse = new SuccessResponse();
            successResponse.setStatusCode(-201);
            successResponse.setMessage("Lỗi, thí sinh đã tồn tại trong bài kiểm tra!");
            return successResponse;
        }
        if (this.studentCodeRepository.existsByCodeAndTest_Id(studentCodeRequest.getCode(), test.getId())) {
            SuccessResponse successResponse = new SuccessResponse();
            successResponse.setStatusCode(-200);
            successResponse.setMessage("Lỗi, đã tồn tại số báo danh tương ứng với mã đề thi!");
            return successResponse;
        }
        StudentCode studentCode = new StudentCode();
        studentCode.setStudent(student);
        studentCode.setCode(studentCodeRequest.getCode());
        studentCode.setTest(test);
        this.studentCodeRepository.save(studentCode);
        return SuccessResponse.created(new StudentCodeResponse(studentCode));
    }

    @Override
    public SuccessResponse updateOne(StudentCodeRequest studentCodeRequest) {
        Test test = this.testRepository.findById(studentCodeRequest.getTestId()).orElseThrow(() -> APIException.incorrectData("Lỗi, không tìm thấy thông tin bài kiểm tra"));
        StudentCode studentCode = this.studentCodeRepository.findById(studentCodeRequest.getId()).orElseThrow(() -> APIException.incorrectData("Lỗi, không tìm thấy thông tin thí sinh!"));
        if (!studentCodeRequest.getCode().equals(studentCode.getCode())) {
            if (this.studentCodeRepository.existsByCodeAndTest_Id(studentCodeRequest.getCode(), test.getId())) {
                SuccessResponse successResponse = new SuccessResponse();
                successResponse.setStatusCode(-200);
                successResponse.setMessage("Lỗi, đã tồn tại số báo danh tương ứng với mã đề thi!");
                return successResponse;
            }
        }
        studentCode.setCode(studentCodeRequest.getCode());
        studentCode.setTest(test);
        this.studentCodeRepository.save(studentCode);
        return SuccessResponse.created(new StudentCodeResponse(studentCode));
    }

    @Override
    public SuccessResponse existsByStudentCodeAndTestId(String studentCode, Long testId) {
        if (this.studentCodeRepository.existsByCodeAndTest_Id(studentCode, testId)) {
            throw APIException.incorrectData("Lỗi, đã tồn tại số báo danh tương ứng với mã đề thi!");
        }
        return SuccessResponse.ok();
    }

    @Override
    public SuccessResponse getOneById(Long studentCodeId) {
        StudentCode studentCode = this.studentCodeRepository.findById(studentCodeId).orElseThrow(() -> APIException.incorrectData("Lỗi, không tìm thấy thí sinh!"));
        StudentCodeRequest studentCodeRequest = new StudentCodeRequest();
        studentCodeRequest.setId(studentCode.getId());
        studentCodeRequest.setCode(studentCode.getCode());
        studentCodeRequest.setStudentId(studentCode.getStudent().getId());
        studentCodeRequest.setTestId(studentCode.getTest().getId());
        studentCode.setCode(studentCode.getCode());
        return SuccessResponse.ok(studentCodeRequest);
    }

    @Override
    public SuccessResponse deleteById(Long studentCodeId) {
        this.studentCodeRepository.deleteById(studentCodeId);
        return SuccessResponse.ok();
    }

    @Override
    public SuccessResponse test() {
        return SuccessResponse.ok(this.studentCodeRepository.getAllStudentCodeDTO());
    }
}
