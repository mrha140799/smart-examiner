package com.siten.backend.domain.usecase.impl;

import com.siten.backend.core.exception.APIException;
import com.siten.backend.data.entity.Classroom;
import com.siten.backend.data.entity.Student;
import com.siten.backend.data.repository.ClassroomRepository;
import com.siten.backend.data.repository.StudentRepository;
import com.siten.backend.domain.usecase.StudentService;
import com.siten.backend.model.SuccessResponse;
import com.siten.backend.model.request.StudentsRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private ClassroomRepository classroomRepository;

    @Override
    public SuccessResponse createOne(Student student, Long classroomId) {
        Classroom classroom = this.classroomRepository.findById(classroomId).orElseThrow(() -> APIException.incorrectData("Lỗi, dữ liệu không chính xác!"));
        student.setClassroom(classroom);
        this.studentRepository.save(student);
        return SuccessResponse.created(student);
    }

    @Override
    public SuccessResponse updateOne(Student student, Long classroomId) {
        Classroom classroom = this.classroomRepository.findById(classroomId).orElseThrow(() -> APIException.incorrectData("Lỗi, dữ liệu không chính xác!"));
        student.setClassroom(classroom);
        this.studentRepository.save(student);
        return SuccessResponse.ok(student);
    }

    @Override
    public SuccessResponse createMany(StudentsRequest studentsRequest) {
        Classroom classroom = this.classroomRepository.findById(studentsRequest.getClassroomId()).orElseThrow(() -> APIException.incorrectData("Lỗi, dữ liệu không chính xác!"));
        List<Student> students = new ArrayList<>();
        studentsRequest.getStudents().forEach(e -> {
            e.setClassroom(classroom);
            students.add(e);
        });
        this.studentRepository.saveAll(students);
        return SuccessResponse.ok();
    }

    @Override
    public SuccessResponse findOneById(Long id) {
        return null;
    }

    @Override
    public SuccessResponse getAllByClassId(Long classId) {
        List<Student> students = this.studentRepository.findAllByClassroom_Id(classId);
        return SuccessResponse.ok(students);
    }

    @Override
    public SuccessResponse deleteById(Long id) {
        this.studentRepository.deleteById(id);
        return SuccessResponse.ok();
    }
}
