package com.siten.backend.domain;

import com.siten.backend.data.AuthorityName;
import com.siten.backend.data.entity.Authority;
import com.siten.backend.data.repository.AuthorityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class InitDataListener implements ApplicationRunner {
    @Autowired
    private AuthorityRepository authorityRepository;
    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (!authorityRepository.existsByName(AuthorityName.ROLE_ADMIN.getValue())) {
            authorityRepository.save(new Authority(AuthorityName.ROLE_ADMIN));
        }
        if (!authorityRepository.existsByName(AuthorityName.ROLE_USER.getValue())) {
            authorityRepository.save(new Authority(AuthorityName.ROLE_USER));
        }
        if (!authorityRepository.existsByName(AuthorityName.ROLE_ADMIN_SYSTEM.getValue())) {
            authorityRepository.save(new Authority(AuthorityName.ROLE_ADMIN_SYSTEM));
        }
    }
}
