package com.siten.backend.domain.usecase.impl;

import com.siten.backend.core.exception.APIException;
import com.siten.backend.data.entity.Authority;
import com.siten.backend.data.entity.User;
import com.siten.backend.data.repository.AuthorityRepository;
import com.siten.backend.data.repository.UserRepository;
import com.siten.backend.domain.usecase.AdminService;
import com.siten.backend.model.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AuthorityRepository authorityRepository;
    @Override
    public SuccessResponse changeUserAuthorities(String email, List<String> authorities) {
        User user = this.userRepository.findByEmail(email).orElseThrow(() -> APIException.incorrectData("Email không tồn tại!"));
        Set<Authority> authoritiesUpdate = new HashSet<>();
        for (String authority: authorities) {
            Authority auth = this.authorityRepository.findByName(authority);
            if (auth == null) {
                throw APIException.incorrectData("Dữ liệu nhập không chính xác!");
            }
            authoritiesUpdate.add(auth);
        }
        user.setAuthorities(authoritiesUpdate);
        this.userRepository.save(user);
        return SuccessResponse.ok();
    }
}
