package com.siten.backend.domain.usecase;

import com.siten.backend.model.SuccessResponse;

import java.util.List;

public interface AdminService {
    SuccessResponse changeUserAuthorities(String email, List<String> authorities);
}
