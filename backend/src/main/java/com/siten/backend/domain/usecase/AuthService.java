package com.siten.backend.domain.usecase;

import com.siten.backend.model.SuccessResponse;
import com.siten.backend.model.request.UserDTO;

public interface AuthService {
    SuccessResponse register(UserDTO userDTO);
    SuccessResponse login(String email, String password);
    SuccessResponse isValidEmail(String email);
}
