package com.siten.backend.domain.usecase.impl;

import com.siten.backend.core.auth.JwtProvider;
import com.siten.backend.core.auth.MyAuthenticationProvider;
import com.siten.backend.core.auth.UserPrinciple;
import com.siten.backend.core.exception.APIException;
import com.siten.backend.data.AuthorityName;
import com.siten.backend.data.entity.Authority;
import com.siten.backend.data.entity.User;
import com.siten.backend.data.repository.AuthorityRepository;
import com.siten.backend.data.repository.UserRepository;
import com.siten.backend.model.SuccessResponse;
import com.siten.backend.model.request.UserDTO;
import com.siten.backend.domain.usecase.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.HashSet;

@Service
public class AuthServiceImpl implements AuthService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AuthorityRepository authorityRepository;
    @Autowired
    private MyAuthenticationProvider authenticationProvider;
    @Autowired
    private JwtProvider jwtProvider;

    public SuccessResponse register(UserDTO userDTO) {
        if (this.userRepository.existsByEmail(userDTO.getEmail())) {
            throw  APIException.incorrectData("Email is exists!");
        }
        User user = userDTO.toUser();
        user.setPassword(this.passwordEncoder.encode(userDTO.getPassword()));
        Authority authority = this.authorityRepository.findByName(AuthorityName.ROLE_USER.getValue());
        user.setAuthorities(new HashSet<Authority>(){{
            add(authority);
        }});
        this.userRepository.save(user);
        HashMap<String, String> response = new HashMap<String, String>() {{
            put("email", user.getEmail());
        }};
        return SuccessResponse.created(response);
    }

    @Override
    public SuccessResponse login(String email, String password) {
        UsernamePasswordAuthenticationToken userLogin = new UsernamePasswordAuthenticationToken(email.toLowerCase(), password);
        Authentication authentication = authenticationProvider.authenticate(userLogin);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtProvider.generateTokenByUsername(email.toLowerCase());
        UserPrinciple userPrinciple = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        HashMap<String, Object> response = new HashMap<String, Object>(){{
            put("token", token);
            put("userInfo", userPrinciple);
        }};
        return SuccessResponse.ok(response);
    }

    @Override
    public SuccessResponse isValidEmail(String email) {
        boolean isValid = !this.userRepository.existsByEmail(email);
        if (!isValid)
            throw APIException.incorrectData("Email đã tồn tại, vui lòng thử email khác");
        return SuccessResponse.ok();
    }
}
