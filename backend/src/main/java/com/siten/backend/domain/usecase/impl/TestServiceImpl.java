package com.siten.backend.domain.usecase.impl;

import com.siten.backend.core.auth.UserPrinciple;
import com.siten.backend.core.exception.APIException;
import com.siten.backend.data.entity.Test;
import com.siten.backend.data.entity.User;
import com.siten.backend.data.repository.TestRepository;
import com.siten.backend.data.repository.UserRepository;
import com.siten.backend.domain.usecase.TestService;
import com.siten.backend.model.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl implements TestService {
    @Autowired
    private TestRepository testRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public SuccessResponse createOne(Test test) {
        UserPrinciple userPrinciple = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        boolean validCode = this.testRepository.existsByCodeAndCreatedUser_Email(test.getCode(), userPrinciple.getEmail());
        if (validCode) {
            throw APIException.incorrectData("Lỗi, Mã đề thi đã tồn tại!");
        }
        User user = this.userRepository.findByEmail(userPrinciple.getEmail()).get();
        test.setCreatedUser(user);
        this.testRepository.save(test);
        return SuccessResponse.created(test);
    }

    @Override
    public SuccessResponse updateTest(Test test) {
        Test originalTest = this.testRepository.findById(test.getId()).orElseThrow(() -> APIException.incorrectData("Lỗi, không tìm thấy thông tin bài test!"));
        originalTest.setSubject(test.getSubject());
        originalTest.setCode(test.getCode());
        originalTest.setTestDay(test.getTestDay());
        originalTest.setAnswers(test.getAnswers());
        this.testRepository.save(originalTest);
        return SuccessResponse.ok();
    }

    @Override
    public SuccessResponse isValidTestCode(String testCode, Long testId) {
        UserPrinciple userPrinciple = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        boolean invalid;
        if (testId == null) {
            invalid = this.testRepository.existsByCodeAndCreatedUser_Email(testCode, userPrinciple.getEmail());
        } else {
            invalid = this.testRepository.existsByCodeAndIdNotAndCreatedUser_Email(testCode, testId, userPrinciple.getEmail());
        }
        if (invalid) {
            throw APIException.incorrectData("Lỗi, mã đề thi đã tồn tại!");
        }
        return SuccessResponse.ok();
    }

    @Override
    public SuccessResponse getById(Long id) {
        Test test = this.testRepository.findById(id).orElseThrow(() -> APIException.incorrectData("Lỗi, Không tìm thấy bài thi!"));
        return SuccessResponse.ok(test);
    }

    @Override
    public SuccessResponse deleteById(Long id) {
        this.testRepository.deleteById(id);
        return SuccessResponse.ok();
    }
}
