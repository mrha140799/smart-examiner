package com.siten.backend.domain.usecase.impl;

import com.siten.backend.core.auth.Commons;
import com.siten.backend.core.auth.UserPrinciple;
import com.siten.backend.core.exception.APIException;
import com.siten.backend.data.entity.School;
import com.siten.backend.data.entity.User;
import com.siten.backend.data.repository.SchoolRepository;
import com.siten.backend.data.repository.UserRepository;
import com.siten.backend.domain.usecase.SchoolService;
import com.siten.backend.model.PageResponse;
import com.siten.backend.model.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;


@Service
public class SchoolServiceImpl implements SchoolService {
    @Autowired
    private SchoolRepository schoolRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public SuccessResponse createSchool(School school) {
        UserPrinciple userPrinciple = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userRepository.findByEmail(userPrinciple.getEmail()).get();
        school.setCreatedUser(user);
        schoolRepository.save(school);
        return SuccessResponse.created(school.getName());
    }

    @Override
    public SuccessResponse updateSchool(School school) {
        School schoolModel = schoolRepository.findById(school.getId()).orElseThrow(() -> APIException.incorrectData("Lỗi, Không tìm thấy thông tin!"));
        schoolModel.setName(school.getName());
        schoolModel.setAddress(school.getAddress());
        schoolRepository.save(schoolModel);
        return SuccessResponse.ok();
    }

    @Override
    public SuccessResponse findSchoolById(Long schoolId) {
        UserPrinciple userPrinciple = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        School school;
        if (userPrinciple.getRoles().contains(Commons.ROLE_ADMIN_SYSTEM_NAME)) {
            school = schoolRepository.findById(schoolId).orElseThrow(() -> APIException.incorrectData("Lỗi, Không tìm thấy thông tin!"));
        } else {
            school = schoolRepository.findByIdAndCreatedUser_Email(schoolId, userPrinciple.getEmail()).orElseThrow(() -> APIException.incorrectData("Lỗi, Không tìm thấy thông tin!"));
        }
        return SuccessResponse.ok(school);
    }

    @Override
    public SuccessResponse findAllSchool(Pageable pageable) {
        Page<School> schools = schoolRepository.findAll(pageable);
        PageResponse<School> schoolPageResponse = new PageResponse<>();
        schoolPageResponse.getPageResponse(schools);
        return SuccessResponse.ok(schoolPageResponse);
    }

    @Override
    public SuccessResponse deleteSchool(Long id) {
        this.schoolRepository.deleteById(id);
        return SuccessResponse.ok();
    }

    @Override
    public SuccessResponse findAllByAddress(String address) {
        return SuccessResponse.ok(this.schoolRepository.findAll(SchoolRepository.hasAddress(address)));
    }

    @Override
    public SuccessResponse searchSchool(Long idEqual, List<Long> idIn, String nameEqual, List<String> nameIn) {
        List<School> schools = new ArrayList<>();
        schools = this.schoolRepository.findAll(schoolSpecification(idEqual, idIn, nameEqual, nameIn));
        return SuccessResponse.ok(schools);
    }

    private Specification<School> schoolSpecification(Long idEqual, List<Long> idIn, String nameEqual, List<String> nameIn) {
        Specification<School> specification = Specification.where((root, criteriaQuery, criteriaBuilder) -> {
            criteriaQuery.distinct(true);
            return null;
        });
        if (idEqual != null) {
            specification = specification.and((school, cq, cb) -> {
                return cb.equal(school.get("id"), idEqual);
            });
        }
        if (idIn != null) {
            specification = specification.and((school, cq, cb) -> {
                return school.get("id").in(idIn);
            });
        }
        if (nameEqual != null) {
            specification = specification.and((school, cq, cb) -> {
                return cb.equal(school.get("name"), idEqual);
            });
        }
        if (nameIn != null) {
            specification = specification.and((school, cq, cb) -> {
                return school.get("name").in(nameIn);
            });
        }
        return specification;
    }
}
