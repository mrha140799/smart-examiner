package com.siten.backend.domain;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfPoint;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class OpenCVService {
    @Value("${file.director.output}")
    private String DIRECTOR_OUT_PUT_FILE;

    public void test() {
        Mat src = Imgcodecs.imread("./files/rotage.png", Imgcodecs.IMREAD_GRAYSCALE);
        Mat matOut = new Mat();
        MatOfPoint matOfPoint = new MatOfPoint();
        List<MatOfPoint> contours = new ArrayList<>();
        Imgproc.findContours(src, contours, matOut, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

        Point[] retangle4Corner = new Point[4];
        for (int i = 0; i < src.height(); i++) {
            for (int j = 0; j < src.width(); j++) {
                double[] data = src.get(i, j);
                System.out.println(data[0]);
            }
        }
        MatOfByte matOfByte = new MatOfByte();
        Imgcodecs.imencode(".png", src, matOfByte);
        byte[] bytes = matOfByte.toArray();
        if (this.writeFileByBytes(bytes, "input.png")) {
            System.out.println("----------> wrote file successfully!");
        }
    }

    private boolean writeFileByBytes(byte[] byteArray, String fileName) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(this.DIRECTOR_OUT_PUT_FILE + fileName);
            fileOutputStream.write(byteArray);
            return true;
        } catch (IOException ex) {
            System.out.println("error: " + ex.getMessage());
            return false;
        }
    }

    private boolean writeFileBufferImage(BufferedImage bufferedImage, String fileName) {
        try {
            File outputFile = new File(this.DIRECTOR_OUT_PUT_FILE + fileName);
            ImageIO.write(bufferedImage, "jpg", outputFile);
            return true;
        } catch (IOException ex) {
            System.out.println("error: " + ex.getMessage());
            return false;
        }
    }
}
