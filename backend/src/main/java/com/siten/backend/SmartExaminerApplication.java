package com.siten.backend;

import com.siten.backend.core.auth.ApplicationProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@EnableConfigurationProperties({ ApplicationProperties.class})
@EnableScheduling
@EntityScan( basePackages = {"com.siten.backend"})
public class SmartExaminerApplication implements WebMvcConfigurer {

    public static void main(String[] args) {
        SpringApplication.run(SmartExaminerApplication.class, args);
        nu.pattern.OpenCV.loadLocally();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/avatar/**")
                .addResourceLocations("file:./files/avatar/");
        registry
                .addResourceHandler("/scan-result/**")
                .addResourceLocations("file:./files/scan_results/");
    }
}
