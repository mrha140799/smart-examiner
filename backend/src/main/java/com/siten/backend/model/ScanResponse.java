package com.siten.backend.model;

import com.siten.backend.data.entity.Student;
import com.siten.backend.data.entity.StudentCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class ScanResponse {
    private String studentFullName;
    private String studentCode;
    private String testCode;
    private float score;
    private String numberQuestionCorrect;
    private String subjectName;
    private Date testDay;
    private String imageFullFileName;
    private String imageAnswerFileName;
    private String imageUserInfoFileName;
    private Date createdTime;

    public ScanResponse(StudentCode studentCode) {
        String questionsIncorrect = studentCode.getTestScore().getResult();
        long numberQuestionCorrect = questionsIncorrect.chars().filter(e -> e == '1').count();
        this.studentFullName = studentCode.getStudent() != null ? studentCode.getStudent().getFullName() : "";
        this.studentCode = studentCode.getCode();
        this.testCode = studentCode.getTest().getCode();
        this.score = studentCode.getTestScore().getScore();
        this.numberQuestionCorrect = numberQuestionCorrect + "/50";
        this.subjectName = studentCode.getTest().getSubject();
        this.testDay = studentCode.getTest().getTestDay();
        this.imageFullFileName = studentCode.getTestScore().getTestImageFullName();
        this.imageAnswerFileName = studentCode.getTestScore().getTestImageAnswerName();
        this.imageUserInfoFileName = studentCode.getTestScore().getTestImageUserInfoName();
        this.createdTime = studentCode.getTestScore().getCreatedTime();
    }

    public ScanResponse convertByStudentCode(StudentCode studentCode) {
        String questionsIncorrect = studentCode.getTestScore().getResult();
        long numberQuestionCorrect = questionsIncorrect.chars().filter(e -> e == '1').count();
        this.studentFullName = studentCode.getStudent() != null ? studentCode.getStudent().getFullName() : "";
        this.studentCode = studentCode.getCode();
        this.testCode = studentCode.getTest().getCode();
        this.score = studentCode.getTestScore().getScore();
        this.numberQuestionCorrect = numberQuestionCorrect + "/50";
        this.subjectName = studentCode.getTest().getSubject();
        this.testDay = studentCode.getTest().getTestDay();
        this.imageFullFileName = studentCode.getTestScore().getTestImageFullName();
        this.imageAnswerFileName = studentCode.getTestScore().getTestImageAnswerName();
        this.imageUserInfoFileName = studentCode.getTestScore().getTestImageUserInfoName();
        this.createdTime = studentCode.getTestScore().getCreatedTime();
        return this;
    }

}
