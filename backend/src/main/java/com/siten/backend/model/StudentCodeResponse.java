package com.siten.backend.model;

import com.siten.backend.data.entity.StudentCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class StudentCodeResponse {
    private Long id;
    private String code;
    private String fullName;
    private String className;

    public StudentCodeResponse(StudentCode studentCode) {
        this.id = studentCode.getId();
        this.code = studentCode.getCode();
        this.fullName = studentCode.getStudent().getFullName();
        this.className = studentCode.getStudent().getClassroom().getName();
    }
}
