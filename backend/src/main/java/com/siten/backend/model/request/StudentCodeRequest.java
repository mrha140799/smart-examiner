package com.siten.backend.model.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
@Getter
@Setter
@NoArgsConstructor
public class StudentCodeRequest {
    private Long id;
    @NotNull
    private Long studentId;
    @NotNull
    @Length(min = 7, max = 7)
    private String code;
    @NotNull
    private Long testId;
}
