package com.siten.backend.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class PageResponse<T> {
    private List<T> contents;
    private int totalPages;
    private long totalElements;
    private int number;

    public void getPageResponse(Page<T> page) {
        contents = page.getContent();
        totalElements = page.getTotalElements();
        totalPages = page.getTotalPages();
        number = page.getNumber();
    }
}
