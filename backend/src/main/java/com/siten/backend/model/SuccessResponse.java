package com.siten.backend.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SuccessResponse {
    @JsonProperty("statusCode")
    private Integer statusCode;
    @JsonProperty("message")
    private String message;
    @JsonProperty("data")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object data;

    public static SuccessResponse ok() {
        SuccessResponse response = new SuccessResponse();
        response.statusCode = 200;
        response.message = "SUCCESS";
        return response;
    }

    public static SuccessResponse ok(Object data) {
        SuccessResponse response = new SuccessResponse();
        response.statusCode = 200;
        response.message = "SUCCESS";
        response.data = data;
        return response;
    }

    public static SuccessResponse created(Object data) {
        SuccessResponse response = new SuccessResponse();
        response.statusCode = 201;
        response.message = "CREATED";
        response.data = data;
        return response;
    }

}
