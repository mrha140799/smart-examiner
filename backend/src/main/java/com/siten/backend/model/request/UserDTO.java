package com.siten.backend.model.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.siten.backend.data.entity.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDTO {
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String imageAvatarName;
    private boolean gender;
    private String email;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String password;

    public User toUser() {
        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setPhoneNumber(phoneNumber);
        user.setImageAvatarName(imageAvatarName);
        user.setGender(gender);
        user.setEmail(email);
        return user;
    }
}
