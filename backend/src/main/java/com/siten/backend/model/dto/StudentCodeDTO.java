package com.siten.backend.model.dto;

import com.siten.backend.data.entity.Student;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class StudentCodeDTO {
    private Long id;
    private String code;
    private Student student;

    public StudentCodeDTO(Long id, String code, Date dateOfBirth, String fullName) {
        this.id = id;
        this.code = code;
        this.student = new Student();
        this.student.setDateOfBirth(dateOfBirth);
        this.student.setFullName(fullName);
    }
}
