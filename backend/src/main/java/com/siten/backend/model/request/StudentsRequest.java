package com.siten.backend.model.request;

import com.siten.backend.data.entity.Student;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class StudentsRequest {
    @NotNull
    private Long classroomId;
    @NotEmpty
    private List<Student> students;
}
