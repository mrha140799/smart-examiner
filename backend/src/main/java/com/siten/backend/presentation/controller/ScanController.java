package com.siten.backend.presentation.controller;

import com.siten.backend.domain.usecase.ScanService;
import com.siten.backend.model.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/api/scan")
@CrossOrigin("*")
public class ScanController {
    @Autowired
    private ScanService scanService;

    @PostMapping("/write-file")
    public ResponseEntity<SuccessResponse> writeFile(@RequestBody HashMap<String, String> requestData) {
        return new ResponseEntity<>(this.scanService.saveFile(requestData.get("fileContent")), HttpStatus.OK);
    }

    @PostMapping("/my-answer")
    public ResponseEntity<SuccessResponse> myAnswer(@RequestBody HashMap<String, String> requestData) {
        return new ResponseEntity<>(this.scanService.myAnswer(requestData.get("fileContent")), HttpStatus.OK);
    }

    @GetMapping("/my-histories")
    public ResponseEntity<SuccessResponse> myScanHistories() {
        return new ResponseEntity<>(this.scanService.myHistories(), HttpStatus.OK);
    }
}
