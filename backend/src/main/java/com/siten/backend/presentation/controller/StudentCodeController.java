package com.siten.backend.presentation.controller;

import com.siten.backend.domain.usecase.StudentCodeService;
import com.siten.backend.model.SuccessResponse;
import com.siten.backend.model.request.StudentCodeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/student-codes")
@CrossOrigin("*")
public class StudentCodeController {
    @Autowired
    private StudentCodeService studentCodeService;

    @GetMapping("/get-by-test-id")
    public ResponseEntity<SuccessResponse> getAllByTestId(@RequestParam Long testId) {
        return new ResponseEntity<>(this.studentCodeService.getAllMyStudentCodesByTestId(testId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<SuccessResponse> createOne(@Valid @RequestBody StudentCodeRequest studentCodeRequest) {
        return new ResponseEntity<>(this.studentCodeService.createOne(studentCodeRequest), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<SuccessResponse> updateOne(@Valid @RequestBody StudentCodeRequest studentCodeRequest) {
        return new ResponseEntity<>(this.studentCodeService.updateOne(studentCodeRequest), HttpStatus.CREATED);
    }

    @GetMapping("/exists-by-student-code-and-test-id")
    public ResponseEntity<SuccessResponse> existsByStudentCodeAndTestId(@RequestParam("studentCode") String studentCode, @RequestParam("testId") Long testId) {
        return new ResponseEntity<>(this.studentCodeService.existsByStudentCodeAndTestId(studentCode, testId), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SuccessResponse> existsByStudentCodeAndTestId(@PathVariable Long id) {
        return new ResponseEntity<>(this.studentCodeService.getOneById(id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<SuccessResponse> deleteById(@PathVariable Long id) {
        return new ResponseEntity<>(this.studentCodeService.deleteById(id), HttpStatus.OK);
    }

    @GetMapping("/test")
    public ResponseEntity<SuccessResponse> getByNativeQueryTest() {
        return new ResponseEntity<>(this.studentCodeService.test(), HttpStatus.OK);
    }
}
