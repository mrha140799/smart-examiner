package com.siten.backend.presentation.controller;

import com.siten.backend.domain.usecase.AuthService;
import com.siten.backend.model.SuccessResponse;
import com.siten.backend.model.request.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
@CrossOrigin("*")
public class AuthController {
    @Autowired
    private AuthService authService;

    @PostMapping("/register")
    public ResponseEntity<SuccessResponse> register(@RequestBody UserDTO userDTO) {
        return new ResponseEntity<>(this.authService.register(userDTO), HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<SuccessResponse> login(@RequestParam(required = true, value = "email") String email,
                                                 @RequestParam(required = true, value = "password") String password) {
        return new ResponseEntity<>(this.authService.login(email, password), HttpStatus.OK);
    }

    @GetMapping("/is-valid-email")
    public ResponseEntity<SuccessResponse> isValidEmail(@RequestParam("email") String email) {
        return new ResponseEntity<>(this.authService.isValidEmail(email), HttpStatus.OK);
    }
}
