package com.siten.backend.presentation.controller;

import com.siten.backend.domain.usecase.UserService;
import com.siten.backend.model.SuccessResponse;
import com.siten.backend.model.request.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

@RestController
@RequestMapping("/api/user")
@CrossOrigin("*")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/me")
    public ResponseEntity<SuccessResponse> getCurrentUser() {
        return new ResponseEntity<>(userService.getCurrentUser(), HttpStatus.OK);
    }

    @PostMapping("/change-my-profile")
    public ResponseEntity<SuccessResponse> changeMyProfile(@RequestBody UserDTO userDTO) {
        return new ResponseEntity<>(userService.changeMyProfile(userDTO), HttpStatus.OK);
    }

    @PostMapping("/change-my-avatar")
    public ResponseEntity<SuccessResponse> changeMyAvatar(@RequestBody HashMap<String, String> requestData) {
        return new ResponseEntity<>(userService.changeMyAvatar(requestData.get("fileContent")), HttpStatus.OK);
    }


    @GetMapping("/my-schools")
    public ResponseEntity<SuccessResponse> getAllMySchool() {
        return new ResponseEntity<>(userService.getAllMySchools(), HttpStatus.OK);
    }

    @GetMapping("/my-classrooms")
    public ResponseEntity<SuccessResponse> getAllMyClassrooms() {
        return new ResponseEntity<>(userService.getAllMyClassRooms(), HttpStatus.OK);
    }

    @GetMapping("/my-students")
    public ResponseEntity<SuccessResponse> getAllMyStudents() {
        return new ResponseEntity<>(userService.getAllMyStudents(), HttpStatus.OK);
    }

    @GetMapping("/my-tests")
    public ResponseEntity<SuccessResponse> getAllMyTests() {
        return new ResponseEntity<>(userService.getAllMyTests(), HttpStatus.OK);
    }

    @GetMapping("/file-test-created")
    public ResponseEntity<String> getTestCreatedFIle() throws IOException {
        return new ResponseEntity<>(this.userService.getTestCreatedFile(), HttpStatus.OK);
    }

    @GetMapping("/file-student-created")
    public ResponseEntity<String> getStudentCreatedFile() throws IOException {
        return new ResponseEntity<>(this.userService.getStudentCreatedFile(), HttpStatus.OK);

    }

    @GetMapping("/file-form-answer")
    public ResponseEntity<String> getFormAnswerFile() throws IOException {
        return new ResponseEntity<>(this.userService.getFormAnswerFile(), HttpStatus.OK);

    }
}
