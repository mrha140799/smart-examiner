package com.siten.backend.presentation.controller;

import com.siten.backend.data.entity.Classroom;
import com.siten.backend.domain.usecase.ClassroomService;
import com.siten.backend.model.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/classroom")
@CrossOrigin("*")
public class ClassroomController {
    @Autowired
    private ClassroomService classroomService;

    @PostMapping
    public ResponseEntity<SuccessResponse> createOne(@RequestBody Classroom classroom) {
        return new ResponseEntity<>(this.classroomService.createOne(classroom), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<SuccessResponse> deleteById(@PathVariable Long id) {
        return new ResponseEntity<>(this.classroomService.deleteById(id), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SuccessResponse> getOneById(@PathVariable Long id) {
        return new ResponseEntity<>(this.classroomService.findOneById(id), HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<SuccessResponse> updateClassroom(@RequestBody Classroom classroom) {
        return new ResponseEntity<>(this.classroomService.updateOne(classroom), HttpStatus.OK);
    }

}
