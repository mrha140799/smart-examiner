package com.siten.backend.presentation.controller;

import com.siten.backend.data.entity.Student;
import com.siten.backend.domain.usecase.StudentService;
import com.siten.backend.model.SuccessResponse;
import com.siten.backend.model.request.StudentsRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/students")
@CrossOrigin("*")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @GetMapping("/get-all-by-class-id")
    public ResponseEntity<SuccessResponse> getAllByClassId(@RequestParam Long classId) {
        return new ResponseEntity<>(this.studentService.getAllByClassId(classId), HttpStatus.OK);
    }

    @PostMapping("/create-many")
    public ResponseEntity<SuccessResponse> createMany(@RequestBody StudentsRequest studentsRequest) {
        return new ResponseEntity<>(this.studentService.createMany(studentsRequest), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<SuccessResponse> createOne(@Valid @RequestBody Student student, @RequestParam Long classroomId) {
        return new ResponseEntity<>(this.studentService.createOne(student, classroomId), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<SuccessResponse> updateOne(@Valid @RequestBody Student student, @RequestParam Long classroomId) {
        return new ResponseEntity<>(this.studentService.createOne(student, classroomId), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<SuccessResponse> deleteById(@PathVariable Long id) {
        return new ResponseEntity<>(this.studentService.deleteById(id), HttpStatus.OK);
    }
}
