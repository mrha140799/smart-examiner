package com.siten.backend.presentation.controller;

import com.siten.backend.data.entity.School;
import com.siten.backend.domain.usecase.SchoolService;
import com.siten.backend.model.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/school")
@CrossOrigin("*")
public class SchoolController {
    @Autowired
    private SchoolService schoolService;
    @GetMapping("/{id}")
    public ResponseEntity<SuccessResponse> getOneById(@PathVariable Long id) {
        return new ResponseEntity<>(schoolService.findSchoolById(id), HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<SuccessResponse> createOne(@RequestBody School school)  {
        return new ResponseEntity<>(schoolService.createSchool(school), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<SuccessResponse> updateSchool(@RequestBody School school)  {
        return new ResponseEntity<>(schoolService.updateSchool(school), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<SuccessResponse> deleteSchool(@PathVariable("id") Long id) {
        return new ResponseEntity<>(schoolService.deleteSchool(id), HttpStatus.OK);
    }

    @GetMapping("/get-all-by-address")
    public ResponseEntity<SuccessResponse> getAllByAddress(@RequestParam String address) {
        return new ResponseEntity<>(schoolService.findAllByAddress(address), HttpStatus.OK);
    }

    @GetMapping("/search-school")
    public ResponseEntity<SuccessResponse> getAllByClassroomName(
            @RequestParam(name = "idEqual", required = false) Long idEqual,
            @RequestParam(name = "idIn", required = false) List<Long> idIn,
            @RequestParam(name = "nameEqual", required = false) String nameEqual,
            @RequestParam(name = "nameIn", required = false) List<String> nameIn
    ) {
        return new ResponseEntity<>(schoolService.searchSchool(idEqual, idIn, nameEqual, nameIn), HttpStatus.OK);
    }
}
