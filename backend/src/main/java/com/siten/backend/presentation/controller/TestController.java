package com.siten.backend.presentation.controller;

import com.siten.backend.data.entity.Test;
import com.siten.backend.domain.usecase.TestService;
import com.siten.backend.model.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/test")
@CrossOrigin("*")
public class TestController {
    @Autowired
    private TestService testService;

    @PostMapping
    public ResponseEntity<SuccessResponse> createOne(@RequestBody @Valid Test test) {
        return new ResponseEntity<>(testService.createOne(test), HttpStatus.CREATED);
    }

    @GetMapping("/valid-test-code")
    public ResponseEntity<SuccessResponse> isValidTestCode(@RequestParam("code") String testCode, @RequestParam(value = "testId", required = false) Long testId) {
        return new ResponseEntity<>(testService.isValidTestCode(testCode, testId), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SuccessResponse> getTestById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(testService.getById(id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<SuccessResponse> deleteTestById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(testService.deleteById(id), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<SuccessResponse> updateTest(@RequestBody @Valid Test test) {
        return new ResponseEntity<>(testService.updateTest(test), HttpStatus.CREATED);
    }
}
