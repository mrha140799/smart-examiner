package com.siten.backend.presentation.controller;

import com.siten.backend.domain.usecase.AdminService;
import com.siten.backend.model.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/admin")
public class AdminController {
    @Autowired
    private AdminService adminService;

    @PostMapping("/change-user-authorities")
    public ResponseEntity<SuccessResponse> changeUserAuthorities(@RequestParam("email") String email,
                                                                 @RequestParam("authorities")List<String> authorities) {
        return new ResponseEntity<>(adminService.changeUserAuthorities(email, authorities), HttpStatus.OK);
    }
}
