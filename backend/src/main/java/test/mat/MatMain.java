package test.mat;

import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.springframework.security.core.parameters.P;

import java.awt.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MatMain {
    public static void main(String[] args) throws IOException {
        nu.pattern.OpenCV.loadLocally();
        // Import ảnh vào Mat
        Mat src = Imgcodecs.imread("./files/test-in-cam-cors-v2.jpg", Imgcodecs.IMREAD_GRAYSCALE);
        Mat dstThres = new Mat();
        Imgproc.threshold(src, dstThres, 100, 255, Imgproc.THRESH_BINARY);
        List<MatOfPoint> matOfPoints = new ArrayList<>();
        Imgproc.findContours(dstThres, matOfPoints, new Mat(), Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_NONE);
        // Cắt ảnh mới có khích thước 500x400 tại vị trí x: 500, y: 400
//        Mat c = new Mat(a, new Rect(500, 500, 500, 400));
        // ghi ảnh
        Mat output = src.clone();
        for (MatOfPoint mop : matOfPoints) {
            MatOfPoint2f approx = new MatOfPoint2f();
            MatOfPoint2f toMop2f = new MatOfPoint2f(mop.toArray());
            Imgproc.approxPolyDP(toMop2f, approx, Imgproc.arcLength(toMop2f, true) * .01, true);
            Rect rect = Imgproc.boundingRect(mop);
            long total = approx.rows();
            if (approx.rows() == 4 && approx.cols() == 1) {
                List<Double> cos = new ArrayList<>();
                Point[] points = approx.toArray();
                for (int j = 2; j < total + 1; j++) {
                    cos.add(angle(points[(int) (j % total)], points[j - 2], points[j - 1]));
                }
                Collections.sort(cos);
                Double minCos = cos.get(0);
                Double maxCos = cos.get(cos.size() - 1);
                double ratio = Math.abs(1 - (double) rect.width / rect.height);
                double approxArea = Imgproc.contourArea(approx);
                System.out.println(approxArea);
//                boolean isRect = total == 4 && minCos >= -0.1 && maxCos <= 0.3 && ratio <= 0.01 && approxArea >= 50 && approxArea <= 700;
                boolean isRect = total == 4;
                if (isRect) {
                    drawText(output, rect.tl(),"SQU");
                }
            }
        }
        MatOfByte matOfByte = new MatOfByte();
        Imgcodecs.imencode(".png", output, matOfByte);
        byte[] bytesSrc = matOfByte.toArray();
        writeFile(bytesSrc, "test.png");
    }

    public static void writeFile(byte[] bytes, String filename) throws IOException {
        try (FileOutputStream fileOutputStream = new FileOutputStream("./files/output/" + filename)) {
            fileOutputStream.write(bytes);
        }
    }

    private static double angle(Point pt1, Point pt2, Point pt0) {
        double dx1 = pt1.x - pt0.x;
        double dy1 = pt1.y - pt0.y;
        double dx2 = pt2.x - pt0.x;
        double dy2 = pt2.y - pt0.y;
        return (dx1 * dx2 + dy1 * dy2) / Math.sqrt((dx1 * dx1 + dy1 * dy1) * (dx2 * dx2 + dy2 * dy2) + 1e-10);
    }

    private static void drawText(Mat mat, Point ofs, String text) {
        Imgproc.putText(mat, text, ofs, Core.FONT_HERSHEY_COMPLEX, 0.5, new Scalar(0, 0, 0));
    }

}
