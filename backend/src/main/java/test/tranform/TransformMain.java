package test.tranform;

import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import test.mat.MatMain;

import java.io.IOException;
import java.util.*;

public class TransformMain {
    public static void main(String[] args) throws IOException {
        nu.pattern.OpenCV.loadLocally();
        Mat src = Imgcodecs.imread("./files/scan_results/test.png", Imgcodecs.IMREAD_GRAYSCALE);
        Mat mathThreshold = thresholdImage(src.clone(), 100, Imgproc.THRESH_BINARY_INV);
        Mat kernelCross = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5, 5));
        Mat dilateCross = new Mat();
        Imgproc.dilate(mathThreshold, dilateCross, kernelCross);
        List<Rect> rectsEdge = findContoursRect(dilateCross,Imgproc.RETR_EXTERNAL, 4500, 6500);
//        drawCircleToImage(src, dilateCross, 4500, 6500);
        Point[] pointsEdge = get4PointsEdge(rectsEdge);
//////        transform
        Mat matTransform4PointEdge = transform4Point(1500, 1000, src, pointsEdge);
        Mat matCrossAnswerThreshold = adaptiveThresholdImage(matTransform4PointEdge, 255, Imgproc.THRESH_BINARY_INV | Imgproc.THRESH_BINARY);
        Mat dilateAnswer = new Mat();
        Mat kernelForm = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3));
        Imgproc.dilate(matCrossAnswerThreshold, dilateAnswer, kernelForm);
//        drawCircleToImage(matTransform4PointEdge, dilateAnswer, 0, 800);
        List<Rect> rectsAnswerEdge = findContoursRect(dilateAnswer,Imgproc.RETR_LIST ,500, 800);
        Point[] pointsAnswer = get4PointsEdge(rectsAnswerEdge.subList(4, 8));
        Point[] pointsInfo = get4pointsInfo(rectsAnswerEdge.subList(0, 4));
        Mat test = transform4Point(500, 900, matTransform4PointEdge, pointsAnswer);
//        Mat test = transform4Point(300, 600, matTransform4PointEdge, pointsInfo);
        Mat matThresholdAnswer = thresholdImage(test, 255, Imgproc.THRESH_BINARY_INV | Imgproc.THRESH_OTSU);
        Mat dilateTest = new Mat();
        Mat kernelAnswerForm = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(1, 1));
        Imgproc.dilate(matThresholdAnswer, dilateTest, kernelAnswerForm);

//        List<Rect> circleAnswer = findContoursCircle(dilateAnswer, 0, 1000);

        drawAnswerToImage(test, dilateTest, 0, 890);
        MatOfByte matOfByte = new MatOfByte();
        Imgcodecs.imencode(".png", test, matOfByte);
        byte[] bytesSrc = matOfByte.toArray();
        MatMain.writeFile(bytesSrc, "test.png");
        System.out.println("----> wrote file successfully!");
    }

    private static Point[] get4pointsInfo(List<Rect> rectangles) {
        Point[] points = new Point[4];
        Rect rectXMax = rectangles.get(0);
        Rect rectXMin = rectangles.get(0);
        Rect rectYMax = rectangles.get(0);
        Rect rectYMin = rectangles.get(0);
        for (Rect rect : rectangles) {
            if (((rectXMax.tl().x + rectXMax.br().x) / 2) < ((rect.tl().x + rect.br().x) / 2)) {
                rectXMax = rect;
            }
            if (((rectXMin.tl().x + rectXMin.br().x) / 2) > ((rect.tl().x + rect.br().x) / 2)) {
                rectXMin = rect;
            }
            if (((rectYMax.tl().y + rectYMax.br().y) / 2) < ((rect.tl().y + rect.br().y) / 2)) {
                rectYMax = rect;
            }
            if (((rectYMin.tl().y + rectYMin.br().y) / 2) > ((rect.tl().y + rect.br().y) / 2)) {
                rectYMin = rect;
            }
        }
        points[0] = new Point(rectXMax.tl().x, rectXMax.tl().y);
        points[1] = new Point(rectXMin.br().x, rectXMax.tl().y);
        points[2] = new Point(rectXMin.br().x, rectXMin.br().y);
        points[3] = new Point(rectXMax.tl().x, rectXMin.br().y);
        return points;
    }

    private static List<Rect> sortByYDesc(List<Rect> rects) {
        for (int i = 0; i < rects.size() - 1; i++) {
            for (int j = 1; j < rects.size(); j++) {
                if (rects.get(i).y < rects.get(j).y) {
                    Rect rectTemp = rects.get(i);
                    rects.remove(i);
                    rects.add(j, rectTemp);
                }
            }
        }
        return rects;
    }


    private static Mat thresholdImage(Mat src, int thresholdNumber, int thresholdType) {
        Mat dst = new Mat();
        Imgproc.threshold(src, dst, thresholdNumber, 255, thresholdType);
        return dst;
    }

    private static Mat adaptiveThresholdImage(Mat src, int thresholdNumber, int thresholdType) {
        Mat dst = new Mat();
        Imgproc.adaptiveThreshold(src, dst, thresholdNumber, Imgproc.ADAPTIVE_THRESH_MEAN_C,
                thresholdType, 59, 18);
        return dst;
    }

    private static Point[] get4PointsEdge(List<Rect> rectangles) {
        Point[] points = new Point[4];
        Rect rectXMax = rectangles.get(0);
        Rect rectXMin = rectangles.get(0);
        Rect rectYMax = rectangles.get(0);
        Rect rectYMin = rectangles.get(0);
        for (Rect rect : rectangles) {
            if (((rectXMax.tl().x + rectXMax.br().x) / 2) < ((rect.tl().x + rect.br().x) / 2)) {
                rectXMax = rect;
            }
            if (((rectXMin.tl().x + rectXMin.br().x) / 2) > ((rect.tl().x + rect.br().x) / 2)) {
                rectXMin = rect;
            }
            if (((rectYMax.tl().y + rectYMax.br().y) / 2) < ((rect.tl().y + rect.br().y) / 2)) {
                rectYMax = rect;
            }
            if (((rectYMin.tl().y + rectYMin.br().y) / 2) > ((rect.tl().y + rect.br().y) / 2)) {
                rectYMin = rect;
            }
        }
        double xAverage = (((rectXMax.tl().x + rectXMax.br().x) / 2) + (rectXMin.tl().x + rectXMin.br().x) / 2) / 2;
        double yAverage = (((rectYMax.tl().y + rectYMax.br().y) / 2) + (rectYMin.tl().y + rectYMin.br().y) / 2) / 2;
        for (Rect rect : rectangles) {
            if (((rect.tl().x + rect.br().x) / 2) < xAverage && ((rect.tl().y + rect.br().y) / 2) < yAverage) {
                points[2] = rect.br();
            }
            if (((rect.tl().x + rect.br().x) / 2) > xAverage && ((rect.tl().y + rect.br().y) / 2) < yAverage) {
                points[3] = new Point(rect.tl().x, rect.br().y);
            }
            if (((rect.tl().x + rect.br().x) / 2) < xAverage && ((rect.tl().y + rect.br().y) / 2) > yAverage) {
                points[1] = new Point(rect.br().x, rect.tl().y);
            }
            if (((rect.tl().x + rect.br().x) / 2) > xAverage && ((rect.tl().y + rect.br().y) / 2) > yAverage) {
                points[0] = rect.tl();
            }
        }
        return points;
    }


    private static Mat transform4Point(int row, int col, Mat src, Point[] pointsEdge) {
        Mat destImage = new Mat(row, col, src.type());
        Mat src_mat = new MatOfPoint2f(pointsEdge[0], pointsEdge[1], pointsEdge[2], pointsEdge[3]);
        Mat dst = new MatOfPoint2f(new Point(destImage.width() - 1, destImage.height() - 1), new Point(0, destImage.height() - 1), new Point(0, 0), new Point(destImage.width() - 1, 0));
        Mat transform = Imgproc.getPerspectiveTransform(src_mat, dst);
        Imgproc.warpPerspective(src, destImage, transform, destImage.size());
        return destImage;
    }

    private static List<Rect> findContoursRect(Mat mathThreshold,int RETR_Type, int rectAreaGreater, int rectAreaLess) {
        List<MatOfPoint> matOfPoints = new ArrayList<>();
        Imgproc.findContours(mathThreshold, matOfPoints, new Mat(), RETR_Type, Imgproc.CHAIN_APPROX_SIMPLE);
        sortTopToButton(matOfPoints);
        List<Rect> rects = new ArrayList<>();
        for (MatOfPoint mop : matOfPoints) {
            MatOfPoint2f approx = new MatOfPoint2f();
            MatOfPoint2f toMop2f = new MatOfPoint2f(mop.toArray());
            Imgproc.approxPolyDP(toMop2f, approx, Imgproc.arcLength(toMop2f, true) * .01, true);
            if (Imgproc.contourArea(approx) > 100 && approx.rows() >= 4 && approx.rows() <=6) {
                Rect rect = Imgproc.boundingRect(mop);
                double width = rect.width;
                double height = rect.height;
                boolean isRect = (width / height) < 1.5;
                int pixelOfContour = Core.countNonZero(mathThreshold.submat(rect));
                if (isRect && pixelOfContour > 500 && rect.area() > rectAreaGreater && rect.area() < rectAreaLess) {
                    rects.add(rect);
                }
            }
        }
        return rects;
    }

    private static List<Rect> findContoursCircle(Mat mathThreshold, int rectAreaGreater, int rectAreaLess) {
        List<MatOfPoint> matOfPoints = new ArrayList<>();
        Imgproc.findContours(mathThreshold, matOfPoints, new Mat(), Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);
        matOfPoints.sort(new Comparator<MatOfPoint>() {
            @Override
            public int compare(MatOfPoint o1, MatOfPoint o2) {
                Rect rect1 = Imgproc.boundingRect(o1);
                Rect rect2 = Imgproc.boundingRect(o2);
                int result = 0;
                double total = rect1.tl().y / rect2.tl().y;
                if (total >= 0.9 && total <= 1.4) {
                    result = Double.compare(rect1.tl().x, rect2.tl().x);
                }
                return result;
            }
        });
        List<Rect> rects = new ArrayList<>();
        for (MatOfPoint mop : matOfPoints) {
            MatOfPoint2f approx = new MatOfPoint2f();
            MatOfPoint2f toMop2f = new MatOfPoint2f(mop.toArray());
            Imgproc.approxPolyDP(toMop2f, approx, Imgproc.arcLength(toMop2f, true) * .01, true);
            if (Imgproc.contourArea(approx) > 20 && approx.rows() > 20) {
                Rect rect = Imgproc.boundingRect(mop);
                double width = rect.width;
                double height = rect.height;
                boolean isRect = (width / height) < 1.5;
                int pixelOfContour = Core.countNonZero(mathThreshold.submat(rect));
                if (isRect && pixelOfContour > 10 && rect.area() > rectAreaGreater && rect.area() < rectAreaLess) {
                    rects.add(rect);
                }
            }
        }
        return rects;
    }

    private static void drawCircleToImage(Mat src, Mat mathThreshold, int start, int end) {
        List<MatOfPoint> matOfPoints = new ArrayList<>();
        Imgproc.findContours(mathThreshold, matOfPoints, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
        sortTopToButton(matOfPoints);
        int i = 0;
        for (MatOfPoint mop : matOfPoints) {
            MatOfPoint2f approx = new MatOfPoint2f();
            MatOfPoint2f toMop2f = new MatOfPoint2f(mop.toArray());
            Imgproc.approxPolyDP(toMop2f, approx, Imgproc.arcLength(toMop2f, true) * .01, true);
            if (Imgproc.contourArea(approx) > 100 && approx.rows() >= 4 && approx.rows() < 6) {
                Rect rect = Imgproc.boundingRect(mop);
//                for (Point point : approx.toArray()) {
//                    Imgproc.circle(src, point, 20, new Scalar(0,0,0), 10);
//                }
                double width = rect.width;
                double height = rect.height;
                boolean isRect = (width / height) < 1.5;
                int pixelOfContour = Core.countNonZero(mathThreshold.submat(rect));
                if (isRect && pixelOfContour > 500 && rect.area() > start && rect.area() < end) {
                    System.out.println(pixelOfContour);
//                    System.out.println(rect.area());
//                    System.out.printf("rect: w-%f, h-%f, area-%f\n", width, height, rect.area());
//                    for (Point point : approx.toArray()) {
//                        Imgproc.circle(src, point, 20, new Scalar(0, 0, 0), 10);
//                    }
                    Imgproc.putText(
                            src,
                            "" + i,
                            rect.tl(),
                            Core.FONT_HERSHEY_SIMPLEX,
                            2,
                            new Scalar(0, 0, 255),
                            1
                    );
                    i++;
                }
            }
        }
    }

    private static void drawAnswerToImage(Mat src, Mat mathThreshold, int start, int end) {
        List<MatOfPoint> matOfPoints = new ArrayList<>();
        Imgproc.findContours(mathThreshold, matOfPoints, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
        filterCircleAnswer(matOfPoints, mathThreshold);
//        sortBottomToTop(matOfPoints);
        sortLeftToRight(matOfPoints);
        int i = 0;
//        HashMap<String, String> listAnswerChosen = getUserInfoChosen(matOfPoints, mathThreshold);
//        System.out.println(listAnswerChosen);
        for (MatOfPoint mop : matOfPoints) {
            Rect rect = Imgproc.boundingRect(mop);
            int pixelOfContour = Core.countNonZero(mathThreshold.submat(rect));
            System.out.println("filter" +  pixelOfContour);
            Imgproc.putText(
                    src,
                    "" + i,
                    rect.tl(),
                    Core.FONT_HERSHEY_SIMPLEX,
                    0.4,
                    new Scalar(0, 0, 255),
                    1
            );
            i++;
        }
    }

    private static void sortBottomToTop(List<MatOfPoint> data) {
        for (int i = 0; i < data.size() - 1; i++) {
            for (int j = i + 1; j < data.size(); j++) {
                Rect rect1 = Imgproc.boundingRect(data.get(i));
                Rect rect2 = Imgproc.boundingRect(data.get(j));
                if (isBubbleYLessThan(rect1, rect2)) {
                    MatOfPoint mopTemp = data.get(i);
                    data.set(i, data.get(j));
                    data.set(j, mopTemp);
                }
            }
        }
    }

    private static HashMap<String, String> getUserInfoChosen(List<MatOfPoint> data, Mat matThreshold) {
        int indexBubbleStart = 0;
        int indexBubbleEnd = 10;
        int indexBubbleTestCodeEnd = 40;
        HashMap<String, String> result = new HashMap<>();
        StringBuilder userId = new StringBuilder();
        StringBuilder testCode = new StringBuilder();
        for (int i = 0; i < 11; i++) {
            List<MatOfPoint> listAnswerOfQuestion = data.subList(indexBubbleStart, indexBubbleEnd);
            String label = getUserInfoChosenLabel(listAnswerOfQuestion, matThreshold);
            boolean isTestCodeLabel = indexBubbleEnd > indexBubbleTestCodeEnd;
            if (isTestCodeLabel) {
                if (!label.equals("")) {
                    testCode.insert(0, label);
                }
            } else {
                if (!label.equals("")) {
                    userId.insert(0, label);
                }
            }
            indexBubbleStart += 10;
            indexBubbleEnd += 10;
        }
        result.put("userId", userId.toString());
        result.put("testCode", testCode.toString());
        return result;
    }

    private static String getUserInfoChosenLabel(List<MatOfPoint> data, Mat matThreshold) {
        List<Integer> indexChosen = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            MatOfPoint2f approx = new MatOfPoint2f();
            MatOfPoint2f toMop2f = new MatOfPoint2f(data.get(i).toArray());
            Imgproc.approxPolyDP(toMop2f, approx, Imgproc.arcLength(toMop2f, true) * .01, true);
            Rect rect = Imgproc.boundingRect(data.get(i));
            int pixelOfContour = Core.countNonZero(matThreshold.submat(rect));
            if (pixelOfContour > 340) {
                System.out.println("pixelOfContour " + pixelOfContour);
                indexChosen.add(i);
            }
        }
        return convertIndexToUserInfoLabel(indexChosen);
    }

    private static void sortTopToButton(List<MatOfPoint> data) {
        data.sort((o1, o2) -> {
            Rect rect1 = Imgproc.boundingRect(o1);
            Rect rect2 = Imgproc.boundingRect(o2);
            return Double.compare(rect1.tl().y, rect2.tl().y);
        });
    }

    private static void sortLeftToRight(List<MatOfPoint> data) {
        for (int i = 0; i < data.size() - 1; i++) {
            for (int j = i + 1; j < data.size(); j++) {
                Rect rect1 = Imgproc.boundingRect(data.get(i));
                Rect rect2 = Imgproc.boundingRect(data.get(j));
                if (isBubbleXLessThan(rect1, rect2)) {
                    MatOfPoint mopTemp = data.get(i);
                    data.set(i, data.get(j));
                    data.set(j, mopTemp);
                }
            }
        }
    }

    private static boolean isBubbleXLessThan(Rect rect1, Rect rect2) {
        int tolerance = 9;
        if (rect1.tl().y + tolerance < rect2.tl().y)
            return true;
        if (rect2.tl().y + tolerance < rect1.tl().y)
            return false;
        return (rect1.tl().x < rect2.tl().x);
    }

    private static boolean isBubbleYLessThan(Rect rect1, Rect rect2) {
        int tolerance = 9;
        if (rect1.tl().x + tolerance < rect2.tl().x)
            return true;
        if (rect2.tl().x + tolerance < rect1.tl().x)
            return false;
        return (rect1.tl().y < rect2.tl().y);
    }

    private static boolean filterCircleAnswer(List<MatOfPoint> listCircle, Mat mathThreshold) {
        return listCircle.removeIf(e -> {
            MatOfPoint2f approx = new MatOfPoint2f();
            MatOfPoint2f toMop2f = new MatOfPoint2f(e.toArray());
            Imgproc.approxPolyDP(toMop2f, approx, Imgproc.arcLength(toMop2f, true) * .01, true);
            Rect rect = Imgproc.boundingRect(e);
            int pixelOfContour = Core.countNonZero(mathThreshold.submat(rect));
//            System.out.println("filter" +  pixelOfContour);
            return approx.rows() < 7 || pixelOfContour < 130;
        });
    }

    private static HashMap<Integer, String> getListAnswerChosen(List<MatOfPoint> data, Mat matThreshold) {
        int approx = 17;
        int questionIndex = 50;
        int colNumber = 3;
        int indexAnswerStart = 8;
        int indexAnswerEnd = 12;
        HashMap<Integer, String> result = new HashMap<>();
        List<MatOfPoint> question34 = data.subList(0, 4);
        result.put(34, getAnswerChosen(question34, matThreshold));
        List<MatOfPoint> question17 = data.subList(4, 8);
        result.put(17, getAnswerChosen(question17, matThreshold));
        for (int i = 50; i >= 35; i--) {
            for (int j = 0; j < colNumber; j++) {
                List<MatOfPoint> listAnswerOfQuestion = data.subList(indexAnswerStart, indexAnswerEnd);
                result.put(questionIndex - (j * approx), getAnswerChosen(listAnswerOfQuestion, matThreshold));
                indexAnswerStart += 4;
                indexAnswerEnd += 4;
            }
            questionIndex--;
        }
        return result;
    }

    private static String getAnswerChosen(List<MatOfPoint> data, Mat matThreshold) {
        int indexChosen = -1;
        for (int i = 0; i < data.size(); i++) {
            MatOfPoint2f approx = new MatOfPoint2f();
            MatOfPoint2f toMop2f = new MatOfPoint2f(data.get(i).toArray());
            Imgproc.approxPolyDP(toMop2f, approx, Imgproc.arcLength(toMop2f, true) * .01, true);
            Rect rect = Imgproc.boundingRect(data.get(i));
            int pixelOfContour = Core.countNonZero(matThreshold.submat(rect));
            System.out.println(pixelOfContour);
            if (pixelOfContour > 357) {
                indexChosen = i;
                break;
            }
        }
        return convertIndexToAnswerLabel(indexChosen);
    }

    private static String convertIndexToUserInfoLabel(List<Integer> indexes) {
        StringBuilder userInfoLabel = new StringBuilder();
        for (Integer index : indexes) {
            switch (index) {
                case 0:
                    userInfoLabel.append("9");
                    break;
                case 1:
                    userInfoLabel.append("8");
                    break;
                case 2:
                    userInfoLabel.append("7");
                    break;
                case 3:
                    userInfoLabel.append("6");
                    break;
                case 4:
                    userInfoLabel.append("5");
                    break;
                case 5:
                    userInfoLabel.append("4");
                    break;
                case 6:
                    userInfoLabel.append("3");
                    break;
                case 7:
                    userInfoLabel.append("2");
                    break;
                case 8:
                    userInfoLabel.append("1");
                    break;
                case 9:
                    userInfoLabel.append("0");
                    break;
            }
        }
        return userInfoLabel.toString();
    }

    private static String convertIndexToAnswerLabel(int index) {
        switch (index) {
            case 0:
                return "D";
            case 1:
                return "C";
            case 2:
                return "B";
            case 3:
                return "A";
        }
        return null;
    }
}

