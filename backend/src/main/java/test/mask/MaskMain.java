package test.mask;
import org.opencv.core.*;
import org.opencv.highgui.Highgui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MaskMain {
    public static void main(String[] args) throws IOException {
        nu.pattern.OpenCV.loadLocally();
        Mat src = Imgcodecs.imread("./files/test1.jpeg", Imgcodecs.IMREAD_COLOR);
//        Mat dst0 = sharpen(src, new Mat());
        System.out.println(src.channels());
        Mat kern = new Mat(3, 3, CvType.CV_8S);
        int row = 1, col = 0;
        kern.put(row, col, 0, -1, 0, -1, 5, -1, 0, -1, 0);
        Mat dst1 = new Mat();
        Imgproc.filter2D(src, dst1, src.depth(), kern);



        MatOfByte matOfByte = new MatOfByte();
        Imgcodecs.imencode(".png", dst1, matOfByte);
        byte[] bytes = matOfByte.toArray();
        FileOutputStream fileOutputStream = new FileOutputStream("./files/output/test.png");
        fileOutputStream.write(bytes);
    }
    public static Mat sharpen(Mat myImage, Mat Result) {
        myImage.convertTo(myImage, CvType.CV_8U);
        int nChannels = myImage.channels();
        Result.create(myImage.size(), myImage.type());
        for (int j = 1; j < myImage.rows() - 1; ++j) {
            for (int i = 1; i < myImage.cols() - 1; ++i) {
                double sum[] = new double[nChannels];
                for (int k = 0; k < nChannels; ++k) {
                    double top = -myImage.get(j - 1, i)[k];
                    double bottom = -myImage.get(j + 1, i)[k];
                    double center = (5 * myImage.get(j, i)[k]);
                    double left = -myImage.get(j, i - 1)[k];
                    double right = -myImage.get(j, i + 1)[k];
                    sum[k] = saturate(top + bottom + center + left + right);
                }
                Result.put(j, i, sum);
            }
        }
        Result.row(0).setTo(new Scalar(0));
        Result.row(Result.rows() - 1).setTo(new Scalar(0));
        Result.col(0).setTo(new Scalar(0));
        Result.col(Result.cols() - 1).setTo(new Scalar(0));
        return Result;
    }
    public static double saturate(double x) {
        return x > 255.0 ? 255.0 : (x < 0.0 ? 0.0 : x);
    }
}
